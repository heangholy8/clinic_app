// To parse this JSON data, do
//
//     final authModel = authModelFromJson(jsonString);

import 'dart:convert';

AuthModel authModelFromJson(String str) => AuthModel.fromJson(json.decode(str));

String authModelToJson(AuthModel data) => json.encode(data.toJson());

class AuthModel {
    String? refresh;
    String? access;
    int? id;
    String? username;
    bool? isActive;
    bool? isStaff;
    bool? isDoctor;
    bool? isNurse;
    bool? isProvider;
    ProfileData? profileData;
    ClinicData? clinicData;
    int? branchId;

    AuthModel({
        this.refresh,
        this.access,
        this.id,
        this.username,
        this.isActive,
        this.isStaff,
        this.isDoctor,
        this.isNurse,
        this.isProvider,
        this.profileData,
        this.clinicData,
        this.branchId,
    });

    factory AuthModel.fromJson(Map<String, dynamic> json) => AuthModel(
        refresh: json["refresh"],
        access: json["access"],
        id: json["id"],
        username: json["username"],
        isActive: json["is_active"],
        isStaff: json["is_staff"],
        isDoctor: json["is_doctor"],
        isNurse: json["is_nurse"],
        isProvider: json["is_provider"],
        profileData: ProfileData.fromJson(json["profile_data"]),
        clinicData: ClinicData.fromJson(json["clinic_data"]),
        branchId: json["branch_id"],
    );

    Map<String, dynamic> toJson() => {
        "refresh": refresh,
        "access": access,
        "id": id,
        "username": username,
        "is_active": isActive,
        "is_staff": isStaff,
        "is_doctor": isDoctor,
        "is_nurse": isNurse,
        "is_provider": isProvider,
        "profile_data": profileData!.toJson(),
        "clinic_data": clinicData!.toJson(),
        "branch_id": branchId,
    };
}

class ClinicData {
    int? id;
    String? name;
    String? nameLatin;
    int? isMonday;
    int? isTuesday;
    int? isWednesday;
    int? isThursday;
    int? isFriday;
    int? isSaturday;
    int? isSunday;
    int? timeSlot;
    String? morningStartTime;
    String? morningEndTime;
    String? afternoonStartTime;
    String? afternoonEndTime;
    String? eveningStartTime;
    String? eveningEndTime;
    int? branchId;
    List<String>? morningSliceData;
    List<String>? afternoonSliceData;
    List<String>? eveningSliceData;
    String? secretKey;

    ClinicData({
        this.id,
        this.name,
        this.nameLatin,
        this.isMonday,
        this.isTuesday,
        this.isWednesday,
        this.isThursday,
        this.isFriday,
        this.isSaturday,
        this.isSunday,
        this.timeSlot,
        this.morningStartTime,
        this.morningEndTime,
        this.afternoonStartTime,
        this.afternoonEndTime,
        this.eveningStartTime,
        this.eveningEndTime,
        this.branchId,
        this.morningSliceData,
        this.afternoonSliceData,
        this.eveningSliceData,
        this.secretKey,
    });

    factory ClinicData.fromJson(Map<String, dynamic> json) => ClinicData(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
        isMonday: json["is_monday"],
        isTuesday: json["is_tuesday"],
        isWednesday: json["is_wednesday"],
        isThursday: json["is_thursday"],
        isFriday: json["is_friday"],
        isSaturday: json["is_saturday"],
        isSunday: json["is_sunday"],
        timeSlot: json["time_slot"],
        morningStartTime: json["morning_start_time"],
        morningEndTime: json["morning_end_time"],
        afternoonStartTime: json["afternoon_start_time"],
        afternoonEndTime: json["afternoon_end_time"],
        eveningStartTime: json["evening_start_time"],
        eveningEndTime: json["evening_end_time"],
        branchId: json["branch_id"],
        morningSliceData: List<String>.from(json["morning_slice_data"].map((x) => x)),
        afternoonSliceData: List<String>.from(json["afternoon_slice_data"].map((x) => x)),
        eveningSliceData: List<String>.from(json["evening_slice_data"].map((x) => x)),
        secretKey: json["secret_key"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
        "is_monday": isMonday,
        "is_tuesday": isTuesday,
        "is_wednesday": isWednesday,
        "is_thursday": isThursday,
        "is_friday": isFriday,
        "is_saturday": isSaturday,
        "is_sunday": isSunday,
        "time_slot": timeSlot,
        "morning_start_time": morningStartTime,
        "morning_end_time": morningEndTime,
        "afternoon_start_time": afternoonStartTime,
        "afternoon_end_time": afternoonEndTime,
        "evening_start_time": eveningStartTime,
        "evening_end_time": eveningEndTime,
        "branch_id": branchId,
        "morning_slice_data": List<dynamic>.from(morningSliceData!.map((x) => x)),
        "afternoon_slice_data": List<dynamic>.from(afternoonSliceData!.map((x) => x)),
        "evening_slice_data": List<dynamic>.from(eveningSliceData!.map((x) => x)),
        "secret_key": secretKey,
    };
}

class ProfileData {
    int? id;
    String? title;
    String? titleLatin;
    String? lastname;
    String? firstname;
    String? lastnameLatin;
    String? firstnameLatin;
    String? gender;
    String? dateOfBirth;
    String? phone;

    ProfileData({
        this.id,
        this.title,
        this.titleLatin,
        this.lastname,
        this.firstname,
        this.lastnameLatin,
        this.firstnameLatin,
        this.gender,
        this.dateOfBirth,
        this.phone,
    });

    factory ProfileData.fromJson(Map<String, dynamic> json) => ProfileData(
        id: json["id"],
        title: json["title"],
        titleLatin: json["title_latin"],
        lastname: json["lastname"],
        firstname: json["firstname"],
        lastnameLatin: json["lastname_latin"],
        firstnameLatin: json["firstname_latin"],
        gender: json["gender"],
        dateOfBirth:json["date_of_birth"],
        phone: json["phone"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "title_latin": titleLatin,
        "lastname": lastname,
        "firstname": firstname,
        "lastname_latin": lastnameLatin,
        "firstname_latin": firstnameLatin,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "phone": phone,
    };
}