// To parse this JSON data, do
//
//     final authPateintModel = authPateintModelFromJson(jsonString);

import 'dart:convert';

AuthPateintModel authPateintModelFromJson(String str) => AuthPateintModel.fromJson(json.decode(str));

String authPateintModelToJson(AuthPateintModel data) => json.encode(data.toJson());

class AuthPateintModel {
    String? refresh;
    String? access;
    int? id;
    int? globalId;
    String? username;
    int? clinicId;
    UserActive? userActive;
    ProfileData? profileData;

    AuthPateintModel({
        this.refresh,
        this.access,
        this.id,
        this.globalId,
        this.username,
        this.clinicId,
        this.userActive,
        this.profileData,
    });

    factory AuthPateintModel.fromJson(Map<String, dynamic> json) => AuthPateintModel(
        refresh: json["refresh"],
        access: json["access"],
        id: json["id"],
        globalId: json["global_id"],
        username: json["username"],
        clinicId: json["clinic_id"],
        userActive: json["user_active"] ==null?null: UserActive.fromJson(json["user_active"]),
        profileData: json["profile_data"] == null?null:ProfileData.fromJson(json["profile_data"]),
    );

    Map<String, dynamic> toJson() => {
        "refresh": refresh,
        "access": access,
        "id": id,
        "global_id": globalId,
        "username": username,
        "clinic_id": clinicId,
        "user_active": userActive==null?null:userActive!.toJson(),
        "profile_data": profileData==null?null:profileData!.toJson(),
    };
}

class ProfileData {
    dynamic patientId;
    dynamic id;
    dynamic lastname;
    dynamic firstname;
    dynamic lastnameLatin;
    dynamic firstnameLatin;
    dynamic gender;
    String? dateOfBirth;
    dynamic age;
    dynamic personalCardId;
    dynamic phone;
    dynamic email;
    dynamic isNssfMember;
    dynamic nssfNumber;
    dynamic isForeigner;
    BloodGroupTypeData? bloodGroupTypeData;
    dynamic status;
    String? registreredOn;
    dynamic registreredNumber;
    dynamic contactAddress;
    dynamic village;
    dynamic communeId;
    dynamic cityDistrictId;
    dynamic nationalityId;
    dynamic emergencyContact;
    dynamic familyMedicalHistory;
    dynamic lifestyleHabits;
    dynamic insuranceId;
    dynamic insuranceGroupNumber;
    dynamic medicalHistory;
    dynamic primaryCarePhysician;
    Profile? profile;
    dynamic qrCode;

    ProfileData({
        this.patientId,
        this.id,
        this.lastname,
        this.firstname,
        this.lastnameLatin,
        this.firstnameLatin,
        this.gender,
        this.dateOfBirth,
        this.age,
        this.personalCardId,
        this.phone,
        this.email,
        this.isNssfMember,
        this.nssfNumber,
        this.isForeigner,
        this.bloodGroupTypeData,
        this.status,
        this.registreredOn,
        this.registreredNumber,
        this.contactAddress,
        this.village,
        this.communeId,
        this.cityDistrictId,
        this.nationalityId,
        this.emergencyContact,
        this.familyMedicalHistory,
        this.lifestyleHabits,
        this.insuranceId,
        this.insuranceGroupNumber,
        this.medicalHistory,
        this.primaryCarePhysician,
        this.profile,
        this.qrCode,
    });

    factory ProfileData.fromJson(Map<String, dynamic> json) => ProfileData(
        patientId: json["patient_id"],
        id: json["id"],
        lastname: json["lastname"],
        firstname: json["firstname"],
        lastnameLatin: json["lastname_latin"],
        firstnameLatin: json["firstname_latin"],
        gender: json["gender"],
        dateOfBirth:json["date_of_birth"],
        age: json["age"],
        personalCardId: json["personal_card_id"],
        phone: json["phone"],
        email: json["email"],
        isNssfMember: json["is_nssf_member"],
        nssfNumber: json["nssf_number"],
        isForeigner: json["is_foreigner"],
        bloodGroupTypeData: json["blood_group_type_data"]==null?null:BloodGroupTypeData.fromJson(json["blood_group_type_data"]),
        status: json["status"],
        registreredOn: json["registrered_on"],
        registreredNumber: json["registrered_number"],
        contactAddress: json["contact_address"],
        village: json["village"],
        communeId: json["commune_id"],
        cityDistrictId: json["city_district_id"],
        nationalityId: json["nationality_id"],
        emergencyContact: json["emergency_contact"],
        familyMedicalHistory: json["family_medical_history"],
        lifestyleHabits: json["lifestyle_habits"],
        insuranceId: json["insurance_id"],
        insuranceGroupNumber: json["insurance_group_number"],
        medicalHistory: json["medical_history"],
        primaryCarePhysician: json["primary_care_physician"],
        profile: json["profile"] == null?null:Profile.fromJson(json["profile"]),
        qrCode: json["qr_code"],
    );

    Map<String, dynamic> toJson() => {
        "patient_id": patientId,
        "id": id,
        "lastname": lastname,
        "firstname": firstname,
        "lastname_latin": lastnameLatin,
        "firstname_latin": firstnameLatin,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "age": age,
        "personal_card_id": personalCardId,
        "phone": phone,
        "email": email,
        "is_nssf_member": isNssfMember,
        "nssf_number": nssfNumber,
        "is_foreigner": isForeigner,
        "blood_group_type_data": bloodGroupTypeData == null?null:bloodGroupTypeData!.toJson(),
        "status": status,
        "registrered_on": registreredOn,
        "registrered_number": registreredNumber,
        "contact_address": contactAddress,
        "village": village,
        "commune_id": communeId,
        "city_district_id": cityDistrictId,
        "nationality_id": nationalityId,
        "emergency_contact": emergencyContact,
        "family_medical_history": familyMedicalHistory,
        "lifestyle_habits": lifestyleHabits,
        "insurance_id": insuranceId,
        "insurance_group_number": insuranceGroupNumber,
        "medical_history": medicalHistory,
        "primary_care_physician": primaryCarePhysician,
        "profile": profile==null?null:profile!.toJson(),
        "qr_code": qrCode,
    };
}

class BloodGroupTypeData {
    dynamic id;
    dynamic name;
    dynamic nameLatin;
    dynamic objectType;
    dynamic child;

    BloodGroupTypeData({
        this.id,
        this.name,
        this.nameLatin,
        this.objectType,
        this.child,
    });

    factory BloodGroupTypeData.fromJson(Map<String, dynamic> json) => BloodGroupTypeData(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
        objectType: json["object_type"],
        child: json["child"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
        "object_type": objectType,
        "child": child,
    };
}

class Profile {
    int? id;
    String? fileOriginalName;
    int? fileSize;
    String? fileType;
    String? fileThumbnail;
    String? fileDisplay;
    int? objectId;
    String? objectType;
    int? clinicId;

    Profile({
        this.id,
        this.fileOriginalName,
        this.fileSize,
        this.fileType,
        this.fileThumbnail,
        this.fileDisplay,
        this.objectId,
        this.objectType,
        this.clinicId,
    });

    factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"],
        fileOriginalName: json["file_original_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileThumbnail: json["file_thumbnail"],
        fileDisplay: json["file_display"],
        objectId: json["object_id"],
        objectType: json["object_type"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_original_name": fileOriginalName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_thumbnail": fileThumbnail,
        "file_display": fileDisplay,
        "object_id": objectId,
        "object_type": objectType,
        "clinic_id": clinicId,
    };
}

class UserActive {
    int? id;
    String? clinicName;
    String? clinicToken;
    String? url;

    UserActive({
        this.id,
        this.clinicName,
        this.clinicToken,
        this.url,
    });

    factory UserActive.fromJson(Map<String, dynamic> json) => UserActive(
        id: json["id"],
        clinicName: json["clinic_name"],
        clinicToken: json["clinic_token"],
        url: json["url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "clinic_name": clinicName,
        "clinic_token": clinicToken,
        "url": url,
    };
}
