// To parse this JSON data, do
//
//     final vitalsignDataModel = vitalsignDataModelFromJson(jsonString);

import 'dart:convert';

VitalsignDataModel vitalsignDataModelFromJson(String str) => VitalsignDataModel.fromJson(json.decode(str));

String vitalsignDataModelToJson(VitalsignDataModel data) => json.encode(data.toJson());

class VitalsignDataModel {
    String? message;
    Data? data;

    VitalsignDataModel({
        this.message,
        this.data,
    });

    factory VitalsignDataModel.fromJson(Map<String, dynamic> json) => VitalsignDataModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    int? patientVitalSignId;
    int? patientVisitId;
    String? chiefComplaint;
    String? currentMedication;
    String? systolic;
    String? diastolic;
    String? respiratoryRate;
    String? spo2;
    String? pulse;
    String? temperature;
    String? glucose;
    String? height;
    String? weight;
    String? historyOfIllness;
    dynamic isCompleted;

    Data({
        this.patientVitalSignId,
        this.patientVisitId,
        this.chiefComplaint,
        this.currentMedication,
        this.systolic,
        this.diastolic,
        this.respiratoryRate,
        this.spo2,
        this.pulse,
        this.temperature,
        this.glucose,
        this.height,
        this.weight,
        this.historyOfIllness,
        this.isCompleted,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        patientVitalSignId: json["patient_vital_sign_id"],
        patientVisitId: json["patient_visit_id"],
        chiefComplaint: json["chief_complaint"],
        currentMedication: json["current_medication"],
        systolic: json["systolic"],
        diastolic: json["diastolic"],
        respiratoryRate: json["respiratory_rate"],
        spo2: json["spo2"],
        pulse: json["pulse"],
        temperature: json["temperature"],
        glucose: json["glucose"],
        height: json["height"],
        weight: json["weight"],
        historyOfIllness: json["history_of_illness"],
        isCompleted: json["is_completed"],
    );

    Map<String, dynamic> toJson() => {
        "patient_vital_sign_id": patientVitalSignId,
        "patient_visit_id": patientVisitId,
        "chief_complaint": chiefComplaint,
        "current_medication": currentMedication,
        "systolic": systolic,
        "diastolic": diastolic,
        "respiratory_rate": respiratoryRate,
        "spo2": spo2,
        "pulse": pulse,
        "temperature": temperature,
        "glucose": glucose,
        "height": height,
        "weight": weight,
        "history_of_illness": historyOfIllness,
        "is_completed": isCompleted,
    };
}
