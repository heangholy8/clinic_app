// To parse this JSON data, do
//
//     final decryptDataModel = decryptDataModelFromJson(jsonString);

import 'dart:convert';

DecryptDataModel decryptDataModelFromJson(String str) => DecryptDataModel.fromJson(json.decode(str));

String decryptDataModelToJson(DecryptDataModel data) => json.encode(data.toJson());

class DecryptDataModel {
    String? message;
    Data? data;

    DecryptDataModel({
        this.message,
        this.data,
    });

    factory DecryptDataModel.fromJson(Map<String, dynamic> json) => DecryptDataModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    int? id;
    String? objectType;
    int? clinicId;
    String? secret;

    Data({
        this.id,
        this.objectType,
        this.clinicId,
        this.secret,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        objectType: json["object_type"],
        clinicId: json["clinic_id"],
        secret: json["secret"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "object_type": objectType,
        "clinic_id": clinicId,
        "secret": secret,
    };
}
