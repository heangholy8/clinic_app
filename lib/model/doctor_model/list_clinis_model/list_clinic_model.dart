// To parse this JSON data, do
//
//     final listClinicModel = listClinicModelFromJson(jsonString);

import 'dart:convert';

ListClinicModel listClinicModelFromJson(String str) => ListClinicModel.fromJson(json.decode(str));

String listClinicModelToJson(ListClinicModel data) => json.encode(data.toJson());

class ListClinicModel {
    String? message;
    List<DataClinic>? data;

    ListClinicModel({
        this.message,
        this.data,
    });

    factory ListClinicModel.fromJson(Map<String, dynamic> json) => ListClinicModel(
        message: json["message"],
        data: List<DataClinic>.from(json["data"].map((x) => DataClinic.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class DataClinic {
    int? id;
    String? customerName;
    String? secretKey;

    DataClinic({
        this.id,
        this.customerName,
        this.secretKey,
    });

    factory DataClinic.fromJson(Map<String, dynamic> json) => DataClinic(
        id: json["id"],
        customerName: json["customer_name"],
        secretKey: json["secret_key"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "customer_name": customerName,
        "secret_key": secretKey,
    };
}
