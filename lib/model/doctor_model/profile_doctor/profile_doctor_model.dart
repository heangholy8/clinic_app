// To parse this JSON data, do
//
//     final profileDoctorModel = profileDoctorModelFromJson(jsonString);

import 'dart:convert';

ProfileDoctorModel profileDoctorModelFromJson(String str) => ProfileDoctorModel.fromJson(json.decode(str));

String profileDoctorModelToJson(ProfileDoctorModel data) => json.encode(data.toJson());

class ProfileDoctorModel {
    String? message;
    Data? data;

    ProfileDoctorModel({
        this.message,
        this.data,
    });

    factory ProfileDoctorModel.fromJson(Map<String, dynamic> json) => ProfileDoctorModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    int? employeeId;
    int? userId;
    String? title;
    String? titleLatin;
    String? lastname;
    String? firstname;
    String? lastnameLatin;
    String? firstnameLatin;
    String? gender;
    String? dateOfBirth;
    String? personalCardId;
    String? phone;
    String? email;
    ClinicDataClass? nationalityData;
    int? status;
    String? entryDate;
    String? objectType;
    String? address;
    dynamic profileText;
    ClinicDataClass? clinicData;
    dynamic profile;

    Data({
        this.employeeId,
        this.userId,
        this.title,
        this.titleLatin,
        this.lastname,
        this.firstname,
        this.lastnameLatin,
        this.firstnameLatin,
        this.gender,
        this.dateOfBirth,
        this.personalCardId,
        this.phone,
        this.email,
        this.nationalityData,
        this.status,
        this.entryDate,
        this.objectType,
        this.address,
        this.profileText,
        this.clinicData,
        this.profile,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        employeeId: json["employee_id"],
        userId: json["user_id"],
        title: json["title"],
        titleLatin: json["title_latin"],
        lastname: json["lastname"],
        firstname: json["firstname"],
        lastnameLatin: json["lastname_latin"],
        firstnameLatin: json["firstname_latin"],
        gender: json["gender"],
        dateOfBirth: json["date_of_birth"],
        personalCardId: json["personal_card_id"],
        phone: json["phone"],
        email: json["email"],
        nationalityData: json["nationality_data"] == null?null: ClinicDataClass.fromJson(json["nationality_data"]),
        status: json["status"],
        entryDate: json["entry_date"],
        objectType: json["object_type"],
        address: json["address"],
        profileText: json["profile_text"],
        clinicData: json["clinic_data"] == null?null: ClinicDataClass.fromJson(json["clinic_data"]),
        profile: json["profile"],
    );

    Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "user_id": userId,
        "title": title,
        "title_latin": titleLatin,
        "lastname": lastname,
        "firstname": firstname,
        "lastname_latin": lastnameLatin,
        "firstname_latin": firstnameLatin,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "personal_card_id": personalCardId,
        "phone": phone,
        "email": email,
        "nationality_data": nationalityData ==null?null:nationalityData!.toJson(),
        "status": status,
        "entry_date": entryDate,
        "object_type": objectType,
        "address": address,
        "profile_text": profileText,
        "clinic_data": clinicData == null?null:clinicData!.toJson(),
        "profile": profile,
    };
}

class ClinicDataClass {
    int? id;
    String? name;
    String? nameLatin;

    ClinicDataClass({
        this.id,
        this.name,
        this.nameLatin,
    });

    factory ClinicDataClass.fromJson(Map<String, dynamic> json) => ClinicDataClass(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
    };
}
