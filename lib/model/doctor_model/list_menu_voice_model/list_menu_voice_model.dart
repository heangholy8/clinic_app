// To parse this JSON data, do
//
//     final listMenuVoiceModel = listMenuVoiceModelFromJson(jsonString);

import 'dart:convert';

ListMenuVoiceModel listMenuVoiceModelFromJson(String str) => ListMenuVoiceModel.fromJson(json.decode(str));

String listMenuVoiceModelToJson(ListMenuVoiceModel data) => json.encode(data.toJson());

class ListMenuVoiceModel {
    String? message;
    ListMenuVoiceModelData? data;

    ListMenuVoiceModel({
        this.message,
        this.data,
    });

    factory ListMenuVoiceModel.fromJson(Map<String, dynamic> json) => ListMenuVoiceModel(
        message: json["message"],
        data: ListMenuVoiceModelData.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
    };
}

class ListMenuVoiceModelData {
    int? objectId;
    List<Datum>? data;

    ListMenuVoiceModelData({
        this.objectId,
        this.data,
    });

    factory ListMenuVoiceModelData.fromJson(Map<String, dynamic> json) => ListMenuVoiceModelData(
        objectId: json["object_id"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "object_id": objectId,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    bool isExpand;
    String? name;
    List<Child>? child;

    Datum({
        this.name,
        this.child,
        this.isExpand = false,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        name: json["name"],
        child: List<Child>.from(json["child"].map((x) => Child.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "child": List<dynamic>.from(child!.map((x) => x.toJson())),
    };
}

class Child {
    String? objectType;
    String? name;
    List<ChildData>? data;

    Child({
        this.objectType,
        this.name,
        this.data,
    });

    factory Child.fromJson(Map<String, dynamic> json) => Child(
        objectType: json["object_type"],
        name: json["name"],
        data: List<ChildData>.from(json["data"].map((x) => ChildData.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "object_type": objectType,
        "name": name,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class ChildData {
    int? id;
    String? fileOriginalName;
    int? fileSize;
    String? fileType;
    String? fileThumbnail;
    String? fileDisplay;
    int? objectId;
    String? objectType;
    int? clinicId;

    ChildData({
        this.id,
        this.fileOriginalName,
        this.fileSize,
        this.fileType,
        this.fileThumbnail,
        this.fileDisplay,
        this.objectId,
        this.objectType,
        this.clinicId,
    });

    factory ChildData.fromJson(Map<String, dynamic> json) => ChildData(
        id: json["id"],
        fileOriginalName: json["file_original_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileThumbnail: json["file_thumbnail"],
        fileDisplay: json["file_display"],
        objectId: json["object_id"],
        objectType: json["object_type"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_original_name": fileOriginalName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_thumbnail": fileThumbnail,
        "file_display": fileDisplay,
        "object_id": objectId,
        "object_type": objectType,
        "clinic_id": clinicId,
    };
}
