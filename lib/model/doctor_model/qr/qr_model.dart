// To parse this JSON data, do
//
//     final qrModel = qrModelFromJson(jsonString);

import 'dart:convert';

QrModel qrModelFromJson(String str) => QrModel.fromJson(json.decode(str));

String qrModelToJson(QrModel data) => json.encode(data.toJson());

class QrModel {
    int? id;
    String? objectType;
    int? clinicId;

    QrModel({
        this.id,
        this.objectType,
        this.clinicId,
    });

    factory QrModel.fromJson(Map<String, dynamic> json) => QrModel(
        id: json["id"],
        objectType: json["object_type"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "object_type": objectType,
        "clinic_id": clinicId,
    };
}
