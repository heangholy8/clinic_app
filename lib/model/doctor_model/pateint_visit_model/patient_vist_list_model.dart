// To parse this JSON data, do
//
//     final listPatientVisitModel = listPatientVisitModelFromJson(jsonString);

import 'dart:convert';

ListPatientVisitModel listPatientVisitModelFromJson(String str) => ListPatientVisitModel.fromJson(json.decode(str));

String listPatientVisitModelToJson(ListPatientVisitModel data) => json.encode(data.toJson());

class ListPatientVisitModel {
    String? message;
    List<DataPatientVisit>? data;
    List<VisitStatusDatum>? visitStatusData;

    ListPatientVisitModel({
        this.message,
        this.data,
        this.visitStatusData,
    });

    factory ListPatientVisitModel.fromJson(Map<String, dynamic> json) => ListPatientVisitModel(
        message: json["message"],
        data: List<DataPatientVisit>.from(json["data"].map((x) => DataPatientVisit.fromJson(x))),
        visitStatusData: List<VisitStatusDatum>.from(json["visit_status_data"].map((x) => VisitStatusDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "visit_status_data": List<dynamic>.from(visitStatusData!.map((x) => x.toJson())),
    };
}

class DataPatientVisit {
    dynamic patientVisitId;
    dynamic status;
    dynamic visitDate;
    bool? isVisited;
    dynamic visitCode;
    dynamic patientVisitNumber;
    dynamic doctorVisitRoom;
    dynamic note;
    dynamic referDoctorDate;
    PatientData? patientData;
    Data? departmentData;

    DataPatientVisit({
        this.patientVisitId,
        this.status,
        this.visitDate,
        this.isVisited,
        this.visitCode,
        this.patientVisitNumber,
        this.doctorVisitRoom,
        this.note,
        this.referDoctorDate,
        this.patientData,
        this.departmentData,
    });

    factory DataPatientVisit.fromJson(Map<String, dynamic> json) => DataPatientVisit(
        patientVisitId: json["patient_visit_id"],
        status: json["status"],
        visitDate:json["visit_date"],
        isVisited: json["is_visited"],
        visitCode: json["visit_code"],
        patientVisitNumber: json["patient_visit_number"],
        doctorVisitRoom: json["doctor_visit_room"],
        note: json["note"],
        referDoctorDate: json["refer_doctor_date"],
        patientData: json["patient_data"] == null?null:PatientData.fromJson(json["patient_data"]),
        departmentData: json["department_data"] == null?null:Data.fromJson(json["department_data"]),
    );

    Map<String, dynamic> toJson() => {
        "patient_visit_id": patientVisitId,
        "status": status,
        "visit_date": visitDate,
        "is_visited": isVisited,
        "visit_code": visitCode,
        "patient_visit_number": patientVisitNumber,
        "doctor_visit_room": doctorVisitRoom,
        "note": note,
        "refer_doctor_date": referDoctorDate,
        "patient_data": patientData ==null?null: patientData!.toJson(),
        "department_data": departmentData ==null?null:departmentData!.toJson(),
    };
}

class Data {
    int? id;
    String? name;
    String? nameLatin;

    Data({
        this.id,
        this.name,
        this.nameLatin,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
    };
}

class PatientData {
    int? id;
    String? personalCardId;
    String? registreredNumber;
    String? registreredOn;
    String? fullname;
    String? fullnameLatin;
    dynamic gender;
    String? dateOfBirth;
    dynamic age;
    String? phone;
    dynamic isNssfMember;
    dynamic nssfNumber;
    int? isForeigner;
    dynamic emergencyContact;
    Data? bloodGroupTypeData;
    Profile? profile;
    String? qrCode;

    PatientData({
        this.id,
        this.personalCardId,
        this.registreredNumber,
        this.registreredOn,
        this.fullname,
        this.fullnameLatin,
        this.gender,
        this.dateOfBirth,
        this.age,
        this.phone,
        this.isNssfMember,
        this.nssfNumber,
        this.isForeigner,
        this.emergencyContact,
        this.bloodGroupTypeData,
        this.profile,
        this.qrCode,
    });

    factory PatientData.fromJson(Map<String, dynamic> json) => PatientData(
        id: json["id"],
        personalCardId: json["personal_card_id"],
        registreredNumber: json["registrered_number"],
        registreredOn: json["registrered_on"],
        fullname: json["fullname"],
        fullnameLatin: json["fullname_latin"],
        gender: json["gender"],
        dateOfBirth:json["date_of_birth"],
        age: json["age"],
        phone: json["phone"],
        isNssfMember: json["is_nssf_member"],
        nssfNumber: json["nssf_number"],
        isForeigner: json["is_foreigner"],
        emergencyContact: json["emergency_contact"],
        bloodGroupTypeData: json["blood_group_type_data"] ==null?null: Data.fromJson(json["blood_group_type_data"]),
        profile: json["profile"] ==null?null: Profile.fromJson(json["profile"]),
        qrCode: json["qr_code"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "personal_card_id": personalCardId,
        "registrered_number": registreredNumber,
        "registrered_on": registreredOn,
        "fullname": fullname,
        "fullname_latin": fullnameLatin,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "age": age,
        "phone": phone,
        "is_nssf_member": isNssfMember,
        "nssf_number": nssfNumber,
        "is_foreigner": isForeigner,
        "emergency_contact": emergencyContact,
        "blood_group_type_data": bloodGroupTypeData ==null?null: bloodGroupTypeData!.toJson(),
        "profile": profile == null?null:profile!.toJson(),
        "qr_code": qrCode,
    };
}

class Profile {
    int? id;
    String? fileOriginalName;
    int? fileSize;
    String? fileType;
    String? fileThumbnail;
    String? fileDisplay;
    int? objectId;
    String? objectType;
    String? createdAt;
    int? clinicId;

    Profile({
        this.id,
        this.fileOriginalName,
        this.fileSize,
        this.fileType,
        this.fileThumbnail,
        this.fileDisplay,
        this.objectId,
        this.objectType,
        this.createdAt,
        this.clinicId,
    });

    factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"],
        fileOriginalName: json["file_original_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileThumbnail: json["file_thumbnail"],
        fileDisplay: json["file_display"],
        objectId: json["object_id"],
        objectType: json["object_type"],
        createdAt: json["created_at"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_original_name": fileOriginalName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_thumbnail": fileThumbnail,
        "file_display": fileDisplay,
        "object_id": objectId,
        "object_type": objectType,
        "created_at": createdAt,
        "clinic_id": clinicId,
    };
}

class VisitStatusDatum {
    int? id;
    String? name;
    String? nameLatin;
    String? value;
    String? icon;
    String? color;
    String? objectType;

    VisitStatusDatum({
        this.id,
        this.name,
        this.nameLatin,
        this.value,
        this.icon,
        this.color,
        this.objectType,
    });

    factory VisitStatusDatum.fromJson(Map<String, dynamic> json) => VisitStatusDatum(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
        value: json["value"],
        icon: json["icon"],
        color: json["color"],
        objectType: json["object_type"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
        "value": value,
        "icon": icon,
        "color": color,
        "object_type": objectType,
    };
}
