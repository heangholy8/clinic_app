class AppointmentModel {
  late bool isDisplay;
  final String profilePic;
  final String name;
  final String type;
  final String discription;
  final String date;
  AppointmentModel({required this.date, required this.name, required this.profilePic, required this.type, required this.discription,required this.isDisplay});
}
List<AppointmentModel> allAppointment=[
  AppointmentModel(isDisplay:true, date:'Today . 2 - 3pm',name: 'Dr. Chenda San',type: "Genaral Doctor",discription: "Follow up on the last diagnoses on the stomach ache last time.",profilePic: "https://img.fixthephoto.com/blog/images/gallery/news_preview_mob_image__preview_11368.png",),
  AppointmentModel(isDisplay:false, date:'Wen May 10 .  9 - 10am',name: 'Dr. Roger Kim',type: "Surgent",discription: "Re-diagnoses",profilePic: "https://images.ctfassets.net/h6goo9gw1hh6/2sNZtFAWOdP1lmQ33VwRN3/24e953b920a9cd0ff2e1d587742a2472/1-intro-photo-final.jpg?w=1200&h=992&fl=progressive&q=70&fm=jpg",),
];