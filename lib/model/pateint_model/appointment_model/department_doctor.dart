// To parse this JSON data, do
//
//     final departmentDoctorModel = departmentDoctorModelFromJson(jsonString);

import 'dart:convert';

DepartmentDoctorModel departmentDoctorModelFromJson(String str) => DepartmentDoctorModel.fromJson(json.decode(str));

String departmentDoctorModelToJson(DepartmentDoctorModel data) => json.encode(data.toJson());

class DepartmentDoctorModel {
    List<Datum>? data;

    DepartmentDoctorModel({
        this.data,
    });

    factory DepartmentDoctorModel.fromJson(Map<String, dynamic> json) => DepartmentDoctorModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    int? employeeId;
    int? userId;
    String? title;
    String? titleLatin;
    String? lastname;
    String? firstname;
    String? lastnameLatin;
    String? firstnameLatin;
    String? gender;
    String? dateOfBirth;
    String? personalCardId;
    String? phone;
    String? email;
    dynamic nationalityData;
    int? status;
    String? entryDate;
    String? objectType;
    String? address;
    String? profileText;
    ClinicData? clinicData;
    Profile? profile;

    Datum({
        this.employeeId,
        this.userId,
        this.title,
        this.titleLatin,
        this.lastname,
        this.firstname,
        this.lastnameLatin,
        this.firstnameLatin,
        this.gender,
        this.dateOfBirth,
        this.personalCardId,
        this.phone,
        this.email,
        this.nationalityData,
        this.status,
        this.entryDate,
        this.objectType,
        this.address,
        this.profileText,
        this.clinicData,
        this.profile,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        employeeId: json["employee_id"],
        userId: json["user_id"],
        title: json["title"],
        titleLatin: json["title_latin"],
        lastname: json["lastname"],
        firstname: json["firstname"],
        lastnameLatin: json["lastname_latin"],
        firstnameLatin: json["firstname_latin"],
        gender: json["gender"],
        dateOfBirth:json["date_of_birth"],
        personalCardId: json["personal_card_id"],
        phone: json["phone"],
        email: json["email"],
        nationalityData: json["nationality_data"],
        status: json["status"],
        entryDate:json["entry_date"],
        objectType: json["object_type"],
        address: json["address"],
        profileText: json["profile_text"],
        clinicData: json["clinic_data"] == null?null: ClinicData.fromJson(json["clinic_data"]),
        profile: json["profile"] == null?null: Profile.fromJson(json["profile"]),
    );

    Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "user_id": userId,
        "title": title,
        "title_latin": titleLatin,
        "lastname": lastname,
        "firstname": firstname,
        "lastname_latin": lastnameLatin,
        "firstname_latin": firstnameLatin,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "personal_card_id": personalCardId,
        "phone": phone,
        "email": email,
        "nationality_data": nationalityData,
        "status": status,
        "entry_date": entryDate,
        "object_type": objectType,
        "address": address,
        "profile_text": profileText,
        "clinic_data": clinicData == null?null:clinicData!.toJson(),
        "profile": profile == null?null:profile!.toJson(),
    };
}

class ClinicData {
    int? id;
    String? name;
    String? nameLatin;

    ClinicData({
        this.id,
        this.name,
        this.nameLatin,
    });

    factory ClinicData.fromJson(Map<String, dynamic> json) => ClinicData(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
    };
}

class Profile {
    int? id;
    String? fileOriginalName;
    int? fileSize;
    String? fileType;
    String? fileThumbnail;
    String? fileDisplay;
    int? objectId;
    String? objectType;
    String? createdAt;
    int? clinicId;

    Profile({
        this.id,
        this.fileOriginalName,
        this.fileSize,
        this.fileType,
        this.fileThumbnail,
        this.fileDisplay,
        this.objectId,
        this.objectType,
        this.createdAt,
        this.clinicId,
    });

    factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"],
        fileOriginalName: json["file_original_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileThumbnail: json["file_thumbnail"],
        fileDisplay: json["file_display"],
        objectId: json["object_id"],
        objectType: json["object_type"],
        createdAt: json["created_at"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_original_name": fileOriginalName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_thumbnail": fileThumbnail,
        "file_display": fileDisplay,
        "object_id": objectId,
        "object_type": objectType,
        "created_at": createdAt,
        "clinic_id": clinicId,
    };
}
