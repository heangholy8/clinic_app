// To parse this JSON data, do
//
//     final appointMentTypeModel = appointMentTypeModelFromJson(jsonString);

import 'dart:convert';

AppointMentTypeModel appointMentTypeModelFromJson(String str) => AppointMentTypeModel.fromJson(json.decode(str));

String appointMentTypeModelToJson(AppointMentTypeModel data) => json.encode(data.toJson());

class AppointMentTypeModel {
    List<Message>? message;

    AppointMentTypeModel({
        this.message,
    });

    factory AppointMentTypeModel.fromJson(Map<String, dynamic> json) => AppointMentTypeModel(
        message: List<Message>.from(json["message"].map((x) => Message.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": List<dynamic>.from(message!.map((x) => x.toJson())),
    };
}

class Message {
    int? id;
    String? name;
    String? nameLatin;

    Message({
        this.id,
        this.name,
        this.nameLatin,
    });

    factory Message.fromJson(Map<String, dynamic> json) => Message(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
    };
}
