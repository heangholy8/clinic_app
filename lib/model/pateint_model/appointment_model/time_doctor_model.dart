// To parse this JSON data, do
//
//     final timeDoctorModel = timeDoctorModelFromJson(jsonString);

import 'dart:convert';

TimeDoctorModel timeDoctorModelFromJson(String str) => TimeDoctorModel.fromJson(json.decode(str));

String timeDoctorModelToJson(TimeDoctorModel data) => json.encode(data.toJson());

class TimeDoctorModel {
    String? message;
    Data? data;

    TimeDoctorModel({
        this.message,
        this.data,
    });

    factory TimeDoctorModel.fromJson(Map<String, dynamic> json) => TimeDoctorModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    int? employeeId;
    String? fullname;
    String? fullnameLatin;
    String? phone;
    int? status;
    List<AvailableTime>? availableTime;

    Data({
        this.employeeId,
        this.fullname,
        this.fullnameLatin,
        this.phone,
        this.status,
        this.availableTime,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        employeeId: json["employee_id"],
        fullname: json["fullname"],
        fullnameLatin: json["fullname_latin"],
        phone: json["phone"],
        status: json["status"],
        availableTime: List<AvailableTime>.from(json["available_time"].map((x) => AvailableTime.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "fullname": fullname,
        "fullname_latin": fullnameLatin,
        "phone": phone,
        "status": status,
        "available_time": List<dynamic>.from(availableTime!.map((x) => x.toJson())),
    };
}

class AvailableTime {
    String? timeStr;

    AvailableTime({
        this.timeStr,
    });

    factory AvailableTime.fromJson(Map<String, dynamic> json) => AvailableTime(
        timeStr: json["time_str"],
    );

    Map<String, dynamic> toJson() => {
        "time_str": timeStr,
    };
}
