// To parse this JSON data, do
//
//     final departmentModel = departmentModelFromJson(jsonString);

import 'dart:convert';

DepartmentModel departmentModelFromJson(String str) => DepartmentModel.fromJson(json.decode(str));

String departmentModelToJson(DepartmentModel data) => json.encode(data.toJson());

class DepartmentModel {
    List<Message>? message;

    DepartmentModel({
        this.message,
    });

    factory DepartmentModel.fromJson(Map<String, dynamic> json) => DepartmentModel(
        message: List<Message>.from(json["message"].map((x) => Message.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": List<dynamic>.from(message!.map((x) => x.toJson())),
    };
}

class Message {
    int? id;
    String? name;
    String? departmentKey;
    String? nameLatin;
    String? redirectUrl;

    Message({
        this.id,
        this.name,
        this.departmentKey,
        this.nameLatin,
        this.redirectUrl,
    });

    factory Message.fromJson(Map<String, dynamic> json) => Message(
        id: json["id"],
        name: json["name"],
        departmentKey: json["department_key"],
        nameLatin: json["name_latin"],
        redirectUrl: json["redirect_url"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "department_key": departmentKey,
        "name_latin": nameLatin,
        "redirect_url": redirectUrl,
    };
}
