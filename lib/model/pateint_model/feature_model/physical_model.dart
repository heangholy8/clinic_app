// To parse this JSON data, do
//
//     final physicalHistory = physicalHistoryFromJson(jsonString);

import 'dart:convert';

PhysicalHistory physicalHistoryFromJson(String str) => PhysicalHistory.fromJson(json.decode(str));

String physicalHistoryToJson(PhysicalHistory data) => json.encode(data.toJson());

class PhysicalHistory {
    String? message;
    List<Datum>? data;

    PhysicalHistory({
        this.message,
        this.data,
    });

    factory PhysicalHistory.fromJson(Map<String, dynamic> json) => PhysicalHistory(
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    bool isExpand;
    dynamic date;
    dynamic presentIllness;
    dynamic other;
    dynamic clinicFeature;
    dynamic clinicalFeatureTypeValue;
    dynamic generalAppearance;
    dynamic generalAppearanceTypeValue;
    dynamic ent;
    dynamic entTypeValue;
    dynamic cardiovascularSystem;
    dynamic cardiovascularSystemTypeValue;
    dynamic respiratorySystem;
    dynamic respiratorySystemTypeValue;
    dynamic gastroIntestinalSystem;
    dynamic gastroIntestinalSystemTypeValue;
    dynamic urogenitalSystem;
    dynamic urogenitalSystemTypeValue;
    dynamic centralNervousSystem;
    dynamic centralNervousSystemTypeValue;
    dynamic skinExtremity;
    dynamic skinExtremityTypeValue;
    dynamic mentalStatus;
    dynamic mentalStatusTypeValue;
    dynamic glasgowComaScale;
    dynamic glasgowComaScaleTypeValue;
    dynamic abdomen;
    dynamic abdomenTypeValue;

    Datum({
      this.isExpand = false,
        this.date,
        this.presentIllness,
        this.other,
        this.clinicFeature,
        this.clinicalFeatureTypeValue,
        this.generalAppearance,
        this.generalAppearanceTypeValue,
        this.ent,
        this.entTypeValue,
        this.cardiovascularSystem,
        this.cardiovascularSystemTypeValue,
        this.respiratorySystem,
        this.respiratorySystemTypeValue,
        this.gastroIntestinalSystem,
        this.gastroIntestinalSystemTypeValue,
        this.urogenitalSystem,
        this.urogenitalSystemTypeValue,
        this.centralNervousSystem,
        this.centralNervousSystemTypeValue,
        this.skinExtremity,
        this.skinExtremityTypeValue,
        this.mentalStatus,
        this.mentalStatusTypeValue,
        this.glasgowComaScale,
        this.glasgowComaScaleTypeValue,
        this.abdomen,
        this.abdomenTypeValue,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        date: json["date"],
        presentIllness: json["present_illness"],
        other: json["other"],
        clinicFeature: json["clinic_feature"],
        clinicalFeatureTypeValue: json["clinical_feature_type_value"],
        generalAppearance: json["general_appearance"],
        generalAppearanceTypeValue: json["general_appearance_type_value"],
        ent: json["ent"],
        entTypeValue: json["ent_type_value"],
        cardiovascularSystem: json["cardiovascular_system"],
        cardiovascularSystemTypeValue: json["cardiovascular_system_type_value"],
        respiratorySystem: json["respiratory_system"],
        respiratorySystemTypeValue: json["respiratory_system_type_value"],
        gastroIntestinalSystem: json["gastro_intestinal_system"],
        gastroIntestinalSystemTypeValue: json["gastro_intestinal_system_type_value"],
        urogenitalSystem: json["urogenital_system"],
        urogenitalSystemTypeValue: json["urogenital_system_type_value"],
        centralNervousSystem: json["central_nervous_system"],
        centralNervousSystemTypeValue: json["central_nervous_system_type_value"],
        skinExtremity: json["skin_extremity"],
        skinExtremityTypeValue: json["skin_extremity_type_value"],
        mentalStatus: json["mental_status"],
        mentalStatusTypeValue: json["mental_status_type_value"],
        glasgowComaScale: json["glasgow_coma_scale"],
        glasgowComaScaleTypeValue: json["glasgow_coma_scale_type_value"],
        abdomen: json["abdomen"],
        abdomenTypeValue: json["abdomen_type_value"],
    );

    Map<String, dynamic> toJson() => {
        "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "present_illness": presentIllness,
        "other": other,
        "clinic_feature": clinicFeature,
        "clinical_feature_type_value": clinicalFeatureTypeValue,
        "general_appearance": generalAppearance,
        "general_appearance_type_value": generalAppearanceTypeValue,
        "ent": ent,
        "ent_type_value": entTypeValue,
        "cardiovascular_system": cardiovascularSystem,
        "cardiovascular_system_type_value": cardiovascularSystemTypeValue,
        "respiratory_system": respiratorySystem,
        "respiratory_system_type_value": respiratorySystemTypeValue,
        "gastro_intestinal_system": gastroIntestinalSystem,
        "gastro_intestinal_system_type_value": gastroIntestinalSystemTypeValue,
        "urogenital_system": urogenitalSystem,
        "urogenital_system_type_value": urogenitalSystemTypeValue,
        "central_nervous_system": centralNervousSystem,
        "central_nervous_system_type_value": centralNervousSystemTypeValue,
        "skin_extremity": skinExtremity,
        "skin_extremity_type_value": skinExtremityTypeValue,
        "mental_status": mentalStatus,
        "mental_status_type_value": mentalStatusTypeValue,
        "glasgow_coma_scale": glasgowComaScale,
        "glasgow_coma_scale_type_value": glasgowComaScaleTypeValue,
        "abdomen": abdomen,
        "abdomen_type_value": abdomenTypeValue,
    };
}
