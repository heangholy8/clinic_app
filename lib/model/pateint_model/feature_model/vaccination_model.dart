// To parse this JSON data, do
//
//     final vaccinationModel = vaccinationModelFromJson(jsonString);

import 'dart:convert';

VaccinationModel vaccinationModelFromJson(String str) => VaccinationModel.fromJson(json.decode(str));

String vaccinationModelToJson(VaccinationModel data) => json.encode(data.toJson());

class VaccinationModel {
    String? message;
    List<Datum>? data;

    VaccinationModel({
        this.message,
        this.data,
    });

    factory VaccinationModel.fromJson(Map<String, dynamic> json) => VaccinationModel(
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    bool isExpand;
    int? patientVaccinationId;
    String? dosage;
    String? locationSite;
    String? lotNumber;
    String? note;
    dynamic price;
    String? vaccinationDate;
    VaccinationSheetData? vaccinationSheetData;
    int? administratorId;
    AdministratorData? administratorData;
    int? isCompleted;

    Datum({
        this.isExpand = false,
        this.patientVaccinationId,
        this.dosage,
        this.locationSite,
        this.lotNumber,
        this.note,
        this.price,
        this.vaccinationDate,
        this.vaccinationSheetData,
        this.administratorId,
        this.administratorData,
        this.isCompleted,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        patientVaccinationId: json["patient_vaccination_id"],
        dosage: json["dosage"],
        locationSite: json["location_site"],
        lotNumber: json["lot_number"],
        note: json["note"],
        price: json["price"],
        vaccinationDate: json["vaccination_date"],
        vaccinationSheetData: VaccinationSheetData.fromJson(json["vaccination_sheet_data"]),
        administratorId: json["administrator_id"],
        administratorData: AdministratorData.fromJson(json["administrator_data"]),
        isCompleted: json["is_completed"],
    );

    Map<String, dynamic> toJson() => {
        "patient_vaccination_id": patientVaccinationId,
        "dosage": dosage,
        "location_site": locationSite,
        "lot_number": lotNumber,
        "note": note,
        "price": price,
        "vaccination_date": vaccinationDate,
        "vaccination_sheet_data": vaccinationSheetData!.toJson(),
        "administrator_id": administratorId,
        "administrator_data": administratorData!.toJson(),
        "is_completed": isCompleted,
    };
}

class AdministratorData {
    int? employeeId;
    String? fullname;
    String? fullnameLatin;
    String? personalCardId;
    String? phone;
    String? email;
    String? objectType;
    dynamic profile;

    AdministratorData({
        this.employeeId,
        this.fullname,
        this.fullnameLatin,
        this.personalCardId,
        this.phone,
        this.email,
        this.objectType,
        this.profile,
    });

    factory AdministratorData.fromJson(Map<String, dynamic> json) => AdministratorData(
        employeeId: json["employee_id"],
        fullname: json["fullname"],
        fullnameLatin: json["fullname_latin"],
        personalCardId: json["personal_card_id"],
        phone: json["phone"],
        email: json["email"],
        objectType: json["object_type"],
        profile: json["profile"],
    );

    Map<String, dynamic> toJson() => {
        "employee_id": employeeId,
        "fullname": fullname,
        "fullname_latin": fullnameLatin,
        "personal_card_id": personalCardId,
        "phone": phone,
        "email": email,
        "object_type": objectType,
        "profile": profile,
    };
}

class VaccinationSheetData {
    int? id;
    String? name;
    String? nameLatin;

    VaccinationSheetData({
        this.id,
        this.name,
        this.nameLatin,
    });

    factory VaccinationSheetData.fromJson(Map<String, dynamic> json) => VaccinationSheetData(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
    };
}
