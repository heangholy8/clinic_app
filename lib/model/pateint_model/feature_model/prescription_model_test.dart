class PrescriptionHistoryModelModel {
  late bool isCheck;
  final String date;
  final List<prescriptionItem> prescription;
  PrescriptionHistoryModelModel({required this.date,required this.prescription, required this.isCheck,});
}
List<PrescriptionHistoryModelModel> allPrescription=[
  PrescriptionHistoryModelModel(isCheck:true, date:'14/05/2024',prescription: [ prescriptionItem(image: "https://www.thoughtco.com/thmb/VpsMx-RPud3-yIv2uleQJmZKtPU=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/pink-tablet-169947044-579cb31f5f9b589aa91f9d50.jpg", total: 2, name: "Pepto-Bismol (Bismuth subsalicylate)"),prescriptionItem(image: "https://cdn.britannica.com/30/130830-050-6D88060B/Tums-calcium-carbonate-ingredient.jpg", total: 5, name: "Tums (Calcium carbonate)"),prescriptionItem(image: "https://southstardrug.com.ph/cdn/shop/products/30786_1_800x.jpg?v=1711962595", total: 10, name: "Imodium (Loperamide)"),prescriptionItem(image: "https://cloudfront-us-east-2.images.arcpublishing.com/reuters/O2S3EWIA4NIS7ED7N4YMFZDKRM.jpg", total: 1, name: "Zantac (Ranitidine)"),prescriptionItem(image: "https://www.shopmassystoresgy.com/wp-content/uploads/2021/01/0000000084701-rotated.jpg", total: 3, name: "Gas-X (Simethicone)")],),
];


class prescriptionItem {
  final String image;
  final int total;
  final String name;
  prescriptionItem({required this.image,required this.total, required this.name,});
}