// To parse this JSON data, do
//
//     final vitalSingModel = vitalSingModelFromJson(jsonString);

import 'dart:convert';

VitalSingModel vitalSingModelFromJson(String str) => VitalSingModel.fromJson(json.decode(str));

String vitalSingModelToJson(VitalSingModel data) => json.encode(data.toJson());

class VitalSingModel {
    String? message;
    List<Datum>? data;


    VitalSingModel({
        this.message,
        this.data,
    });

    factory VitalSingModel.fromJson(Map<String, dynamic> json) => VitalSingModel(
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    bool isExpand;
    String? data;
    String? chiefComplaint;
    String? currentMedication;
    String? systolic;
    String? diastolic;
    String? respiratoryRate;
    String? spo2;
    String? pulse;
    String? temperature;
    String? glucose;
    String? height;
    String? weight;
    String? historyOfIllness;

    Datum({
      this.isExpand = false,
        this.data,
        this.chiefComplaint,
        this.currentMedication,
        this.systolic,
        this.diastolic,
        this.respiratoryRate,
        this.spo2,
        this.pulse,
        this.temperature,
        this.glucose,
        this.height,
        this.weight,
        this.historyOfIllness,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        data: json["data"],
        chiefComplaint: json["chief_complaint"],
        currentMedication: json["current_medication"],
        systolic: json["systolic"],
        diastolic: json["diastolic"],
        respiratoryRate: json["respiratory_rate"],
        spo2: json["spo2"],
        pulse: json["pulse"],
        temperature: json["temperature"],
        glucose: json["glucose"],
        height: json["height"],
        weight: json["weight"],
        historyOfIllness: json["history_of_illness"],
    );

    Map<String, dynamic> toJson() => {
        "data": data,
        "chief_complaint": chiefComplaint,
        "current_medication": currentMedication,
        "systolic": systolic,
        "diastolic": diastolic,
        "respiratory_rate": respiratoryRate,
        "po2": spo2,
        "pulse": pulse,
        "temperature": temperature,
        "glucose": glucose,
        "height": height,
        "weight": weight,
        "history_of_illness": historyOfIllness,
    };
}
