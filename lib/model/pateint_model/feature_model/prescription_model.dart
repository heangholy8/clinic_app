// To parse this JSON data, do
//
//     final prescriptionModel = prescriptionModelFromJson(jsonString);

import 'dart:convert';

PrescriptionModel prescriptionModelFromJson(String str) => PrescriptionModel.fromJson(json.decode(str));

String prescriptionModelToJson(PrescriptionModel data) => json.encode(data.toJson());

class PrescriptionModel {
    String? message;
    List<Datum>? data;

    PrescriptionModel({
        this.message,
        this.data,
    });

    factory PrescriptionModel.fromJson(Map<String, dynamic> json) => PrescriptionModel(
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    String? date;
    List<PrescriptionItemDatum>? prescriptionItemData;
    bool isExpand;

    Datum({
        this.date,
        this.prescriptionItemData,
        this.isExpand = false,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        date: json["date"],
        prescriptionItemData: List<PrescriptionItemDatum>.from(json["prescription_item_data"].map((x) => PrescriptionItemDatum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "date": date,
        "prescription_item_data": List<dynamic>.from(prescriptionItemData!.map((x) => x.toJson())),
    };
}

class PrescriptionItemDatum {
    Medication? medication;
    int? quantity;
    String? dosageMorning;
    DosageNightForm? unitMorning;
    String? dosageAfternoon;
    DosageNightForm? unitAlternoon;
    String? dosageEvening;
    DosageNightForm? unitEvening;
    String? dosageNight;
    DosageNightForm? dosageNightForm;
    DurationData? durationData;

    PrescriptionItemDatum({
        this.medication,
        this.quantity,
        this.dosageMorning,
        this.unitMorning,
        this.dosageAfternoon,
        this.unitAlternoon,
        this.dosageEvening,
        this.unitEvening,
        this.dosageNight,
        this.dosageNightForm,
        this.durationData,
    });

    factory PrescriptionItemDatum.fromJson(Map<String, dynamic> json) => PrescriptionItemDatum(
        medication: Medication.fromJson(json["medication"]),
        quantity: json["quantity"],
        dosageMorning: json["dosage_morning"],
        unitMorning: DosageNightForm.fromJson(json["unit_morning"]),
        dosageAfternoon: json["dosage_afternoon"],
        unitAlternoon: DosageNightForm.fromJson(json["unit_alternoon"]),
        dosageEvening: json["dosage_evening"],
        unitEvening: DosageNightForm.fromJson(json["unit_evening"]),
        dosageNight: json["dosage_night"],
        dosageNightForm: DosageNightForm.fromJson(json["dosage_night_form"]),
        durationData: DurationData.fromJson(json["duration_data"]),
    );

    Map<String, dynamic> toJson() => {
        "medication": medication!.toJson(),
        "quantity": quantity,
        "dosage_morning": dosageMorning,
        "unit_morning": unitMorning!.toJson(),
        "dosage_afternoon": dosageAfternoon,
        "unit_alternoon": unitAlternoon!.toJson(),
        "dosage_evening": dosageEvening,
        "unit_evening": unitEvening!.toJson(),
        "dosage_night": dosageNight,
        "dosage_night_form": dosageNightForm!.toJson(),
        "duration_data": durationData!.toJson(),
    };
}

class DosageNightForm {
    String? name;
    String? nameLantin;

    DosageNightForm({
        this.name,
        this.nameLantin,
    });

    factory DosageNightForm.fromJson(Map<String, dynamic> json) => DosageNightForm(
        name: json["name"],
        nameLantin: json["name_lantin"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "name_lantin": nameLantin,
    };
}

class DurationData {
    int? id;
    String? name;
    String? nameLatin;

    DurationData({
        this.id,
        this.name,
        this.nameLatin,
    });

    factory DurationData.fromJson(Map<String, dynamic> json) => DurationData(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
    };
}

class Medication {
    String? code;
    String? name;
    ImageData? imageData;

    Medication({
        this.code,
        this.name,
        this.imageData,
    });

    factory Medication.fromJson(Map<String, dynamic> json) => Medication(
        code: json["code"],
        name: json["name"],
        imageData: json["image_data"] == null?null:ImageData.fromJson(json["image_data"]),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "image_data": imageData==null?null:imageData!.toJson(),
    };
}

class EnumValues<T> {
    Map<String, T> map;
    late Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        reverseMap = map.map((k, v) => MapEntry(v, k));
        return reverseMap;
    }
}

class ImageData {
    int? id;
    String? fileOriginalName;
    int? fileSize;
    String? fileType;
    String? fileThumbnail;
    String? fileDisplay;
    int? objectId;
    String? objectType;
    String? createdAt;
    int? clinicId;

    ImageData({
        this.id,
        this.fileOriginalName,
        this.fileSize,
        this.fileType,
        this.fileThumbnail,
        this.fileDisplay,
        this.objectId,
        this.objectType,
        this.createdAt,
        this.clinicId,
    });

    factory ImageData.fromJson(Map<String, dynamic> json) => ImageData(
        id: json["id"],
        fileOriginalName: json["file_original_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileThumbnail: json["file_thumbnail"],
        fileDisplay: json["file_display"],
        objectId: json["object_id"],
        objectType: json["object_type"],
        createdAt: json["created_at"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_original_name": fileOriginalName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_thumbnail": fileThumbnail,
        "file_display": fileDisplay,
        "object_id": objectId,
        "object_type": objectType,
        "created_at": createdAt,
        "clinic_id": clinicId,
    };
}
