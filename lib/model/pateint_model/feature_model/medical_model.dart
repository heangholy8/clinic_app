// To parse this JSON data, do
//
//     final medicalHistoryModel = medicalHistoryModelFromJson(jsonString);
import 'dart:convert';
import 'dart:ffi';

MedicalHistoryModel medicalHistoryModelFromJson(String str) => MedicalHistoryModel.fromJson(json.decode(str));

String medicalHistoryModelToJson(MedicalHistoryModel data) => json.encode(data.toJson());

class MedicalHistoryModel {
    String? message;
    List<Datum>? data;

    MedicalHistoryModel({
        this.message,
        this.data,
    });

    factory MedicalHistoryModel.fromJson(Map<String, dynamic> json) => MedicalHistoryModel(
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    bool isExpand;
    String? data;
    List<Allergy>? allergies;
    dynamic allergyDescription;
    String? pastMedicalHistory;
    String? socialHistory;
    String? familyHistory;
    String? medications;
    String? reviewOfSystems;

    Datum({
      this.isExpand = false,
        this.data,
        this.allergies,
        this.allergyDescription,
        this.pastMedicalHistory,
        this.socialHistory,
        this.familyHistory,
        this.medications,
        this.reviewOfSystems,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        data: json["data"],
        allergies: List<Allergy>.from(json["allergies"].map((x) => Allergy.fromJson(x))),
        allergyDescription: json["allergy_description"],
        pastMedicalHistory: json["past_medical_history"],
        socialHistory: json["social_history"],
        familyHistory: json["family_history"],
        medications: json["medications"],
        reviewOfSystems: json["review_of_systems"],
    );

    Map<String, dynamic> toJson() => {
        "data": data,
        "allergies": List<dynamic>.from(allergies!.map((x) => x.toJson())),
        "allergy_description": allergyDescription,
        "past_medical_history": pastMedicalHistory,
        "social_history": socialHistory,
        "family_history": familyHistory,
        "medications": medications,
        "review_of_systems": reviewOfSystems,
    };
}

class Allergy {
    int? id;
    String? name;
    String? nameLatin;
    String? categoryName;
    String? categoryNameLatin;

    Allergy({
        this.id,
        this.name,
        this.nameLatin,
        this.categoryName,
        this.categoryNameLatin,
    });

    factory Allergy.fromJson(Map<String, dynamic> json) => Allergy(
        id: json["id"],
        name: json["name"],
        nameLatin: json["name_latin"],
        categoryName: json["category_name"],
        categoryNameLatin: json["category_name_latin"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "name_latin": nameLatin,
        "category_name": categoryName,
        "category_name_latin": categoryNameLatin,
    };
}
