// To parse this JSON data, do
//
//     final investigationModel = investigationModelFromJson(jsonString);

import 'dart:convert';

InvestigationModel investigationModelFromJson(String str) => InvestigationModel.fromJson(json.decode(str));

String investigationModelToJson(InvestigationModel data) => json.encode(data.toJson());

class InvestigationModel {
    String? message;
    Data? data;

    InvestigationModel({
        this.message,
        this.data,
    });

    factory InvestigationModel.fromJson(Map<String, dynamic> json) => InvestigationModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
    };
}

class Data {
    List<Imaging>? labor;
    List<Imaging>? imaging;

    Data({
        this.labor,
        this.imaging,
    });

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        labor: List<Imaging>.from(json["labor"].map((x) => Imaging.fromJson(x))),
        imaging: List<Imaging>.from(json["imaging"].map((x) => Imaging.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "labor": List<dynamic>.from(labor!.map((x) => x.toJson())),
        "imaging": List<dynamic>.from(imaging!.map((x) => x.toJson())),
    };
}

class Imaging {
    int? id;
    String? fileOriginalName;
    int? fileSize;
    String? fileType;
    String? fileThumbnail;
    String? fileDisplay;
    int? objectId;
    String? objectType;
    String? createdAt;
    int? clinicId;
    bool isShowPdf;

    Imaging({
        this.id,
        this.fileOriginalName,
        this.fileSize,
        this.fileType,
        this.fileThumbnail,
        this.fileDisplay,
        this.objectId,
        this.objectType,
        this.createdAt,
        this.clinicId,
        this.isShowPdf = false,
    });

    factory Imaging.fromJson(Map<String, dynamic> json) => Imaging(
        id: json["id"],
        fileOriginalName: json["file_original_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileThumbnail: json["file_thumbnail"],
        fileDisplay: json["file_display"],
        objectId: json["object_id"],
        objectType: json["object_type"],
        createdAt: json["created_at"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_original_name": fileOriginalName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_thumbnail": fileThumbnail,
        "file_display": fileDisplay,
        "object_id": objectId,
        "object_type": objectType,
        "created_at": createdAt,
        "clinic_id": clinicId,
    };
}
