// To parse this JSON data, do
//
//     final getPatientModal = getPatientModalFromJson(jsonString);

import 'dart:convert';

GetPatientModal getPatientModalFromJson(String str) => GetPatientModal.fromJson(json.decode(str));

String getPatientModalToJson(GetPatientModal data) => json.encode(data.toJson());

class GetPatientModal {
    dynamic message;
    DataPateintInfo? data;

    GetPatientModal({
        this.message,
        this.data,
    });

    factory GetPatientModal.fromJson(Map<String, dynamic> json) => GetPatientModal(
        message: json["message"],
        data: DataPateintInfo.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data!.toJson(),
    };
}

class DataPateintInfo {
    dynamic patientId;
    dynamic id;
    dynamic lastname;
    dynamic firstname;
    dynamic lastnameLatin;
    dynamic firstnameLatin;
    dynamic gender;
    dynamic dateOfBirth;
    dynamic age;
    dynamic personalCardId;
    dynamic phone;
    dynamic email;
    Profile? profile;
    dynamic qrCode;

    DataPateintInfo({
        this.patientId,
        this.id,
        this.lastname,
        this.firstname,
        this.lastnameLatin,
        this.firstnameLatin,
        this.gender,
        this.dateOfBirth,
        this.age,
        this.personalCardId,
        this.phone,
        this.email,
        this.profile,
        this.qrCode,
    });
    factory DataPateintInfo.fromJson(Map<String, dynamic> json) => DataPateintInfo(
        patientId: json["patient_id"],
        id: json["id"],
        lastname: json["lastname"],
        firstname: json["firstname"],
        lastnameLatin: json["lastname_latin"],
        firstnameLatin: json["firstname_latin"],
        gender: json["gender"],
        dateOfBirth:json["date_of_birth"],
        age: json["age"],
        personalCardId: json["personal_card_id"],
        phone: json["phone"],
        email: json["email"],
        profile: json["profile"]==null?null: Profile.fromJson(json["profile"]),
        qrCode: json["qr_code"],
    );

    Map<String, dynamic> toJson() => {
        "patient_id": patientId,
        "id": id,
        "lastname": lastname,
        "firstname": firstname,
        "lastname_latin": lastnameLatin,
        "firstname_latin": firstnameLatin,
        "gender": gender,
        "date_of_birth": dateOfBirth,
        "age": age,
        "personal_card_id": personalCardId,
        "phone": phone,
        "email": email,
        "profile": profile == null?null:profile!.toJson(),
        "qr_code": qrCode,
    };
}

class Profile {
    dynamic id;
    dynamic fileOriginalName;
    dynamic fileSize;
    dynamic fileType;
    dynamic fileThumbnail;
    dynamic fileDisplay;
    dynamic objectId;
    dynamic objectType;
    dynamic clinicId;

    Profile({
        this.id,
        this.fileOriginalName,
        this.fileSize,
        this.fileType,
        this.fileThumbnail,
        this.fileDisplay,
        this.objectId,
        this.objectType,
        this.clinicId,
    });

    factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"],
        fileOriginalName: json["file_original_name"],
        fileSize: json["file_size"],
        fileType: json["file_type"],
        fileThumbnail: json["file_thumbnail"],
        fileDisplay: json["file_display"],
        objectId: json["object_id"],
        objectType: json["object_type"],
        clinicId: json["clinic_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "file_original_name": fileOriginalName,
        "file_size": fileSize,
        "file_type": fileType,
        "file_thumbnail": fileThumbnail,
        "file_display": fileDisplay,
        "object_id": objectId,
        "object_type": objectType,
        "clinic_id": clinicId,
    };
}