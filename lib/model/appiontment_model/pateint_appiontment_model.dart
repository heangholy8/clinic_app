class PatientAppointmentModel {
  String? message;
  AppointmentModelData? data;

  PatientAppointmentModel({this.message, this.data});

  PatientAppointmentModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? new AppointmentModelData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class AppointmentModelData {
  List<Upcoming>? upcoming;
  List<Past>? past;

  AppointmentModelData({this.upcoming, this.past});

  AppointmentModelData.fromJson(Map<String, dynamic> json) {
    if (json['upcoming'] != null) {
      upcoming = <Upcoming>[];
      json['upcoming'].forEach((v) {
        upcoming!.add(new Upcoming.fromJson(v));
      });
    }
    if (json['past'] != null) {
      past = <Past>[];
      json['past'].forEach((v) {
        past!.add(new Past.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.upcoming != null) {
      data['upcoming'] = this.upcoming!.map((v) => v.toJson()).toList();
    }
    if (this.past != null) {
      data['past'] = this.past!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Upcoming {
  int? patientAppointmentId;
  String? appointmentDate;
  String? bookingTimeStr;
  int? status;
  String? note;
  DoctorData? doctorData;
  AppointmentTypeData? appointmentTypeData;
  AppointmentTypeData? departmentData;

  Upcoming(
      {this.patientAppointmentId,
      this.appointmentDate,
      this.bookingTimeStr,
      this.status,
      this.note,
      this.doctorData,
      this.appointmentTypeData,
      this.departmentData});

  Upcoming.fromJson(Map<String, dynamic> json) {
    patientAppointmentId = json['patient_appointment_id'];
    appointmentDate = json['appointment_date'];
    bookingTimeStr = json['booking_time_str'];
    status = json['status'];
    note = json['note'];
    doctorData = json['doctor_data'] != null
        ? new DoctorData.fromJson(json['doctor_data'])
        : null;
    appointmentTypeData = json['appointment_type_data'] != null
        ? new AppointmentTypeData.fromJson(json['appointment_type_data'])
        : null;
    departmentData = json['department_data'] != null
        ? new AppointmentTypeData.fromJson(json['department_data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['patient_appointment_id'] = this.patientAppointmentId;
    data['appointment_date'] = this.appointmentDate;
    data['booking_time_str'] = this.bookingTimeStr;
    data['status'] = this.status;
    data['note'] = this.note;
    if (this.doctorData != null) {
      data['doctor_data'] = this.doctorData!.toJson();
    }
    if (this.appointmentTypeData != null) {
      data['appointment_type_data'] = this.appointmentTypeData!.toJson();
    }
    if (this.departmentData != null) {
      data['department_data'] = this.departmentData!.toJson();
    }
    return data;
  }
}

class Past {
  int? patientAppointmentId;
  String? appointmentDate;
  String? bookingTimeStr;
  int? status;
  String? note;
  DoctorData? doctorData;
  AppointmentTypeData? appointmentTypeData;
  AppointmentTypeData? departmentData;

  Past(
      {this.patientAppointmentId,
      this.appointmentDate,
      this.bookingTimeStr,
      this.status,
      this.note,
      this.doctorData,
      this.appointmentTypeData,
      this.departmentData});

  Past.fromJson(Map<String, dynamic> json) {
    patientAppointmentId = json['patient_appointment_id'];
    appointmentDate = json['appointment_date'];
    bookingTimeStr = json['booking_time_str'];
    status = json['status'];
    note = json['note'];
    doctorData = json['doctor_data'] != null
        ? new DoctorData.fromJson(json['doctor_data'])
        : null;
    appointmentTypeData = json['appointment_type_data'] != null
        ? new AppointmentTypeData.fromJson(json['appointment_type_data'])
        : null;
    departmentData = json['department_data'] != null
        ? new AppointmentTypeData.fromJson(json['department_data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['patient_appointment_id'] = this.patientAppointmentId;
    data['appointment_date'] = this.appointmentDate;
    data['booking_time_str'] = this.bookingTimeStr;
    data['status'] = this.status;
    data['note'] = this.note;
    if (this.doctorData != null) {
      data['doctor_data'] = this.doctorData!.toJson();
    }
    if (this.appointmentTypeData != null) {
      data['appointment_type_data'] = this.appointmentTypeData!.toJson();
    }
    if (this.departmentData != null) {
      data['department_data'] = this.departmentData!.toJson();
    }
    return data;
  }
}

class DoctorData {
  int? id;
  String? title;
  String? titleLatin;
  String? fullname;
  String? fullnameLatin;
  String? phone;
  Profile? profile;

  DoctorData(
      {this.id,
      this.title,
      this.titleLatin,
      this.fullname,
      this.fullnameLatin,
      this.phone,
      this.profile});

  DoctorData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    titleLatin = json['title_latin'];
    fullname = json['fullname'];
    fullnameLatin = json['fullname_latin'];
    phone = json['phone'];
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['title_latin'] = this.titleLatin;
    data['fullname'] = this.fullname;
    data['fullname_latin'] = this.fullnameLatin;
    data['phone'] = this.phone;
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    return data;
  }
}

class Profile {
  int? id;
  String? fileOriginalName;
  int? fileSize;
  String? fileType;
  String? fileThumbnail;
  String? fileDisplay;
  int? objectId;
  String? objectType;
  String? createdAt;
  int? clinicId;

  Profile(
      {this.id,
      this.fileOriginalName,
      this.fileSize,
      this.fileType,
      this.fileThumbnail,
      this.fileDisplay,
      this.objectId,
      this.objectType,
      this.createdAt,
      this.clinicId});

  Profile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fileOriginalName = json['file_original_name'];
    fileSize = json['file_size'];
    fileType = json['file_type'];
    fileThumbnail = json['file_thumbnail'];
    fileDisplay = json['file_display'];
    objectId = json['object_id'];
    objectType = json['object_type'];
    createdAt = json['created_at'];
    clinicId = json['clinic_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['file_original_name'] = this.fileOriginalName;
    data['file_size'] = this.fileSize;
    data['file_type'] = this.fileType;
    data['file_thumbnail'] = this.fileThumbnail;
    data['file_display'] = this.fileDisplay;
    data['object_id'] = this.objectId;
    data['object_type'] = this.objectType;
    data['created_at'] = this.createdAt;
    data['clinic_id'] = this.clinicId;
    return data;
  }
}

class AppointmentTypeData {
  int? id;
  String? name;
  String? nameLatin;

  AppointmentTypeData({this.id, this.name, this.nameLatin});

  AppointmentTypeData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameLatin = json['name_latin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_latin'] = this.nameLatin;
    return data;
  }
}
