import 'package:flutter/material.dart';

import '../../app/core/themes/color_app.dart';
import '../../app/core/themes/themes.dart';

class DataNotFound extends StatefulWidget {
  final String? title;
  final String? subtitle;
  const DataNotFound({Key? key,required this.title,required this.subtitle}) : super(key: key);

  @override
  State<DataNotFound> createState() => _DataNotFoundState();
}

class _DataNotFoundState extends State<DataNotFound> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Stack(
        children: [
          Image.asset("assets/images/gifs/empty.gif", width: MediaQuery.of(context).size.width/1.5,),
          Positioned(
            bottom: -10,left: 0,right: 0,
            child: Column(
              children:[
                Text(widget.title.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(fontWeight: FontWeight.bold)),
                Text(widget.subtitle.toString(),style:const TextStyle(color: Colorconstands.lightBlack,fontSize: 14.0,),)
              ],
            ),
          )
        ],
      ),
    );
  }
}