

import 'package:flutter/material.dart';
import '../../app/core/themes/color_app.dart';
import '../../app/core/themes/themes.dart';

class EmptyWidget extends StatelessWidget {
  final String title;
  final String subtitle;
  final Widget icon;
  final Color? colorTitle;
  const EmptyWidget({Key? key,required this.icon, required this.title, required this.subtitle, this.colorTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 22, vertical: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon,
          Text(
            title,
            style: ThemeConstands.headline3_SemiBold_20.copyWith(color: colorTitle??Colorconstands.neutralWhite),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 22,
          ),
          subtitle == ""?Container():Text(
            subtitle,
            style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralGrey),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}