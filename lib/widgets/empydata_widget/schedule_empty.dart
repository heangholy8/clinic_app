import 'package:flutter/material.dart';

import '../../app/core/themes/themes.dart';

class ScheduleEmptyWidget extends StatelessWidget {
  final String? title;
  final String? subTitle;
  const ScheduleEmptyWidget({Key? key,required this.subTitle,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Column(
          children: [
            Container(
              child: Image.asset("assets/images/no_schedule_image.png",width: 200,height: 200,),
            ),
            Container(
              child: Text(title!,style: ThemeConstands.headline3_Medium_20_26height.copyWith(color:const Color(0xff1e2123),),textAlign: TextAlign.center,),
            ),
            Container(
              margin:const EdgeInsets.only(top: 8,left: 32,right: 32),
              child: Text(subTitle!,style: ThemeConstands.headline6_Regular_14_24height.copyWith(color:const Color(0xff828c95),),textAlign: TextAlign.center,),
            )
          ],
        ),
      ),
    );
  }
}