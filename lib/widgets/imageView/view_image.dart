import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';


class ImageViewDownloads extends StatefulWidget {
  final List listimagevide;
  final List<dynamic> listimageNet;
  late  int? activepage;
  ImageViewDownloads({Key? key, required this.listimageNet,required this.listimagevide, this.activepage}) : super(key: key);

  @override
  State<ImageViewDownloads> createState() => _ImageViewDownloadsState();
}

class _ImageViewDownloadsState extends State<ImageViewDownloads> {
  var linkImage;
  @override
  void initState() {
    super.initState();
    widget.listimagevide;
    widget.listimageNet;
  }
  @override
  Widget build(BuildContext context) {
    PageController pagec = PageController(initialPage: widget.listimageNet.isEmpty? widget.listimagevide.length==1?0:widget.activepage! :widget.listimageNet.length==1?0:widget.activepage!);
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        automaticallyImplyLeading: true,
        centerTitle: true,
        leading:const BackButton(
          color: Colors.white
        ), 
        actions: [
          widget.listimagevide.length==1?Container(): Container(
            margin:const EdgeInsets.only(top: 15,right: 18),
            child: Text("${widget.activepage! +1} / ${ widget.listimageNet.isEmpty?widget.listimagevide.length:widget.listimageNet.length}",style:const TextStyle(color: Colorconstands.neutralWhite,fontSize: 18.0),),
          ),
        ],
      ),
      body: SafeArea(
        child: Center(
          child:
           PageView.builder(
            controller: pagec,
            itemCount: widget.listimageNet.isEmpty? widget.listimagevide.length:widget.listimageNet.length,
            onPageChanged: (index){
              setState(() {
                widget.activepage=index;
              });
            },
            itemBuilder: (context, index) {
              if(widget.listimageNet.isEmpty){
                linkImage = widget.listimagevide[index];
              }
              return Center(
                child:InteractiveViewer(
                  clipBehavior: Clip.none,
                  child: ClipRRect(
                    child: Center(child: widget.listimageNet.isEmpty?Image.file(linkImage!,):Image.network(widget.listimageNet[index].fileDisplay.toString())),
                  ),
                )
              );
            },
            
          ),
        ),
      ),
    );
  }
}