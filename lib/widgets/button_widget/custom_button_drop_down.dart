// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import '../../app/core/themes/color_app.dart';
class CustomButtonDropDown extends StatelessWidget {
  CustomButtonDropDown({
    Key? key,
    required this.title,required this.onPressed
  }) : super(key: key);

  final String title;
  Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color:const Color(0xFFF1F9FF),
        borderRadius: BorderRadius.circular(12.0)
      ),
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0)),
        padding: const EdgeInsets.all(0.0),
        onPressed: onPressed,
        child: Container(
          margin:const EdgeInsets.symmetric(horizontal: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(title,style:const TextStyle(color: Colorconstands.secondaryColor,fontSize: 16.0),),
              const Icon(Icons.keyboard_arrow_down_rounded,size: 28,color: Colorconstands.secondaryColor,)
            ],
          ),
          alignment: Alignment.centerLeft,
        ),
      ),
    );
  }
}
