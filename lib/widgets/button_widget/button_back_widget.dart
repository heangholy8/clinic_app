import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';

class Iconbuttonback extends StatelessWidget {
  const Iconbuttonback({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      width: 35,
      child: MaterialButton(
        onPressed: () {
          FocusScope.of(context).unfocus();
          Navigator.of(context).pop();
        },
        padding: const EdgeInsets.only(right: 0.0),
        child: const Icon(
          Icons.keyboard_arrow_left,
          size: 32,
          color: Colorconstands.darkGray,
        ),
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
        boxShadow: const [
          BoxShadow(
            color: Color(0xffe0e0e0),
            blurRadius: 8,
            offset: Offset(2, 2),
          ),
        ],
      ),
    );
  }
}
