import 'dart:convert';
import 'package:clinic_application/model/pateint_model/appointment_model/appointment_type_model.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/department_doctor.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/department_model.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/time_doctor_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:clinic_application/service/base_url.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../storages/get_storage.dart';
class BookAppointmentApi {
  Future<AppointMentTypeModel> getAppointmentTypeApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-medical-setting/get-all-appointment-types"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("AppointMent Type ${response.body}");
      return AppointMentTypeModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("AppointMent Type $e");
      });
    }
  }
  Future<DepartmentDoctorModel> getDepartmentDoctorApi({String? departmentId}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-employee/get-all-department-doctors"),
      headers: <String, String>{
          // "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
      body: {
        "department_id":departmentId,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("department Doctor ${response.body}");
      return DepartmentDoctorModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("department Doctor $e");
      });
    }
  }
  Future<DepartmentModel> getDepartmentApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-medical-setting/get-all-departments"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Department ${response.body}");
      return DepartmentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("Department $e");
      });
    }
  }
  Future<TimeDoctorModel> getTimeDoctorApi({String? doctorId,String? appointDate }) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-patient/check-available-doctor-time"),
      headers: <String, String>{
          // "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
      body: {
        "doctor_id": doctorId,
        "appointment_date":appointDate
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("TimeDoctor Type ${response.body}");
      return TimeDoctorModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("TimeDoctor Type $e");
      });
    }
  }

  Future <bool> addAppointment({String? departmentId,String? appointDate,String? status,String? appointmentTypeId,String? employeeId,String? time,String? patientVisitId,String? note }) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-patient/add-patient-appointment"),
      headers: <String, String>{
          // "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
      body: {
        "appointment_date":appointDate,
        "booking_time_str": time,
        "status":status,
        "note": note,
        "employee_id": employeeId,
        "appointment_type_id":appointmentTypeId,
        "patient_id": patientVisitId,
        "department_id": departmentId,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Add appointment ${response.body}");
      return true;
    } else {
      throw Exception((e) {
        debugPrint("Add appointment $e");
        return false;
      });
    }
  }

  Future <bool> deleteAppointment({String? appointmentId }) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-patient/delete-patient-appointment"),
      headers: <String, String>{
          // "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
      body: {
        "patient_appointment_id":appointmentId,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("delete appointment ${response.body}");
      return true;
    } else {
      throw Exception((e) {
        debugPrint("delete appointment $e");
        return false;
      });
    }
  }
}