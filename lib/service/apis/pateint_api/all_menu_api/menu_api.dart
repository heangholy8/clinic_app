import 'dart:convert';
import 'package:clinic_application/model/appiontment_model/pateint_appiontment_model.dart';
import 'package:clinic_application/model/pateint_model/feature_model/investigation_model.dart';
import 'package:clinic_application/model/pateint_model/feature_model/medical_model.dart';
import 'package:clinic_application/model/pateint_model/feature_model/physical_model.dart';
import 'package:clinic_application/model/pateint_model/feature_model/prescription_model.dart';
import 'package:clinic_application/model/pateint_model/feature_model/vaccination_model.dart';
import 'package:clinic_application/model/pateint_model/feature_model/vital_sing_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:clinic_application/service/base_url.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../storages/get_storage.dart';
class GetMenuFeatureApi {
  Future<VitalSingModel> getVitalSingApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-vital-sign-history"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("vital sing Patient${response.body}");
      return VitalSingModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("vital sing Patient $e");
      });
    }
  }

  Future<MedicalHistoryModel> getMedicalHistoryApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-medical-history"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Medical Hostory Patient${response.body}");
      return MedicalHistoryModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("Medical Hostory Patient $e");
      });
    }
  }

  Future<InvestigationModel> getInvestigationApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-investigation-history"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("investigation Hostory Patient${response.body}");
      return InvestigationModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("investigation Hostory Patient $e");
      });
    }
  }

  Future<VaccinationModel> getVaccinationApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-vaccination-history"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("vaccination Hostory Patient${response.body}");
      return VaccinationModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("vaccination Hostory Patient $e");
      });
    }
  }

  Future<PrescriptionModel> getPrescriptionApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-prescription-history"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("prescription Hostory Patient${response.body}");
      return PrescriptionModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("prescription Hostory Patient $e");
      });
    }
  }

  Future<PhysicalHistory> getPhysicalHistoryApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-physical-history"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("physical Hostory Patient${response.body}");
      return PhysicalHistory.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("physical Hostory Patient $e");
      });
    }
  }

  Future<PatientAppointmentModel> getAppointmentApi() async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-appointments"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("abc${response.body} + ${token.access} e");
      return PatientAppointmentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
       
        print("abc error $e");
      });
    }
  }
}