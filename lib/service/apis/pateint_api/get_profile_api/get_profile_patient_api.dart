import 'dart:convert';
import 'package:clinic_application/model/pateint_model/get_profile_model/info_patient_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:clinic_application/service/base_url.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../storages/get_storage.dart';
class GetPatientProfile {
  Future<InfoPateintModel> getProfilePatientApi({String? idPatient}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    var token = await _prefs.getJsonTokenPatient;
    var keySecrypt = _pref.getString("keySecrypt") ?? "";
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/$keySecrypt/api-mobile/get-my-info"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Profile Patient${response.body}");
      return InfoPateintModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("Profile Patient $e");
      });
    }
  }
}