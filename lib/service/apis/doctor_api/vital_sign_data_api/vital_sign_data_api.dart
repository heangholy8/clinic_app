import 'dart:convert';
import 'package:clinic_application/model/doctor_model/vital_sign_data_model/vital_sign_data_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:clinic_application/service/base_url.dart';
import '../../../../storages/get_storage.dart';
class GetVitalSignDataPatientApi {
  Future<VitalsignDataModel> getPatientVitalSignDataApi({String? idPatientVisit}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/${token.clinicData!.secretKey}/api-patient/get-patient-current-vital-sign-by-visit-id"),
      headers: <String, String>{
          "Authorization":"Bearer ${token.access}",
      },
      body: <String, dynamic>{
          "patient_visit_id":idPatientVisit,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Vitasign patient${response.body}");
      return VitalsignDataModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("Vitasign patient $e");
      });
    }
  }

  Future<bool> postPatientVitalSignDataApi({
    String? idPatientVisit,
    String? chiefComplaint,
    String? currentMedication,
    String? systolic,
    String? diastolic,
    String? respiratoryRate,
    String? spo2,
    String? pulse,
    String? temperature,
    String? glucose,
    String? height,
    String? weight,
    String? historyOfIllness,
    String? isCompleted,
    String? objectKey
    }) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/${token.clinicData!.secretKey}/api-patient/update-patient-current-vital-sign-by-visit-id"),
      headers: <String, String>{
          "Authorization":"Bearer ${token.access}",
      },
      body: <String, dynamic>{
          "patient_visit_id":idPatientVisit,
          "chief_complaint":chiefComplaint,
          "current_medication":currentMedication,
          "systolic":systolic,
          "diastolic":diastolic,
          "respiratory_rate":respiratoryRate,
          "spo2":spo2,
          "pulse":pulse,
          "temperature":temperature,
          "glucose":glucose,
          "height":height,
          "weight":weight,
          "history_of_illness":historyOfIllness,
          "is_completed":isCompleted,
          "object_key":objectKey,
      }
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(" poat Vitasign patient${response.body}");
      return true;
    } else {
      debugPrint("post Vitasign patient ${response.body}");
      return false;
    }
  }
}