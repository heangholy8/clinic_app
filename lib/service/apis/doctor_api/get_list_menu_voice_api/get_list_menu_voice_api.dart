import 'dart:convert';

import 'package:clinic_application/model/doctor_model/list_menu_voice_model/list_menu_voice_model.dart';
import 'package:clinic_application/service/base_url.dart';
import 'package:clinic_application/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
class PostCodeVisitGetListMenuVoiceApi {
  Future<ListMenuVoiceModel> postCodeVisitGetListMenuVoiceApi({String? code}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    var body = jsonEncode({
        "code": code
      });
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/WZHVFwHFk7PDk8O1PbGeoTdtaja5X4LzCnUdeJvLKgk/api-patient/get-patient-visit-voice"),
      headers: <String, String>{
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept":"application/json",
        "Authorization": "Bearer ${token.access}"
      },
      body: {
        "code": code,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("data list menu voice${response.body}");
      return ListMenuVoiceModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
       debugPrint(" error data list menu voice${response.body}");
      });
    }
  }
}