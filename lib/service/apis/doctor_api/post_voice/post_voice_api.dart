import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:clinic_application/service/base_url.dart';
import '../../../../storages/get_storage.dart';
class PostVoiceApi {
  Future<bool> postVoiceApi({
    required String objectId,
    required String objectType,
    required List<File> listFile,}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _pref = GetStoragePref();
      var auth = await _pref.getJsonToken;
      var request = http.MultipartRequest(
        'POST', Uri.parse("${baseUrl.adminBaseUrl}/${auth.clinicData!.secretKey}/api-patient/add-patient-visit-voice"),
      );
      request.fields.addAll({
          "clinic_id": auth.clinicData!.id.toString(),
          "object_id": objectId,
          "object_type":objectType,
        });

      if(listFile.isNotEmpty){
        for (var file in listFile) {
          var stream = http.ByteStream(file.openRead());
          var length = await file.length();
          var multipartFile = http.MultipartFile('files', stream, length, filename: file.path);
          request.files.add(multipartFile);
        }
          // for (int i = 0;i<listFile.length;i++) {
          //   var stream = http.ByteStream(listFile[i].openRead());
          //   var length = await listFile[i].length();
          //   var multipartFile = http.MultipartFile('files', stream, length, filename: listFile[i].path);
          //   request.files.add(multipartFile);
          // }
      }

      var headers = {
        "Accept": "application/json",
        "Content-Type":"application/json",
        "Authorization":"Bearer ${auth.access}",
      };

      request.headers.addAll(headers);
      var res = await request.send();
      http.Response response = await http.Response.fromStream(res);
      debugPrint("Post voice${response.body}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Post voice${response.body}");
      return true;
    } else{
      throw(e){
        debugPrint("Post voice error permission"+ e);
        return false;
      };
    }
  }
}