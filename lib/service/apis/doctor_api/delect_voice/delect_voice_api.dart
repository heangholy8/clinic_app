import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:clinic_application/service/base_url.dart';
import '../../../../storages/get_storage.dart';
class DeleteVoiceApi {
  Future<bool> deleteVoiceApi({String? id}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    http.Response response = await http.delete(
      Uri.parse("${baseUrl.adminBaseUrl}/${token.clinicData!.secretKey}/api-file-upload/delete-file-upload/$id"),
      headers: <String, String>{
          "Accept": "application/json",
          "Content-Type":"application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("${response.body}");
      return true;
    } else {
      throw Exception((e) {
        debugPrint("$e");
        return false;
      });
    }
  }
}