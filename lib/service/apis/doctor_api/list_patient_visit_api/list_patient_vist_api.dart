import 'dart:convert';
import 'package:clinic_application/model/doctor_model/pateint_visit_model/patient_vist_list_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:clinic_application/service/base_url.dart';
import '../../../../storages/get_storage.dart';

class ListVisitPatientApi {
  Future<ListPatientVisitModel> getPatientVisitApi({String? idPatient}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${baseUrl.adminBaseUrl}/${token.clinicData!.secretKey}/api-patient/get-all-visits"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Pateint visit${response.body}");
      return ListPatientVisitModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("Pateint visit $e");
      });
    }
  }
}