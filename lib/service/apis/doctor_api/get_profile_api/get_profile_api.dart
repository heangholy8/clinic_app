import 'dart:convert';
import 'package:clinic_application/model/pateint_model/get_profile_model/get_profile_api.dart';
import 'package:http/http.dart' as http;

import 'package:clinic_application/service/base_url.dart';
import '../../../../storages/get_storage.dart';
class GetProfilePatient {
  Future<GetPatientModal> getProfileApiApi({String? idPatient}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/${token.clinicData!.secretKey}/api-patient/get/$idPatient"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("fhgfjh${response.body}");
      return GetPatientModal.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print("fhgfjh$e");
      });
    }
  }
}