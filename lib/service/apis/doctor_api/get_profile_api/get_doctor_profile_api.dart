import 'dart:convert';
import 'package:clinic_application/model/doctor_model/profile_doctor/profile_doctor_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:clinic_application/service/base_url.dart';
import '../../../../storages/get_storage.dart';
class GetDoctorProfilePatient {
  Future<ProfileDoctorModel> getDoctorProfileApi({String? idPatient}) async {
    BaseUrl baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    http.Response response = await http.get(
      Uri.parse("${baseUrl.adminBaseUrl}/${token.clinicData!.secretKey}/api-mobile/get-my-info"),
      headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization":"Bearer ${token.access}",
        },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint("Profile doctor${response.body}");
      return ProfileDoctorModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        debugPrint("Profile doctor $e");
      });
    }
  }
}