import 'dart:convert';

import 'package:clinic_application/model/doctor_model/decrypt_data_qr/decrypt_data_qr_model.dart';
import 'package:clinic_application/model/pateint_model/get_profile_model/get_profile_api.dart';
import 'package:clinic_application/service/base_url.dart';
import 'package:clinic_application/storages/get_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
class PostQrApi {
  Future<bool> postQrApi({String? eventName ,DataPateintInfo? data}) async {
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    debugPrint("sadfasdf${"user-${token.profileData!.id}-${token.clinicData!.id}"}");
    var body = jsonEncode({
        "eventName": eventName,
        "data": data,
        "userId": "${token.clinicData!.secretKey}-user-${token.profileData!.id}-${token.clinicData!.id}"
      });
    http.Response response = await http.post(
      Uri.parse("https://dev-ws-clinic.camemis-learn.com/api/send-user-event"),
      headers: <String, String>{
        "Content-Type": "application/json",
      },
      body: body,
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print("fsjadf${response.body}");
      return true;
    } else {
      print("fsjadf${response.body}");
      throw Exception((e) {
        print(e);
        return false;
      });
    }
  }

  Future<DecryptDataModel> postQrDecryptApi({String? data}) async {
    final BaseUrl _baseUrl = BaseUrl();
    GetStoragePref _prefs = GetStoragePref();
    var token = await _prefs.getJsonToken;
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.adminBaseUrl}${token.clinicData!.secretKey}/api-admin/get-decrypt-data"),
      headers: <String, String>{
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body:{
        "data":data
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
     debugPrint("decrypt qr${response.body}");
     return DecryptDataModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
        print(e);
        return false;
      });
    }
  }
}