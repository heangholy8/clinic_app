import 'dart:convert';
import 'package:clinic_application/model/auth_model/auth_model.dart';
import 'package:clinic_application/model/auth_model/auth_pateint_model.dart';
import 'package:clinic_application/model/doctor_model/list_clinis_model/list_clinic_model.dart';
import 'package:clinic_application/service/base_url.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AuthApi {
  final BaseUrl _baseUrl = BaseUrl();
  Future<AuthModel> signInRequestApi({required String loginName,required String password, required String clinicSecretKey})async{
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.adminBaseUrl}/$clinicSecretKey/api-user/login".trim()),
      body: {
        "password": password,
        "username": loginName,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return AuthModel.fromJson(jsonDecode(response.body));
    } else {
      print(response.body);
      throw Exception((e) {
      });
    }
  }

  Future<AuthPateintModel> signInRequestPetientApi({required String loginName,required String password})async{
    http.Response response = await http.post(
      Uri.parse("${_baseUrl.adminBaseUrl}/api-admin/mobile-login".trim()),
      body: {
        "password": password,
        "username": loginName,
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.body);
      return AuthPateintModel.fromJson(jsonDecode(response.body));
    } else {
      print(response.body);
      throw Exception((e) {
      });
    }
  }

  Future<ListClinicModel> getListClinicApi()  async{
    http.Response response = await http.get(Uri.parse("${_baseUrl.adminBaseUrl}/api-admin/get-customer-info".trim()),);
    if (response.statusCode == 200 || response.statusCode == 201) {
      debugPrint(response.body);
      return ListClinicModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception((e) {
      });
    }
  }
}
