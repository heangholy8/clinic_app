// ignore_for_file: non_constant_identifier_names

class BaseUrl {
  final String _admin_base_url =
      "https://dev-api-clinic.camemis-learn.com";

  String get adminBaseUrl => _admin_base_url;
}
