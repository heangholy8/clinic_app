part of 'list_patient_visit_bloc.dart';

sealed class ListPatientVisitEvent extends Equatable {
  const ListPatientVisitEvent();

  @override
  List<Object> get props => [];
}

class GetListPatientVisit extends ListPatientVisitEvent{}
