import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/doctor_model/pateint_visit_model/patient_vist_list_model.dart';
import 'package:clinic_application/service/apis/doctor_api/list_patient_visit_api/list_patient_vist_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
part 'list_patient_visit_event.dart';
part 'list_patient_visit_state.dart';

class ListPatientVisitBloc extends Bloc<ListPatientVisitEvent, ListPatientVisitState> {
  final ListVisitPatientApi listVisitPatientApi;
  List<DataPatientVisit>? pateintVisitData;
  ListPatientVisitBloc({required this.listVisitPatientApi}) : super(ListPatientVisitInitial()) {
    on<GetListPatientVisit>((event, emit) async {
      emit(ListPatientVisitLoading());
      try{
         var data = await listVisitPatientApi.getPatientVisitApi();
         emit(ListPatientVisitLoaded(listPatientVisit: data));
         pateintVisitData = data.data;
      }catch(e){
        debugPrint("$e");
        emit(ListPatientVisitError());
      }
    });
  }
}
