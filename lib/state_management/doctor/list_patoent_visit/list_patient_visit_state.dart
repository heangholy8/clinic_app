part of 'list_patient_visit_bloc.dart';

sealed class ListPatientVisitState extends Equatable {
  const ListPatientVisitState();
  
  @override
  List<Object> get props => [];
}

final class ListPatientVisitInitial extends ListPatientVisitState {}

final class ListPatientVisitLoading extends ListPatientVisitState {}
final class ListPatientVisitLoaded extends ListPatientVisitState {
  final ListPatientVisitModel  listPatientVisit;
  const ListPatientVisitLoaded({required this.listPatientVisit});
}
final class ListPatientVisitError extends ListPatientVisitState {}
