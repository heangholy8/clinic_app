// ignore_for_file: constant_identifier_names


abstract class Routes {
  Routes._();

  static const SPLAHSSCREEN = _Paths.SPLASH;
  static const QRCODESCREEN = _Paths.QRCODE;
  static const LOGINGSCREEN = _Paths.LOGIN;
}

abstract class _Paths {
  static const SPLASH = '/splash';
  static const QRCODE = '/qr_code_login';
  static const LOGIN = '/login';

}
