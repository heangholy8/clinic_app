
import 'package:clinic_application/app/modules/login_screen/views/login_screen.dart';
import 'package:flutter/material.dart';

import 'e.route.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    switch (settings.name) {
      case Routes.SPLAHSSCREEN:
        return MaterialPageRoute(builder: (_) => const SplashScreen());
      default:
        return MaterialPageRoute(builder: (_) => const LogingScreen());
    }
  }
}
