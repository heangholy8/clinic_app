import 'package:clinic_application/app/modules/doctor_screen/home_doctor_screen/view/home_doctor_screen.dart';
import 'package:clinic_application/app/modules/login_screen/state/list_clinic/bloc/list_clinic_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/icon_back.dart';
import 'package:clinic_application/mixins/toast.dart';
import 'package:clinic_application/model/doctor_model/list_clinis_model/list_clinic_model.dart';
import 'package:clinic_application/widgets/button_widget/button_icon_left_with_title.dart';
import 'package:clinic_application/widgets/button_widget/button_widget_custom.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../storages/get_storage.dart';
import '../../../../widgets/expansion/expand_section.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../state/auth/auth_bloc.dart';

class LogingScreen extends StatefulWidget {
  const LogingScreen({Key? key}) : super(key: key);

  @override
  State<LogingScreen> createState() => _LogingScreenState();
}

class _LogingScreenState extends State<LogingScreen> with Toast {
  final GetStoragePref _getStoragePref = GetStoragePref();
  bool checkfocususer = false;
  bool checkfocuspass = false;
  bool passwordshow = true;
  bool loadingsumit = false;
  bool isExpanded = false;
  String? comfrimProfile;
  TextEditingController usercodeController = TextEditingController();
  TextEditingController passcodeController = TextEditingController();
  TextEditingController searchController = TextEditingController();

  List<DataClinic>? _foundData;
  int? isSeleted;
  FocusNode myfocus = FocusNode();
  String? cliniName;

  @override
  void initState() {
    setState(() {
      //BlocProvider.of<ListClinicBloc>(context).add(GetListClinic());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,top: true,
        child: Stack(
          children: [
            BlocListener<AuthBloc, AuthState>(
              listener: (context, state) {
                if (state is Loading) {
                  setState(() {
                    loadingsumit = true;
                  });
                } else if (state is Authenticated) {
                  setState((){
                    HapticFeedback.vibrate();
                    loadingsumit = false;
                    Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) => const HomePageDoctor(),
                      transitionDuration: Duration.zero,
                      reverseTransitionDuration: Duration.zero,
                    ),(route) => false,
                    );
                    // Navigator.push(context, MaterialPageRoute(builder: (context)=> const HomePageDoctor()));
                  });
                } else if (state is UnAUthenticated) {
                  setState(() {
                    loadingsumit = false;
                    showErrorDialog(() {
                      Navigator.of(context).pop();
                    }, context);
                  });
                }
              },
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(
                    top: 25,
                    left: 27,
                    right: 27,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            width: 40,
                            child: IconBack(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              hightButton: 30,
                              radiusButton: 6,
                            ),
                          ),
                          Expanded(child: Container()),
                        ],
                      ),
                      Container(
                        margin:const EdgeInsets.only(top: 15),
                        child: Image.asset(ImageAssets.app_logo),
                      ),
                      Container(
                        margin:const EdgeInsets.only(top: 18),
                        padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 3),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colorconstands.neutralWhite),
                          borderRadius: BorderRadius.circular(16)
                        ),
                        child: Text("Vibolrith’s Health System",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite,),),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 18,bottom: 18),
                        child: Text(
                          'ENTER CODE'.tr(),
                          style: ThemeConstands.headline2_SemiBold_24
                              .copyWith(color: Colorconstands.neutralWhite),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        width: MediaQuery.of(context).size.width,
                        height: 55,
                        padding: const EdgeInsets.symmetric(horizontal: 18),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colorconstands.neutralGrey),
                          color: Colorconstands.neutralWhite,
                          borderRadius: BorderRadius.only(topLeft: const Radius.circular(25),topRight: const Radius.circular(25),bottomLeft: Radius.circular(isExpanded == true?0:25),bottomRight: Radius.circular(isExpanded == true?0:25)),
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: Focus(
                                onFocusChange: (value) {
                                  setState(() {
                                    isExpanded = value;
                                  });
                                },
                                child: TextFormField(
                                  scrollPhysics:const BouncingScrollPhysics(),
                                  controller: searchController,
                                  style: ThemeConstands.headline5_Medium_16,
                                  focusNode: myfocus,
                                  onChanged: (value) => _filter(value, BlocProvider.of<ListClinicBloc>(context).preventData!),
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.symmetric(vertical: 6.0,horizontal: 8.0),
                                    enabledBorder:InputBorder.none,
                                    focusedBorder:InputBorder.none,
                                    border:const UnderlineInputBorder(),
                                    hintText: 'Choose Clinic'.tr(),
                                    hintStyle: ThemeConstands .subtitle1_Regular_16,
                                  ),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  isExpanded = !isExpanded;
                                  if (isExpanded) {
                                    myfocus.requestFocus();
                                  } else {
                                    myfocus.unfocus();
                                  }
                                });
                              },
                              child: SizedBox(
                                width: 38,
                                height: 38,
                                child: !isExpanded? const Icon(Icons.keyboard_arrow_down) : const Icon(
                                        Icons.keyboard_arrow_up,color: Colorconstands.darkBordersDefault,),
                              ),
                            ),
                          ],
                        ),
                      ),
                      BlocConsumer<ListClinicBloc,ListClinicState>(
                        listener: (context, state) {
                          if (state is ListClinicLoaded) {
                            setState(() {
                              _foundData = BlocProvider.of<ListClinicBloc>(context).preventData;
                            });
                          }
                        },
                        builder: (context, state) {
                          if (state is ListClinicLoading) {
                            return const Center(
                              //child: CircularProgressIndicator(),
                            );
                          } else if (state is ListClinicLoaded) {
                            return dropDownSchoolList(
                              isEmptyString:searchController.text,
                              translate: translate,
                              selectedName: searchController.text,
                              isExpaned: isExpanded,
                            );
                          }
                          else{
                            return const EmptyWidget(
                              icon: Icon(Icons.error_outline,color: Colorconstands.errorColor,),
                              title: "Error",
                              subtitle: "",
                            );
                          }
                        },
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 16),
                        padding: EdgeInsets.zero,
                        decoration: BoxDecoration(
                          color: Colorconstands.lightGohan,
                          borderRadius:const BorderRadius.all(Radius.circular(25)),
                          border: Border.all(
                              color: checkfocususer == false
                                  ? Colorconstands.neutralGrey
                                  : Colorconstands.primaryColor,
                              width: checkfocususer == false
                                  ? 1
                                  : 2),
                        ),
                        child: Focus(
                          onFocusChange: (hasfocus) {
                            setState(() {
                              hasfocus
                                  ? checkfocususer = true
                                  : checkfocususer = false;
                            });
                          },
                          child: TextFormField(
                            controller: usercodeController,
                            onChanged: ((value) {
                              setState(() {
                                usercodeController.text;
                              });
                            }),
                            style: ThemeConstands
                                .subtitle1_Regular_16
                                .copyWith(color: Colors.black),
                            decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderRadius:
                                      BorderRadius.circular(
                                          16.0)),
                              contentPadding:
                                  const EdgeInsets.only(
                                      top: 8,
                                      bottom: 5,
                                      left: 18,
                                      right: 18),
                              labelText: 'USERCODE'.tr(),
                              labelStyle: ThemeConstands
                                  .subtitle1_Regular_16
                                  .copyWith(
                                      color: Colorconstands
                                          .lightTrunks),
                              enabledBorder:
                                  const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors
                                              .transparent)),
                              focusedBorder:
                                  const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors
                                              .transparent)),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            top: 16, bottom: 24),
                        padding: EdgeInsets.zero,
                        decoration: BoxDecoration(
                          color: Colorconstands.lightGohan,
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(
                              color: checkfocuspass == false
                                  ? Colorconstands.neutralGrey
                                  : Colorconstands.primaryColor,
                              width: checkfocuspass == false
                                  ? 1
                                  : 2),
                        ),
                        child: Focus(
                          onFocusChange: (hasfocus) {
                            setState(() {
                              hasfocus
                                  ? checkfocuspass = true
                                  : checkfocuspass = false;
                            });
                          },
                          child: TextFormField(
                            controller: passcodeController,
                            onChanged: ((value) {
                              setState(() {
                                passcodeController.text;
                              });
                            }),
                            obscureText: passwordshow,
                            style: ThemeConstands
                                .subtitle1_Regular_16,
                            decoration: InputDecoration(
                              suffix: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      if (passwordshow) {
                                        passwordshow = false;
                                      } else {
                                        passwordshow = true;
                                      }
                                    });
                                  },
                                  child: Text(
                                      passwordshow == true
                                          ? 'SHOW'.tr()
                                          : 'HIDE'.tr(),
                                      style: ThemeConstands
                                          .headline6_Regular_14_20height
                                          .copyWith(
                                        color: Colorconstands
                                            .lightBulma,
                                        decoration: TextDecoration
                                            .underline,
                                      ))),
                              border: UnderlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(16.0),
                              ),
                              contentPadding:
                                  const EdgeInsets.only(
                                      top: 8,
                                      bottom: 5,
                                      left: 18,
                                      right: 18),
                              labelText: 'PASSWORD'.tr(),
                              labelStyle: ThemeConstands
                                  .subtitle1_Regular_16
                                  .copyWith(
                                      color: Colorconstands
                                          .lightTrunks),
                              enabledBorder:
                                  const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors
                                              .transparent)),
                              focusedBorder:
                                  const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors
                                              .transparent)),
                            ),
                          ),
                        ),
                      ),
                      usercodeController.text == "" ||
                                passcodeController.text == ""?Container():const Divider(),
                      ButtonIconLeftWithTitle(
                        panddingVerButton: 6, 
                        iconImage: ImageAssets.icon_login, 
                        iconColor: Colorconstands.primaryColor,
                        buttonColor: Colorconstands.neutralWhite,
                        onTap: usercodeController.text == "" ||
                                passcodeController.text == ""
                            ? null
                            : () {
                              print("sdafasdf${BlocProvider.of<ListClinicBloc>(context).secretKey}");
                                BlocProvider.of<AuthBloc>(context).add(SignInRequested(loginName:usercodeController.text.toString(),password:passcodeController.text.toString(),secretKey: BlocProvider.of<ListClinicBloc>(context).secretKey.toString()));
                             // AuthApi().signInRequestApi(loginName: usercodeController.text, password: passcodeController.text);
                            },
                        panddinHorButton: 22,
                        radiusButton: 20,
                        heightButton: 50,
                        textStyleButton: ThemeConstands
                            .button_SemiBold_16
                            .copyWith(
                                color:usercodeController.text == "" ||
                                passcodeController.text == ""?Colors.transparent:
                                    Colorconstands.primaryColor),
                        title: 'CONTINUE'.tr(),
                      ),
                      Container(
                        height: 160,
                      )
                    ],
                  ),
                ),
              ),
            ),
            loadingsumit == true?
            Container(
              color: Colorconstands.darkGray.withOpacity(0.6),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child:const Center(
                child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
              ),
            ):Container(height: 0,)
          ],
        ),
      ),
    );
  }
  Widget dropDownSchoolList(
      {required String translate,
      required String selectedName,
      String isEmptyString = "",
      bool isExpaned = false}) {
    return ExpandedSection(
      expand: isExpanded,
      child: _foundData != null || _foundData != 0
          ? ListView.separated(
              itemCount: _foundData!.length,
              separatorBuilder: ((context, index) {
                return const Divider(
                  height: 1,
                );
              }),
              physics: const ScrollPhysics(),
              padding: const EdgeInsets.all(0),
              shrinkWrap: true,
              itemBuilder: ((context, index) {
                return Container(
                  height: 44,
                  width: MediaQuery.of(context).size.width,
                  decoration:const BoxDecoration(borderRadius: BorderRadius.only(bottomRight: Radius.circular(25),bottomLeft: Radius.circular(25))),
                  child: MaterialButton(
                    height: 40,
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(0),
                        topRight: const Radius.circular(0),
                        bottomLeft: index == _foundData!.length - 1? const Radius.circular(25.0): const Radius.circular(0),
                        bottomRight: index == _foundData!.length - 1? const Radius.circular(25.0) : const Radius.circular(0),
                      ),
                    ),
                    color: isSeleted == index
                        ? const Color(0xFF055557)
                        : Colors.white,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                    onPressed: () {
                      myfocus.unfocus();
                      setState(() {
                        isSeleted = index;
                        BlocProvider.of<ListClinicBloc>(context).secretKey = _foundData?[isSeleted!].secretKey;
                        if (translate == "km") {
                          cliniName = _foundData?[isSeleted!].customerName.toString();
                        }else {
                          cliniName = _foundData?[isSeleted!].customerName.toString();
                        }
                        searchController.text = cliniName!;
                        isExpanded = false;
                      });
                    },
                    child: Text(
                      translate == "km"? _foundData![index].customerName.toString() : _foundData![index].customerName.toString(),
                      style: ThemeConstands.subtitle1_Regular_16.copyWith(
                          color: isSeleted == index? Colorconstands.neutralWhite: Colors.black),
                    ),
                  ),
                );
              }),
            )
          : Container(),
    );
  }
  void _filter(String query, List<DataClinic> allResult) {
    List<DataClinic> results = [];
    if (query.isEmpty) {
      results = allResult;
    } else {
      results = allResult.where((element) =>element.customerName!.toLowerCase().contains(query.toLowerCase())).toList();
    }
    setState(() {
      _foundData = results;
    });
  }
}
