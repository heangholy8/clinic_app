import 'package:clinic_application/app/modules/pateint_screen/home_page/view/home_page.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/icon_back.dart';
import 'package:clinic_application/mixins/toast.dart';
import 'package:clinic_application/model/doctor_model/list_clinis_model/list_clinic_model.dart';
import 'package:clinic_application/widgets/button_widget/button_icon_left_with_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../core/resources/asset_resource.dart';
import '../../../core/themes/color_app.dart';
import '../../../core/themes/themes.dart';
import '../state/auth/auth_bloc.dart';

class LoginPatientScreen extends StatefulWidget {
  const LoginPatientScreen({Key? key}) : super(key: key);

  @override
  State<LoginPatientScreen> createState() => _LoginPatientScreenState();
}

class _LoginPatientScreenState extends State<LoginPatientScreen> with Toast {
  bool checkfocususer = false;
  bool checkfocuspass = false;
  bool passwordshow = true;
  bool loadingsumit = false;
  bool isExpanded = false;
  String? comfrimProfile;
  TextEditingController usercodeController = TextEditingController();
  TextEditingController passcodeController = TextEditingController();

  List<DataClinic>? _foundData;
  int? isSeleted;
  FocusNode myfocus = FocusNode();
  String? cliniName;

  @override
  void initState() {
    setState(() {
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,top: true,
        child: Stack(
          children: [
            BlocListener<AuthBloc, AuthState>(
              listener: (context, state) {
                if (state is Loading) {
                  setState(() {
                    loadingsumit = true;
                  });
                } else if (state is AuthenticatedPatient) {
                  setState((){
                    HapticFeedback.vibrate();
                    loadingsumit = false;
                    Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) => const HomePagePateint(),
                      transitionDuration: Duration.zero,
                      reverseTransitionDuration: Duration.zero,
                    ),(route) => false,
                    );
                    // Navigator.push(context, MaterialPageRoute(builder: (context)=> const HomePagePateint()));
                  });
                } else if (state is UnAUthenticated) {
                  setState(() {
                    loadingsumit = false;
                    showErrorDialog(() {
                      Navigator.of(context).pop();
                    }, context);
                  });
                }
              },
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.only(
                    top: 25,
                    left: 27,
                    right: 27,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            width: 40,
                            child: IconBack(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              hightButton: 30,
                              radiusButton: 6,
                            ),
                          ),
                          Expanded(child: Container()),
                        ],
                      ),
                      Container(
                        margin:const EdgeInsets.only(top: 15),
                        child: Image.asset(ImageAssets.app_logo),
                      ),
                      Container(
                        margin:const EdgeInsets.only(top: 18),
                        padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 3),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colorconstands.neutralWhite),
                          borderRadius: BorderRadius.circular(16)
                        ),
                        child: Text("Vibolrith’s Health System",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite,),),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 18,bottom: 18),
                        child: Text(
                          'ENTER CODE'.tr(),
                          style: ThemeConstands.headline2_SemiBold_24
                              .copyWith(color: Colorconstands.neutralWhite),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 16),
                        padding: EdgeInsets.zero,
                        decoration: BoxDecoration(
                          color: Colorconstands.lightGohan,
                          borderRadius:const BorderRadius.all(Radius.circular(25)),
                          border: Border.all(
                              color: checkfocususer == false
                                  ? Colorconstands.neutralGrey
                                  : Colorconstands.primaryColor,
                              width: checkfocususer == false
                                  ? 1
                                  : 2),
                        ),
                        child: Focus(
                          onFocusChange: (hasfocus) {
                            setState(() {
                              hasfocus
                                  ? checkfocususer = true
                                  : checkfocususer = false;
                            });
                          },
                          child: TextFormField(
                            controller: usercodeController,
                            onChanged: ((value) {
                              setState(() {
                                usercodeController.text;
                              });
                            }),
                            style: ThemeConstands.subtitle1_Regular_16.copyWith(color: Colors.black),
                            decoration: InputDecoration(
                              border: UnderlineInputBorder(borderRadius:BorderRadius.circular(16.0)),
                              contentPadding: const EdgeInsets.only(top: 8,bottom: 5,left: 18,right: 18),
                              labelText: 'USERCODE'.tr(),
                              labelStyle: ThemeConstands.subtitle1_Regular_16.copyWith(color: Colorconstands.lightTrunks),
                              enabledBorder:const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                              focusedBorder:const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            top: 16, bottom: 24),
                        padding: EdgeInsets.zero,
                        decoration: BoxDecoration(
                          color: Colorconstands.lightGohan,
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(
                              color: checkfocuspass == false
                                  ? Colorconstands.neutralGrey
                                  : Colorconstands.primaryColor,
                              width: checkfocuspass == false
                                  ? 1
                                  : 2),
                        ),
                        child: Focus(
                          onFocusChange: (hasfocus) {
                            setState(() {
                              hasfocus
                                  ? checkfocuspass = true
                                  : checkfocuspass = false;
                            });
                          },
                          child: TextFormField(
                            controller: passcodeController,
                            onChanged: ((value) {
                              setState(() {
                                passcodeController.text;
                              });
                            }),
                            obscureText: passwordshow,
                            style: ThemeConstands
                                .subtitle1_Regular_16,
                            decoration: InputDecoration(
                              suffix: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      if (passwordshow) {
                                        passwordshow = false;
                                      } else {
                                        passwordshow = true;
                                      }
                                    });
                                  },
                                  child: Text(
                                      passwordshow == true
                                          ? 'SHOW'.tr()
                                          : 'HIDE'.tr(),
                                      style: ThemeConstands
                                          .headline6_Regular_14_20height
                                          .copyWith(
                                        color: Colorconstands
                                            .lightBulma,
                                        decoration: TextDecoration
                                            .underline,
                                      ))),
                              border: UnderlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(16.0),
                              ),
                              contentPadding:
                                  const EdgeInsets.only(
                                      top: 8,
                                      bottom: 5,
                                      left: 18,
                                      right: 18),
                              labelText: 'PASSWORD'.tr(),
                              labelStyle: ThemeConstands
                                  .subtitle1_Regular_16
                                  .copyWith(
                                      color: Colorconstands
                                          .lightTrunks),
                              enabledBorder:
                                  const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors
                                              .transparent)),
                              focusedBorder:
                                  const UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors
                                              .transparent)),
                            ),
                          ),
                        ),
                      ),
                      usercodeController.text == "" ||
                                passcodeController.text == ""?Container():const Divider(),
                      ButtonIconLeftWithTitle(
                        panddingVerButton: 6, 
                        iconImage: ImageAssets.icon_login, 
                        iconColor: Colorconstands.primaryColor,
                        buttonColor: Colorconstands.neutralWhite,
                        onTap: usercodeController.text == "" ||
                                passcodeController.text == ""
                            ? null
                            : () {
                              BlocProvider.of<AuthBloc>(context).add(SignInPatientRequested(loginName:usercodeController.text.toString(),password:passcodeController.text.toString() ));
                            },
                        panddinHorButton: 22,
                        radiusButton: 25,
                        heightButton: 55,
                        textStyleButton: ThemeConstands
                            .button_SemiBold_16
                            .copyWith(
                                color:usercodeController.text == "" ||
                                passcodeController.text == ""?Colors.transparent:
                                    Colorconstands.primaryColor),
                        title: 'CONTINUE'.tr(),
                      ),
                      Container(
                        height: 160,
                      )
                    ],
                  ),
                ),
              ),
            ),
            loadingsumit == true?
            Container(
              color: Colorconstands.darkGray.withOpacity(0.6),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child:const Center(
                child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
              ),
            ):Container(height: 0,)
          ],
        ),
      ),
    );
  }
}
