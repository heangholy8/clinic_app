import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:clinic_application/storages/save_storage.dart';
import 'package:clinic_application/service/apis/auth_api/authentication_api.dart';
import 'package:equatable/equatable.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthApi authApi;
  final SaveStoragePref _savePref = SaveStoragePref();
  AuthBloc({required this.authApi}) : super(UnAUthenticated()) {
    on<SignInRequested>((event, emit) async {
      emit(Loading());
      try {
        await authApi.signInRequestApi(
          clinicSecretKey: event.secretKey,
          loginName: event.loginName,
          password: event.password, 
        ).then((value) {
        _savePref.saveJsonToken(authModel: json.encode(value));
        _savePref.saveConfirmInformationLogin(login: "Doctor");
        _savePref.saveKeySecryt(key: value.clinicData!.secretKey.toString());
        });
        emit(Authenticated());
        } catch (e) {
        emit(
          AuthError(e.toString()),
        );
        emit(UnAUthenticated());
      }
    });

    on<SignInPatientRequested>((event, emit) async {
      emit(Loading());
      try {
        await authApi.signInRequestPetientApi(
          loginName: event.loginName,
          password: event.password, 
        ).then((value) {
        _savePref.saveJsonTokenPatient(authPatientModel: json.encode(value));
        _savePref.saveConfirmInformationLogin(login: "Patient");
        _savePref.saveKeySecryt(key: value.userActive!.clinicToken.toString());
        });
        emit(AuthenticatedPatient());
        } catch (e) {
        emit(
          AuthError(e.toString()),
        );
        emit(UnAUthenticated());
      }
    });
  }
}
