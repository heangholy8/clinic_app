// ignore_for_file: must_be_immutable

part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class SignInRequested extends AuthEvent {
  final String loginName;
  final String password;
  final String secretKey;

  SignInRequested(
      {
        required this.secretKey,
      required this.loginName,
      required this.password,});
}

class SignInPatientRequested extends AuthEvent {
  final String loginName;
  final String password;

  SignInPatientRequested(
      {
      required this.loginName,
      required this.password,});
}


class SignOutRequeted extends AuthEvent{}