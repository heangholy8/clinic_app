part of 'list_clinic_bloc.dart';

class ListClinicEvent extends Equatable {
  const ListClinicEvent();

  @override
  List<Object> get props => [];
}

class GetListClinic extends ListClinicEvent{}
