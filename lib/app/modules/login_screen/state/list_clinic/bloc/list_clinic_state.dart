part of 'list_clinic_bloc.dart';

class ListClinicState extends Equatable {
  const ListClinicState();
  
  @override
  List<Object> get props => [];
}

class ListClinicInitial extends ListClinicState {}

class ListClinicLoading extends ListClinicState {}

class ListClinicLoaded extends ListClinicState {
  ListClinicModel listClinicModel;
  ListClinicLoaded({required this.listClinicModel});
}

class ListClinicError extends ListClinicState {}
