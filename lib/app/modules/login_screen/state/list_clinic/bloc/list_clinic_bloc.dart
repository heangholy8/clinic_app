import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/doctor_model/list_clinis_model/list_clinic_model.dart';
import 'package:clinic_application/service/apis/auth_api/authentication_api.dart';
import 'package:equatable/equatable.dart';
part 'list_clinic_event.dart';
part 'list_clinic_state.dart';

class ListClinicBloc extends Bloc<ListClinicEvent, ListClinicState> {
  AuthApi  listClinic;
  List<DataClinic>? preventData;
  String? secretKey;
  ListClinicBloc({required this.listClinic}) : super(ListClinicInitial()) {
    on<GetListClinic>((event, emit) async{
      emit(ListClinicLoading());
      try{
        var data = await listClinic.getListClinicApi();
        emit(ListClinicLoaded(listClinicModel: data));
        preventData = data.data;
      }catch(e){
        emit(ListClinicError());
        preventData = [DataClinic(id: 0,customerName: "Error",),];
      }
    });
  }
}
