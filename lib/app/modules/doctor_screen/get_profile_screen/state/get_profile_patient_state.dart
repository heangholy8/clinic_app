part of 'get_profile_patient_bloc.dart';

class GetProfilePatientState extends Equatable {
  const GetProfilePatientState();
  
  @override
  List<Object> get props => [];
}

class GetProfilePatientInitial extends GetProfilePatientState {}

class  GetProfilePatientLoading extends GetProfilePatientState{}

class  GetProfilePatientLoaded extends GetProfilePatientState {
  final GetPatientModal? getPatientModal;
  const GetProfilePatientLoaded({required this.getPatientModal});
}

class  GetProfilePatientError extends GetProfilePatientState {}
