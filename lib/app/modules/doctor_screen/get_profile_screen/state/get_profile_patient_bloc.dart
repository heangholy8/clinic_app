import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/get_profile_model/get_profile_api.dart';
import 'package:clinic_application/service/apis/doctor_api/get_profile_api/get_profile_api.dart';
import 'package:equatable/equatable.dart';

part 'get_profile_patient_event.dart';
part 'get_profile_patient_state.dart';

class GetProfilePatientBloc extends Bloc<GetProfilePatientEvent, GetProfilePatientState> {
  final GetProfilePatient getProfilePatient;
  GetProfilePatientBloc({required this.getProfilePatient}) : super(GetProfilePatientInitial()) {
    on<GetPatientEvent>((event, emit) async {
      emit(GetProfilePatientLoading());
        try {
          var dataPatient = await getProfilePatient.getProfileApiApi(idPatient: event.id);
          emit(GetProfilePatientLoaded(getPatientModal: dataPatient));
        } catch (e) {
          print("fhgfjh$e");
          emit(GetProfilePatientLoading());
        }
    });
  }
}
