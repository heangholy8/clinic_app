part of 'get_profile_patient_bloc.dart';

class GetProfilePatientEvent extends Equatable {
  const GetProfilePatientEvent();

  @override
  List<Object> get props => [];
}

class GetPatientEvent extends GetProfilePatientEvent{
  final String id;
  const GetPatientEvent({required this.id});
}
