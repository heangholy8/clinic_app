import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/modules/doctor_screen/get_profile_screen/state/get_profile_patient_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/scan_qr/widgets/scan_view_widget.dart';
import 'package:clinic_application/mixins/toast.dart';
import 'package:clinic_application/model/doctor_model/qr/qr_model.dart';
import 'package:clinic_application/service/apis/doctor_api/post_qr_api/post_qr_api.dart';
import 'package:clinic_application/widgets/button_widget/button_back_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import '../../../../../helper/request_camera_permission.dart';
import '../../../../core/themes/color_app.dart';
import '../../../../core/themes/themes.dart';

class ScanQrPatientScreen extends StatefulWidget {
  const ScanQrPatientScreen({Key? key,}) : super(key: key);

  @override
  State<ScanQrPatientScreen> createState() => _ScanQrPatientScreenState();
}

class _ScanQrPatientScreenState extends State<ScanQrPatientScreen> with Toast{
  MobileScannerController? _mobileScannerController;
  bool _isFlash = false;
  bool loadingsumit = false;
  bool successScan = false;
  QrModel data = QrModel();
  var idPatient;
  @override
  initState() {
    super.initState();
    _mobileScannerController = MobileScannerController();
    requestCameraPermission();
  }

  @override
  void dispose() {
    _mobileScannerController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {  
    return Scaffold(
      body: GestureDetector(
         onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: BlocListener<GetProfilePatientBloc, GetProfilePatientState>(
          listener: (context, state) {
            if(state is GetProfilePatientLoading){
              setState(() {
                loadingsumit = true;
              });
            }
            else if(state is GetProfilePatientLoaded){
              var data = state.getPatientModal!.data;
              setState(() {
                loadingsumit = false;
                showInformationDialog( (){
                  PostQrApi().postQrApi(data: data,eventName: "scan-user").then((value){
                    Navigator.of(context).pop();
                    print("post qr$value");
                    return value;
                  });
                  Navigator.of(context).pop();
                },context,name: "${data!.firstname} ${data.lastname}",nameEn: "${data.firstnameLatin} ${data.lastnameLatin}",profile: data.profile == null?"https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-image-182145777.jpg":data.profile!.fileDisplay.toString(),gender: data.gender.toString(),phone: data.phone.toString(),dob: data.dateOfBirth.toString() );
              });
            }
            else{
              setState(() {
                loadingsumit = false;
                Navigator.of(context).pop();
              });
            }
          },
          child: Container(
                        child: Stack(
                          children: [
                            Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              top: 0,
                              child: ScanViewWidget(
                                mobileScannerController: _mobileScannerController!,
                                onDetect: (barcode, args) async {
                                  if (barcode.rawValue != null) {
                                    var codeString = barcode.rawValue.toString();
                                    const start = "info/";
                                    final startIndex = codeString.indexOf(start);
                                    var qrResult = codeString.substring(startIndex + start.length);
                                     print("fasdnflksda$codeString");
                                     print("fasdnflksda$qrResult");
                                    setState(() {
                                      loadingsumit = true;
                                      PostQrApi().postQrDecryptApi(data: qrResult).then((value) {
                                        if(value !=null){
                                          BlocProvider.of<GetProfilePatientBloc>(context).add(GetPatientEvent(id: value.data!.id.toString()));
                                        }
                                        else{
                                          setState(() {
                                            loadingsumit = false;
                                          });
                                        }
                                      },);
                                      _mobileScannerController!.stop();
                                    });
                                  } else {
                                    debugPrint('Failed scan Barcode');
                                  }
                                },
                              ),
                            ),
                            Positioned(
                              top: 0,
                              left: 0,
                              right: 0,
                              bottom: 0,
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Container(
                                      color:Colorconstands.primaryColor,
                                    ),
                                  ),
                                  Container(
                                    height: 230,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            color:Colorconstands.primaryColor,
                                          ),
                                        ),
                                        Container(width: 230,
                                        decoration: BoxDecoration(
                                          border: Border.all(color:Colorconstands.primaryColor,)
                                        ),
                                        child: Stack(
                                          children: [
                                      //============= top Rigeht radius ========================
                                            Positioned(
                                              top: -57.3,
                                              left: -57.3,
                                              child: SvgPicture.asset("assets/images/1.svg",color:Colorconstands.primaryColor,width: 480,height: 480,),
                                            ),
                                            Positioned(
                                              top: -1,
                                              left: -1,
                                              child: SvgPicture.asset("assets/images/1.svg",width: 60,height: 60,),
                                            ),
                                            Positioned(
                                              top: -0.1,
                                              left: -0.1,
                                              child: SvgPicture.asset("assets/images/1.svg"),
                                            ),
                                      //============= top Rigeht radius ========================
                                            Positioned(
                                              top: -53.3,
                                              right: -53.3,
                                              child: SvgPicture.asset("assets/images/2.svg",color:Colorconstands.primaryColor,width: 480,height: 480,),
                                            ),
                                            Positioned(
                                              top: -1,
                                              right: -1,
                                              child: SvgPicture.asset("assets/images/2.svg",width: 65,height: 65,),
                                            ),
                                            Positioned(
                                              top: -0.1,
                                              right: -0.1,
                                              child: SvgPicture.asset("assets/images/2.svg"),
                                            ),
                                      //============= Buttom Rigeht radius ========================
                                            Positioned(
                                              bottom: -58.7,
                                              right: -58.7,
                                              child: SvgPicture.asset("assets/images/4.svg",color:Colorconstands.primaryColor,width: 480,height: 480,),
                                            ),
                                            Positioned(
                                              bottom: -1,
                                              right: -1,
                                              child: SvgPicture.asset("assets/images/4.svg",width: 60,height: 60,),
                                            ),
                                            Positioned(
                                              bottom: -0.1,
                                              right: -0.1,
                                              child: SvgPicture.asset("assets/images/4.svg"),
                                            ),
                      
                                      //============= Buttom Left radius ========================
                                            Positioned(
                                              bottom: -52.5,
                                              left: -52.5,
                                              child: SvgPicture.asset("assets/images/3.svg",color:Colorconstands.primaryColor,width: 480,height: 480,),
                                            ),
                                            Positioned(
                                              bottom: -1,
                                              left: -1,
                                              child: SvgPicture.asset("assets/images/3.svg",width: 67,height: 67,),
                                            ),
                                            Positioned(
                                              bottom: -0.1,
                                              left: -0.1,
                                              child: SvgPicture.asset("assets/images/3.svg"),
                                            ),
                                          ],
                                        ),),
                                        Expanded(
                                          child: Container(
                                            color:Colorconstands.primaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      color:Colorconstands.primaryColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              top: 20,
                              left: 0,
                              right: 0,
                              child: SafeArea(
                                bottom: false,
                                child: Column(
                                  children: [
                                    Row(
                                    children: [
                                      Container(
                                        margin:const EdgeInsets.only(left: 28),
                                        child:const Iconbuttonback(),
                                      ),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Text(
                                            ''.tr().toUpperCase(),
                                            style: ThemeConstands.headline2_SemiBold_242
                                                .copyWith(color: Colorconstands.neutralWhite),
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Container(
                                          margin:const EdgeInsets.only(right: 12),
                                          width: 40,
                                          height: 40,
                                          //child:const Icon(Icons.add,size: 26,color: Colorconstands.neutralWhite,),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    margin:const EdgeInsets.only(top: 18),
                                    child: Image.asset(ImageAssets.app_logo,),
                                  ),
                                  Container(
                                    margin:const EdgeInsets.only(top: 25),
                                    padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 3),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colorconstands.neutralWhite),
                                      borderRadius: BorderRadius.circular(16)
                                    ),
                                    child: Text("Vibolrith’s Health System",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite,),),
                                  ),
                                        ],
                                      ),
                                    ),
                                  ),
                            Positioned(
                              bottom: 0,
                              left: 0,
                              right: 0,
                              top: 290,
                              child: Center(
                                child: InkWell(
                                  onTap: () async {
                                    await _mobileScannerController!.toggleTorch();
                                    setState(() {
                                      _isFlash = !_isFlash;
                                    });
                                  },
                                  child: Container(
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      color: Colorconstands.primaryColor,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: _isFlash
                                          ? const Icon(
                                              Icons.flash_on,
                                              color: Colors.white,
                                              size: 34,
                                            )
                                          : const Icon(
                                              Icons.flash_off,
                                              color: Colors.white,
                                              size: 34,
                                            ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            loadingsumit == false?Container():Positioned(
                              top: 0,left: 0,right: 0,bottom: 0,
                              child: Container(
                                color: Colorconstands.primaryColor.withOpacity(0.6),
                                child:const Center(
                                  child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
        ),
      ),
    );
  }
}

//https://dev-web-clinic.camemis-learn.com/guest/info/gAAAAABmTChe3wweGpy2SI_LdUdUwhg3k5tU0l8NkpYmGylFCmWgsxqgZDUvjHmar9FqdS69470eZgJ_ksFdBqVmsVHQii0A5TfdRa4LD2egosjnxU8TKxuGFjpddCgpLkyZ02SHTYYrjB_RcWAk2OanZXuj5aT7LmT2nmBvOFcwGXq-cyE0iKoOL209AjHl6JPB9fAG3G5W8wedIIlLqzGwhwiZC7kyQA==


