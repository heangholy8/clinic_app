// ignore_for_file: avoid_print

import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/scan_qr/view/scan_qr_screen.dart';
import 'package:clinic_application/widgets/button_widget/button_icon_left_with_title.dart';
import 'package:clinic_application/widgets/button_widget/button_widget_custom.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class StartScanQrScreen extends StatefulWidget {
  const StartScanQrScreen({
    Key? key,}) : super(key: key);
  @override
  State<StartScanQrScreen> createState() => _StartScanQrScreenState();
}

class _StartScanQrScreenState extends State<StartScanQrScreen> {
  bool isTrail = false;
  bool isoffline = true;
  
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin:const EdgeInsets.only(top: 38),
                      child: Image.asset(ImageAssets.app_logo,),
                    ),
                    Container(
                      margin:const EdgeInsets.only(top: 25),
                      padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 3),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colorconstands.neutralWhite),
                        borderRadius: BorderRadius.circular(16)
                      ),
                      child: Text("Vibolrith’s Health System",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite,),),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 28),
                      child:Text("Track your medical history and book clinic appointments effortlessly.",style: ThemeConstands.headline3_Medium_20_26height.copyWith(color: Colorconstands.neutralWhite,),textAlign: TextAlign.center,),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(horizontal: 48),
                      child: ButtonWidgetCustom(
                        onTap: () {
                          setState(() {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> const ScanQrPatientScreen()));
                            });
                        },
                        title: "Scan".tr(),
                        textStyleButton: ThemeConstands
                            .headline1_SemiBold_32
                            .copyWith(
                          color: Colorconstands.primaryColor,
                        ),
                        buttonColor: Colorconstands.neutralWhite,
                        radiusButton: 75,
                        heightButton: 150,
                        weightButton: 150,
                        panddinHorButton: 25,
                        panddingVerButton: 25, 
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
