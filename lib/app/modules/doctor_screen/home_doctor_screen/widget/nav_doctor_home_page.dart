import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/home_doctor_screen/view/home_doctor_screen.dart';
import 'package:clinic_application/app/modules/doctor_screen/profile_doctor_screen.dart/view/profile_doctor_screen.dart';
import 'package:clinic_application/app/modules/doctor_screen/notification_screen/view/motification_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NavDoctorHomeScreen extends StatefulWidget {
  String imageProfile;
  int acitiveNav;
  NavDoctorHomeScreen({Key? key,required this.imageProfile,required this.acitiveNav}) : super(key: key);

  @override
  State<NavDoctorHomeScreen> createState() => _NavDoctorHomeScreenState();
}

class _NavDoctorHomeScreenState extends State<NavDoctorHomeScreen> {
     
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.symmetric(horizontal: 18),
      padding:const EdgeInsets.symmetric(horizontal: 28),
      height: 69,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18),
        color: Colorconstands.neutralWhite
      ),
      child: Row(
        children: [
          Expanded(
            child: MaterialButton(
              onPressed: () {
                if(widget.acitiveNav !=1){
                  Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) => const HomePageDoctor(),
                    transitionDuration: Duration.zero,
                    reverseTransitionDuration: Duration.zero,
                  ),(route) => false,
                  );
                }
              },
              padding:const EdgeInsets.all(0),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                   height: 5,
                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(1),color: widget.acitiveNav == 1? Colorconstands.primaryColor:Colors.transparent),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(widget.acitiveNav == 1?ImageAssets.home_fill:ImageAssets.homeOutline),
                      Text("Home",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: widget.acitiveNav == 1?Colorconstands.primaryColor:Colorconstands.black,fontWeight: widget.acitiveNav == 1?FontWeight.bold:FontWeight.normal),textAlign: TextAlign.center,),
                    ],
                  ),
                  const SizedBox(height: 5,),
                ],
              ),
            ),
          ),
          const SizedBox(width: 5,),
          Expanded(
            child: GestureDetector(
              onTap: () {
                if(widget.acitiveNav !=2){
                  Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) => const ProfileDoctorScreen(),
                      transitionDuration: Duration.zero,
                      reverseTransitionDuration: Duration.zero,
                    ),(route) => false,
                  );
                }
              },
              child: Container(
                height: 60,
                decoration:const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colorconstands.primaryColor,
                ),
                padding: EdgeInsets.all(widget.acitiveNav == 2? 3:0),
                child: CircleAvatar(
                  backgroundImage: NetworkImage(widget.imageProfile),
                ),
              ),
            ),
          ),
          const SizedBox(width: 5,),
          Expanded(
            child: MaterialButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) => const NotificationScreen(fromPatient: false,),
                      transitionDuration: Duration.zero,
                      reverseTransitionDuration: Duration.zero,
                    ),(route) => false,
                  );
              },
              padding:const EdgeInsets.all(0),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                   height: 5,
                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(1),color: widget.acitiveNav == 3? Colorconstands.primaryColor:Colors.transparent),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset( widget.acitiveNav == 3? ImageAssets.notification_fill:ImageAssets.notification),
                      Text("Notification",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color:widget.acitiveNav == 3?Colorconstands.primaryColor: Colorconstands.black,fontWeight: widget.acitiveNav == 3?FontWeight.bold:FontWeight.normal),textAlign: TextAlign.center,),
                    ],
                  ),
                  const SizedBox(height: 5,),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}