import 'dart:developer';

import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/state/vitalsign_create_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/view/vital_sign_form_create_screen.dart';
import 'package:clinic_application/app/modules/doctor_screen/home_doctor_screen/widget/card_menu_home_doctor.dart';
import 'package:clinic_application/app/modules/doctor_screen/home_doctor_screen/widget/nav_doctor_home_page.dart';
import 'package:clinic_application/app/modules/doctor_screen/profile_doctor_screen.dart/state/profile_doctor_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/scan_qr/view/scan_qr_screen.dart';
import 'package:clinic_application/app/modules/doctor_screen/vist_code_screen/state/list_menu_voice_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/vist_code_screen/view/visit_code_screen.dart';
import 'package:clinic_application/mixins/toast.dart';
import 'package:clinic_application/model/doctor_model/pateint_visit_model/patient_vist_list_model.dart';
import 'package:clinic_application/state_management/doctor/list_patoent_visit/list_patient_visit_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePageDoctor extends StatefulWidget {
  const HomePageDoctor({Key? key}) : super(key: key);

  @override
  State<HomePageDoctor> createState() => _HomePageDoctorState();
}

bool expainWidgetEmergency = false;
String nameUser = "-----";
String profilePicture = "https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-image-182145777.jpg";

class _HomePageDoctorState extends State<HomePageDoctor> with Toast {
  
  bool checkfocususer = false;
  bool checkfocuspass = false;
  bool showMenuVisit = false;
  bool isLoadingSendCode = false;
  bool isExpanded = false;
  String visitCode = "";
  String idPatientVisit = "";
  String patientGender = "";
  String patientAge = "";
  FocusNode myfocus = FocusNode();
  List<DataPatientVisit>? _foundDataPatientVisit;
  TextEditingController searchController = TextEditingController();
  @override
  void initState() {
    setState(() {
      BlocProvider.of<ProfileDoctorBloc>(context).add(GetProfileDoctor());
      
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            BlocListener<ProfileDoctorBloc, ProfileDoctorState>(
              listener: (context, state) {
                if(state is ProfileDoctorLoaded){
                  var dataProfile = state.profileDoctor.data;
                  setState(() {
                    nameUser =  translate == "km"?"${dataProfile!.lastname.toString()} ${dataProfile.firstname.toString()}":"${dataProfile!.lastnameLatin.toString()} ${dataProfile.firstnameLatin.toString()}";
                    profilePicture = dataProfile.profile == null? profilePicture:profilePicture;
                  });
                }
              },
              child: Container(),
            ),
            Container(
              margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 18),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(ImageAssets.app_logo,width: MediaQuery.of(context).size.width/3,),
                        Container(
                          margin:const EdgeInsets.only(top: 5),
                          padding:const EdgeInsets.symmetric(horizontal: 5,vertical: 2),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colorconstands.neutralWhite),
                            borderRadius: BorderRadius.circular(16)
                          ),
                          child:const Text("Vibolrith’s Health System",style: TextStyle(fontSize: 9,color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("Welcome back!",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                        Text(nameUser,style: ThemeConstands.headline2_SemiBold_242.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 28,right: 28),
              alignment: Alignment.centerLeft,
              width: MediaQuery.of(context).size.width,
              height: 45,
              padding: const EdgeInsets.symmetric(horizontal: 18),
              decoration: BoxDecoration(
                border: Border.all(color: Colorconstands.neutralGrey),
                color: Colorconstands.neutralWhite,
                borderRadius: BorderRadius.only(topLeft: const Radius.circular(25),topRight: const Radius.circular(25),bottomLeft: Radius.circular(isExpanded == true?0:25),bottomRight: Radius.circular(isExpanded == true?0:25)),
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Focus(
                      onFocusChange: (value) {
                        setState(() {
                          isExpanded = value;
                          isKeyboard = value;
                          if(isExpanded == true && _foundDataPatientVisit==null){
                            BlocProvider.of<ListPatientVisitBloc>(context).add(GetListPatientVisit());
                          }
                        });
                      },
                      child: TextFormField(
                        scrollPhysics:const BouncingScrollPhysics(),
                        controller: searchController,
                        style: ThemeConstands.headline5_Medium_16,
                        focusNode: myfocus,
                        onChanged: (value) {
                            setState(() {
                              showMenuVisit = false;
                            });
                           _filterPatientVisit(value, BlocProvider.of<ListPatientVisitBloc>(context).pateintVisitData!);
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(vertical: 6.0,horizontal: 8.0),
                          enabledBorder:InputBorder.none,
                          focusedBorder:InputBorder.none,
                          border:const UnderlineInputBorder(),
                          hintText: 'Patient Vistit'.tr(),
                          hintStyle: ThemeConstands .subtitle1_Regular_16,
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isExpanded = !isExpanded;
                        if (isExpanded) {
                          myfocus.requestFocus();
                        } else {
                          myfocus.unfocus();
                        }
                      });
                    },
                    child: SizedBox(
                      width: 38,
                      height: 38,
                      child: !isExpanded? const Icon(Icons.keyboard_arrow_down) : const Icon(
                              Icons.keyboard_arrow_up,color: Colorconstands.darkBordersDefault,),
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   margin: const EdgeInsets.only(left: 28,right: 28),
            //   padding: EdgeInsets.zero,
            //   decoration: BoxDecoration(
            //     color: Colorconstands.lightGohan,
            //     borderRadius: BorderRadius.circular(25),
            //     border: Border.all(
            //         color: checkfocususer == false
            //             ? Colorconstands.neutralGrey
            //             : Colorconstands.primaryColor,
            //         width: checkfocususer == false
            //             ? 1
            //             : 2),
            //   ),
            //   child: Row(
            //     children: [
            //       Expanded(
            //         child: TextFormField(
            //           controller:codeController,
            //           onChanged: ((value) {
            //             setState(() {
            //              codeController.text;
            //              showMenuVisit = false;
            //             });
            //           }),
            //           style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colors.black),
            //           decoration: InputDecoration(
            //             border: UnderlineInputBorder(borderRadius:BorderRadius.circular(16.0)),
            //             contentPadding:const EdgeInsets.only(top: 8,bottom: 5,left: 18,right: 18),
            //             labelText: 'Code Visit'.tr(),
            //             labelStyle: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.lightTrunks),
            //             enabledBorder:const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
            //             focusedBorder:const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
            //           ),
            //         ),
            //       ),
            //       codeController.text == ""?Container():TextButton(
            //         onPressed: (){
            //           setState(() {
            //             FocusScope.of(context).unfocus();
            //           });
            //           BlocProvider.of<ListMenuVoiceBloc>(context).add(PostCodeGetListMenuVoice(code: codeController.text.toUpperCase()));
            //         }, 
            //         child: isLoadingSendCode==true? const Center(child: CircularProgressIndicator(),):Text('SEND'.tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.primaryColor),),
        
            //       )
            //     ],
            //   ),
            // ),
            SizedBox(height: isExpanded==true?0:22,),
            Container(
              margin:const EdgeInsets.symmetric(horizontal: 28),
              child:const Divider(height: 0,),
            ),
            Expanded(
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Container(
                      margin:const EdgeInsets.symmetric(horizontal: 28),
                      child: Column(
                        children: [
                          SizedBox(height: isExpanded == true? 50:28,),
                          Container(
                            height: 170,
                            child: CardMenuHomeDoctor(
                              widthIcon: 70,
                              heughtIcon: 70,
                              style: ThemeConstands.headline4_SemiBold_18,
                              width: MediaQuery.of(context).size.width,
                              onPressed: isExpanded == false?() {
                                Navigator.push(context,PageRouteBuilder(
                                    pageBuilder: (context, animation1, animation2) =>const ScanQrPatientScreen(),
                                    transitionDuration: Duration.zero,
                                    reverseTransitionDuration: Duration.zero,
                                  ),
                                );
                              }:null,
                              title: "Scan Pateint",
                              icons: ImageAssets.scan,
                            ),
                          ),
                          const SizedBox(height: 18,),
                          BlocListener<ListMenuVoiceBloc, ListMenuVoiceState>(
                            listener: (context, state) {
                              if(state is ListMenuVoiceLoading){
                                setState(() {
                                  showMenuVisit = false;
                                  isLoadingSendCode = true;
                                });
                              }
                              else if(state is ListMenuVoiceLoaded){
                                setState(() {
                                  showMenuVisit = true;
                                  isLoadingSendCode = false;
                                });
                              }
                              else{
                                setState(() {
                                  showMenuVisit = false;
                                  isLoadingSendCode = false;
                                  showErrorDialog(() {
                                    Navigator.of(context).pop();
                                  }, context);
                                });
                              }
                            },
                            child:Container(
                                height: 170,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        height: 170,
                                        child: CardMenuHomeDoctor(
                                          widthIcon: 70,
                                          heughtIcon: 70,
                                          style: ThemeConstands.headline4_SemiBold_18,
                                          width: MediaQuery.of(context).size.width,
                                          onPressed: showMenuVisit==true?() {
                                            BlocProvider.of<ListMenuVoiceBloc>(context).add(PostCodeGetListMenuVoice(code:visitCode));
                                            Navigator.push(context,PageRouteBuilder(
                                                pageBuilder: (context, animation1, animation2) =>  VisitCodeScreen(visitCode:visitCode),
                                                transitionDuration: Duration.zero,
                                                reverseTransitionDuration: Duration.zero,
                                              ),
                                            );
                                          }:null,
                                          title: "Patient Visit Voice",
                                        icons: ImageAssets.voiceMenu,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 18,),
                                    Expanded(
                                      child: Container(
                                        height: 170,
                                        alignment: Alignment.center,
                                        child: CardMenuHomeDoctor(
                                          widthIcon: 70,
                                          heughtIcon: 70,
                                          style: ThemeConstands.headline4_SemiBold_18,
                                          width: MediaQuery.of(context).size.width,
                                          onPressed: showMenuVisit==true? (){
                                            Navigator.push(context,PageRouteBuilder(
                                                pageBuilder: (context, animation1, animation2) => FormCreateVitalSingScreem(patient: false,patientGender: patientGender ,patientIdAge: patientAge,patientIdVisit: idPatientVisit,),
                                                transitionDuration: Duration.zero,
                                                reverseTransitionDuration: Duration.zero,
                                              ),
                                            );
                                          }:null,
                                          title: "Create\nVital sign",
                                          icons: ImageAssets.vital_sing,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                          ),
                          const SizedBox(height: 48,),
                        ],
                      ),
                    ),
                  ),
                  isExpanded == true?Positioned(
                    child: GestureDetector(
                      onTap: (){
                        setState(() {
                          isExpanded = !isExpanded;
                          myfocus.unfocus();
                        });
                      },
                      child: Container(
                        //margin: EdgeInsets.symmetric(horizontal: 28),
                        color: Colorconstands.black.withOpacity(0.0),
                      ),
                    ),
                  ):const SizedBox(height: 0,),
                  isExpanded == true?Positioned(
                    top: 0,left: 28,right: 28,
                    child: Container(
                      alignment: Alignment.topCenter,
                      height: 172,
                      decoration: const BoxDecoration(
                        color: Colorconstands.neutralWhite,
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(18),bottomRight: Radius.circular(18)),
                        boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                offset: Offset(0, 2),
                                blurRadius: 15.0)
                          ],
                      ),
                      child: BlocConsumer<ListPatientVisitBloc, ListPatientVisitState>(
                        listener: (context, state) {
                          if (state is ListPatientVisitLoaded) {
                            setState(() {
                              _foundDataPatientVisit = BlocProvider.of<ListPatientVisitBloc>(context).pateintVisitData;
                            });
                          }
                        },
                        builder: (context, state) {
                          if(state is ListPatientVisitLoading){
                            return Container(
                              height: 80,
                              child: const Center(
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }
                          else if(state is ListPatientVisitLoaded){
                            var dataStatus = state.listPatientVisit.visitStatusData;
                            // _foundDataPatientVisit = BlocProvider.of<ListPatientVisitBloc>(context).pateintVisitData;
                            return _foundDataPatientVisit!.isEmpty? 
                            Container(
                              alignment: Alignment.center,
                              height: 50,
                              child: const Text("Patient visit empty!",style: ThemeConstands.headline5_Medium_16,textAlign: TextAlign.center,))
                            :ListView.separated(
                              padding:const EdgeInsets.all(0),
                              shrinkWrap: true,
                              itemCount: _foundDataPatientVisit!.length,
                              itemBuilder: (context, index) {
                                return MaterialButton(
                                  height: 35,
                                  onPressed: (){
                                    setState(() {
                                      visitCode = _foundDataPatientVisit![index].visitCode.toString();
                                      searchController.text = _foundDataPatientVisit![index].patientData!.fullname.toString();
                                      isExpanded = false;
                                      patientAge = _foundDataPatientVisit![index].patientData!.age.toString();
                                      patientGender = _foundDataPatientVisit![index].patientData!.gender.toString();
                                      idPatientVisit = _foundDataPatientVisit![index].patientVisitId.toString();
                                      myfocus.unfocus();
                                      BlocProvider.of<ListMenuVoiceBloc>(context).add(PostCodeGetListMenuVoice(code: _foundDataPatientVisit![index].visitCode.toString()));
                                    });
                                  },
                                  padding:const EdgeInsets.all(0),
                                  child: Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 22),
                                    alignment: Alignment.center,
                                    height: 35,
                                      child: Row(
                                        children: [
                                          Expanded(child: Text(translate =="km"? _foundDataPatientVisit![index].patientData!.fullname.toString():_foundDataPatientVisit![index].patientData!.fullnameLatin.toString(),style: ThemeConstands.headline5_Medium_16,textAlign:TextAlign.left,)),
                                          Text( translate =="km"?dataStatus!.where((element) => element.value.toString() == _foundDataPatientVisit![index].status.toString(),).toList()[0].name.toString():dataStatus!.where((element) => element.value.toString() == _foundDataPatientVisit![index].status.toString(),).toList()[0].nameLatin.toString(),style: ThemeConstands.caption_Regular_12.copyWith(color: Color(int.parse(dataStatus.where((element) => element.value.toString() == _foundDataPatientVisit![index].status.toString(),).toList()[0].color.toString().replaceAll("#", "0xFF")))),textAlign:TextAlign.right,)
                                        ],
                                      ),
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return const Divider(thickness: 0.5,height: 1,color: Colorconstands.neutralGrey,);
                              },
                            );
                          }
                          else {
                            return Container(
                              height: 80,
                                child: const Text("Error Data",textAlign: TextAlign.center,),
                            );
                          }
                        },
                      ),
                    ),
                  ):const SizedBox(height: 0,)
                ],
              ),
            ),
            isKeyboard == false?Container(
              padding:const EdgeInsets.only(bottom: 28,top: 16),
              decoration:const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colorconstands.primaryColor,Color(0xFF0C7376), Color(0xFF053031),])),
              child: NavDoctorHomeScreen(acitiveNav: 1,imageProfile: profilePicture,)):Container()
          ],
        ),
      ),
    );
  }
  void _filterPatientVisit(String value, List<DataPatientVisit> allResult) {

    log(value);
    List<DataPatientVisit> results = [];
    if (value.isEmpty) {
      results = allResult;
    } else {
      results = allResult.where((element) => element.patientData!.fullname!.toString().toLowerCase().contains(value.toLowerCase()) || element.patientData!.fullnameLatin!.toString().toLowerCase().contains(value.toLowerCase())).toList();
    }
    setState(() {
      _foundDataPatientVisit = results;
    });
  }
}