import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/state/vitalsign_create_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/view/result_calculate_vital_sign.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/service/apis/doctor_api/vital_sign_data_api/vital_sign_data_api.dart';
import 'package:clinic_application/widgets/button_widget/button_widget_custom.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FormCreateVitalSingScreem extends StatefulWidget {
  bool patient;
  String patientIdVisit;
  String patientIdAge;
  String patientGender;
  FormCreateVitalSingScreem({super.key,required this.patient,required this.patientIdVisit,required this.patientIdAge,required this.patientGender});

  @override
  State<FormCreateVitalSingScreem> createState() => _FormCreateVitalSingScreemState();
}

class _FormCreateVitalSingScreemState extends State<FormCreateVitalSingScreem> {
  
  int sex = 1;
  TextEditingController ageController = TextEditingController();
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  TextEditingController diastolicController = TextEditingController();
  TextEditingController systolicController = TextEditingController();
  TextEditingController glucoseController = TextEditingController();
  TextEditingController temperatureController = TextEditingController();
  TextEditingController spo2Controller = TextEditingController();
  TextEditingController pulseController = TextEditingController();
  TextEditingController respiratoryController = TextEditingController();
  TextEditingController chiefComplaintController = TextEditingController();
  TextEditingController currentMedicationController = TextEditingController();
  TextEditingController historyOfIllnessController = TextEditingController();
  int glucoseOption = 0;
  bool sexMale = true;

  bool loading = false;

  void showSnackBar(BuildContext context,String title,Color color,Icon icon){
    final snackBar =  SnackBar(
      padding:const EdgeInsets.symmetric(vertical: 14,horizontal: 28),
      behavior: SnackBarBehavior.floating,
      content:  Row(
        children: [
          icon,
          const SizedBox(width: 22,),
          Expanded(child: Text(title,style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,)),
        ],
      ),
      backgroundColor: color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18),
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    setState((){
      BlocProvider.of<VitalsignCreateBloc>(context).add(GetVitalSignDataPaitient(idpatientVisit: widget.patientIdVisit));
      ageController.text = widget.patientIdAge ?? "";
      sexMale =  widget.patientGender.toString() == "2" ? false: true;
    });
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              BlocListener<VitalsignCreateBloc,VitalsignCreateState>(
                listener: (context, state) {
                  if(state is VitalsignDataLoaded){
                    var data = state.vitalsignDataModel.data;
                    setState(() {
                      weightController .text = data!.weight.toString();                     
                      heightController .text = data.height.toString();                     
                      diastolicController .text = data.diastolic.toString();                    
                      systolicController .text = data.systolic.toString();                     
                      glucoseController .text = data.glucose.toString();                    
                      temperatureController .text = data.temperature.toString();                    
                      spo2Controller .text = data.spo2.toString();                     
                      pulseController .text = data.pulse.toString();                    
                      respiratoryController .text = data.respiratoryRate.toString();                    
                      chiefComplaintController .text = data.chiefComplaint.toString();              
                      currentMedicationController.text = data.currentMedication.toString();    
                      historyOfIllnessController.text = data.historyOfIllness.toString();            
                    });
                  }
                },
                child: Container(),
              ),
              HeaderFeature(titleHeader: "Vital Sign From"),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin: const EdgeInsets.all(18),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Container(
                        padding:const EdgeInsets.symmetric(vertical: 6),
                        decoration: BoxDecoration(
                          color: Colorconstands.neutralWhite,
                          borderRadius: BorderRadius.circular(14),
                        ),
                        child: Column(
                          children: [
                            const SizedBox(height: 8,),
                            Text("Measurament",style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),),
                            widget.patient == true?Container():Column(
                              children: [
                                ListTile(
                                  title: Text("Chief Complaint",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                                  subtitle: Container(
                                    margin:const EdgeInsets.only(top: 8),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: chiefComplaintController,
                                      style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                      decoration:InputDecoration(
                                        border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                        contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                        hintText: 'Type Chief Complaint here',
                                        hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                        focusedBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                        ),
                                        enabledBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                ListTile(
                                  title: Text("Current Medication",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                                  subtitle: Container(
                                    margin:const EdgeInsets.only(top: 8),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: currentMedicationController,
                                      style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                      decoration:InputDecoration(
                                        border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                        contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                        hintText: 'Type Current Medication here',
                                        hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                        focusedBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                        ),
                                        enabledBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            ListTile(
                              title: Text("Age",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: ageController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type age here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Systolic (mmHg)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: systolicController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Systolic here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Diastolic (mmHg)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: diastolicController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Diastolic here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Respiratory Rate (mn)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: respiratoryController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Respiratory Rate here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Spo2 (%Ra)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: spo2Controller,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Spo2 here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Pulse (mn)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: pulseController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Pulse here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Temperature (C)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: temperatureController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Temperature here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Glucose (mg/dl)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: glucoseController,
                                  onChanged: (value) {
                                    setState(() {
                                      if(glucoseController.text == ""){
                                        glucoseOption = 0;
                                      }
                                    });
                                  },
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Glucose here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            glucoseController.text == ""?Container():Container(
                              margin:const EdgeInsets.symmetric(horizontal: 18),
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: (){
                                          setState(() {
                                            glucoseOption = 1;
                                          });
                                        }, 
                                        icon: Icon(glucoseOption == 1? Icons.radio_button_checked_outlined :Icons.radio_button_off,size: 22,color: glucoseOption == 1?Colorconstands.primaryColor:Colorconstands.black,)
                                      ),
                                      Expanded(child: Text("Fasting",style: ThemeConstands.headline6_SemiBold_14.copyWith(color:glucoseOption == 1?Colorconstands.primaryColor:Colorconstands.black),textAlign: TextAlign.left,)),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: (){
                                          setState(() {
                                            glucoseOption = 2;
                                          });
                                        }, 
                                        icon: Icon(glucoseOption == 2? Icons.radio_button_checked_outlined :Icons.radio_button_off,size: 22,color: glucoseOption == 2?Colorconstands.primaryColor:Colorconstands.black,)
                                      ),
                                      Expanded(child: Text("Affter meal",style: ThemeConstands.headline6_SemiBold_14.copyWith(color:glucoseOption == 2?Colorconstands.primaryColor:Colorconstands.black),textAlign: TextAlign.left,)),
                                    ],
                                  ),
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: (){
                                          setState(() {
                                            glucoseOption = 3;
                                          });
                                        }, 
                                        icon: Icon(glucoseOption == 3? Icons.radio_button_checked_outlined :Icons.radio_button_off,size: 22,color: glucoseOption == 3?Colorconstands.primaryColor:Colorconstands.black,)
                                      ),
                                      Expanded(child: Text("2-3 hrs affter meal",style: ThemeConstands.headline6_SemiBold_14.copyWith(color:glucoseOption == 3?Colorconstands.primaryColor:Colorconstands.black),textAlign: TextAlign.left,)),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            ListTile(
                              title: Text("History of illness",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: historyOfIllnessController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type History of illness here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 18,),
                      Container(
                        padding:const EdgeInsets.symmetric(vertical: 6),
                        decoration: BoxDecoration(
                          color: Colorconstands.neutralWhite,
                          borderRadius: BorderRadius.circular(14),
                        ),
                        child: Column(
                          children: [
                            const SizedBox(height: 8,),
                            Text("BMI",style: ThemeConstands.headline2_SemiBold_24.copyWith(color: Colorconstands.black),),
                            ListTile(
                              title: Text("Height (150 cm)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: heightController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Height (m . 1.50) here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            ListTile(
                              title: Text("Weight (Kg)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                              subtitle: Container(
                                margin:const EdgeInsets.only(top: 8),
                                child: TextFormField(
                                  keyboardType: TextInputType.datetime,
                                  controller: weightController,
                                  style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                  decoration:InputDecoration(
                                    border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                    contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                    hintText: 'Type Weight (Kg) here',
                                    hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                    focusedBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                    ),
                                    enabledBorder:const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(14)),
                                      borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(height: 8,),
                            Container(
                              margin:const EdgeInsets.only(left: 18),
                              alignment: Alignment.centerLeft,
                              child: Text("Gender",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),)),
                            Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: (){
                                          setState(() {
                                            sexMale = !sexMale;
                                            sex = 1;
                                          });
                                        }, 
                                        icon: Icon(sexMale == true? Icons.radio_button_checked_outlined :Icons.radio_button_off,size: 22,color: sexMale == true?Colorconstands.primaryColor:Colorconstands.black,)
                                      ),
                                      Text("Male",style: ThemeConstands.headline4_SemiBold_18.copyWith(color:sexMale == true?Colorconstands.primaryColor:Colorconstands.black),),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: (){
                                          setState(() {
                                            sexMale = !sexMale;
                                            sex = 2;
                                          });
                                        }, 
                                        icon: Icon(sexMale == false? Icons.radio_button_checked_outlined :Icons.radio_button_off,size: 22,color: sexMale == false?Colorconstands.primaryColor:Colorconstands.black,)
                                      ),
                                      Text("Female",style: ThemeConstands.headline4_SemiBold_18.copyWith(color:sexMale == false?Colorconstands.primaryColor:Colorconstands.black),),
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        )
                      ),
                      Container(
                        margin:const EdgeInsets.symmetric(horizontal: 0,vertical: 18),
                        child: loading == true? 
                         Container(height: 35,child: const Center(child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),))
                        :ButtonWidgetCustom(
                          panddingVerButton: 3, 
                          buttonColor: Colorconstands.neutralWhite,
                          onTap: widget.patient == true?(){
                            Navigator.push(context,PageRouteBuilder(
                                  pageBuilder: (context, animation1, animation2) => ResultCalculateVitalSingScreem(
                                    weight: weightController.text,
                                    height: heightController.text == ""?"":"${double.parse(heightController.text)/100}",
                                    age: ageController.text,
                                    gender: sex,
                                    systolic: systolicController.text,
                                    diastolic: diastolicController.text,
                                    glucose: glucoseController.text,
                                    glucoseOption: glucoseOption,
                                    temperature: temperatureController.text,
                                    spo2: spo2Controller.text,
                                    respiratory: respiratoryController.text,
                                    pulse: pulseController.text,
                                    chiefComplaint: chiefComplaintController.text,
                                    currentMedcal: currentMedicationController.text,
                                    patient: widget.patient,
                                  ),
                                  transitionDuration: Duration.zero,
                                  reverseTransitionDuration: Duration.zero,
                                ),
                              );
                          } 
                          :() {
                            setState(() {
                              loading = true;
                            });
                            GetVitalSignDataPatientApi().postPatientVitalSignDataApi(
                              chiefComplaint: chiefComplaintController.text,
                              currentMedication: currentMedicationController.text,
                              systolic: systolicController.text,
                              diastolic: diastolicController.text,
                              respiratoryRate: respiratoryController.text,
                              spo2: spo2Controller.text,
                              pulse: pulseController.text,
                              temperature: temperatureController.text,
                              glucose: glucoseController.text,
                              height: heightController.text,
                              weight: weightController.text,
                              isCompleted: "1",
                              idPatientVisit: widget.patientIdVisit,
                              historyOfIllness: historyOfIllnessController.text,
                              objectKey: "VITAL_SIGN",
                            ).then((onValue){
                              setState(() {
                                  loading = false;
                                });
                              if(onValue == true){
                                showSnackBar(context,"Sucess",Colorconstands.lightPrimaryPiccolo,const Icon(Icons.error_outline,color: Colorconstands.neutralWhite,));
                                Navigator.pushReplacement(context,PageRouteBuilder(
                                  pageBuilder: (context, animation1, animation2) => ResultCalculateVitalSingScreem(
                                    weight: weightController.text,
                                    height: heightController.text == ""?"":"${double.parse(heightController.text)/100}",
                                    age: ageController.text,
                                    gender: sex,
                                    systolic: systolicController.text,
                                    diastolic: diastolicController.text,
                                    glucose: glucoseController.text,
                                    glucoseOption: glucoseOption,
                                    temperature: temperatureController.text,
                                    spo2: spo2Controller.text,
                                    respiratory: respiratoryController.text,
                                    pulse: pulseController.text,
                                    chiefComplaint: chiefComplaintController.text,
                                    currentMedcal: currentMedicationController.text,
                                    patient: widget.patient,
                                  ),
                                  transitionDuration: Duration.zero,
                                  reverseTransitionDuration: Duration.zero,
                                ),
                              );
                              }else{
                                showSnackBar(context,"Fail!",Colorconstands.errorColor,const Icon(Icons.error_outline,color: Colorconstands.exam,));
                              }
                            });
                          },
                          panddinHorButton: 28,
                          radiusButton: 14,
                          heightButton: 50,
                          textStyleButton: ThemeConstands.headline3_SemiBold_20.copyWith(color:Colorconstands.primaryColor),
                          title: 'FINISH'.tr(),
                        ),
                      ),
                    ]),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}