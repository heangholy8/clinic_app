import 'dart:async';

import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/widget/grap_spo2_widget.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/widget/graph_horizontal_vital_sign_widget.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/widget/graph_temperature_widget.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:flutter/material.dart';

class ResultCalculateVitalSingScreem extends StatefulWidget {
  final String weight;
  final String age;
  final String height;
  final String systolic;
  final String diastolic;
  final String respiratory;
  final String spo2;
  final String glucose;
  final String pulse;
  final String temperature;
  final String chiefComplaint;
  final String currentMedcal;
  final int glucoseOption;
  final int gender;//  1 male  2 female
  final bool patient;//  1 male  2 female
  const ResultCalculateVitalSingScreem({super.key,required this.patient,required this.chiefComplaint,required this.currentMedcal,required this.pulse,required this.respiratory,required this.spo2,required this.temperature,required this.diastolic,required this.glucose,required this.glucoseOption,required this.systolic,required this.weight,required this.age,required this.height,required this.gender});

  @override
  State<ResultCalculateVitalSingScreem> createState() => _ResultCalculateVitalSingScreemState();
}

class _ResultCalculateVitalSingScreemState extends State<ResultCalculateVitalSingScreem> {
  
  double scanScrool = 0;
  late Timer scanControl;
  double bmiCalculate = 0.0;
  bool animate = false;
  double? weightContainer;
  String weightUnder = "Being underweight indicates that you are not eating properly or that you are afflicted with sickness.";
  String weightNomal = "Your weight is normal and maintain the same healthy weight by following the best diet.";
  String weightOver = "If you're overweight, stick to a healthy diet and do regular exercise.";
  String weightObese = "Obesity and overweight are both classified as abnormal or excessive fat buildup that poses a health concern.";
  String weightExObese = "Your weight is very much higher than the normal recommended weight and is a risk factor for many other diseases like diabetes and heart diseases. Bring down it with more exercises, correct dietary practices and medical advices.";
  @override
  void initState() {
    setState(() {
      if(widget.weight !="" && widget.height !=""){
        bmiCalculate = (double.parse(widget.weight) / (double.parse(widget.height) * double.parse(widget.height)));
        bmiCalculate = double.parse(bmiCalculate.toStringAsFixed(1));
        scanControl = Timer.periodic(const Duration(milliseconds: 15), (Timer t) {
          if(scanScrool < 300){
            setState(() {
              scanScrool = scanScrool+2;
            });
          }
          else{
            setState(() {
              scanScrool = 0;
            });
          }
        });
      }
      Future.delayed(const Duration(milliseconds: 200,),(){
          setState(() {
            animate = true;
          });
        });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              HeaderFeature(titleHeader: "Vital Sign Result"),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    margin: const EdgeInsets.all(28),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      widget.patient == true? Container():Column(
                        children: [
                          widget.chiefComplaint == ""?Container():Container(
                            margin:const EdgeInsets.only(bottom: 18),
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(22),
                              color: Colorconstands.neutralWhite
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("Chief Complaint",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.left,),
                                Container(
                                  margin:const EdgeInsets.only(top: 8),
                                  child: Text(widget.chiefComplaint,style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                ),
                              ]
                            )
                          ),
                          widget.currentMedcal == ""?Container():Container(
                            margin:const EdgeInsets.only(bottom: 18),
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(22),
                              color: Colorconstands.neutralWhite
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("Current Medication",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.left,),
                                Container(
                                  margin:const EdgeInsets.only(top: 8),
                                  child: Text(widget.currentMedcal,style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                ),
                              ]
                            )
                          ),
                        ],
                      ),
                      widget.systolic == ""?Container():Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: GraphHorizontalVitalsignWidget(
                          animate: animate,
                          titalGraph: "Systolic (${widget.systolic} mmHg)",
                          grapgNum1: "0",
                          grapgNum2: "30",
                          grapgNum3: "60",
                          grapgNum4: "90",
                          grapgNum5: "120",
                          grapgNum6: "150",
                          grapgNum7: "180",
                          grapgNum8: "210",
                          maginContainerHoriZontal: 28,
                          valueGraph: double.parse(widget.systolic),
                        ),
                      ),
                      widget.diastolic == ""?Container():Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: GraphHorizontalVitalsignWidget(
                          animate: animate,
                          titalGraph: "Diastolic (${widget.diastolic} mmHg)",
                          grapgNum1: "0",
                          grapgNum2: "20",
                          grapgNum3: "40",
                          grapgNum4: "60",
                          grapgNum5: "80",
                          grapgNum6: "100",
                          grapgNum7: "120",
                          grapgNum8: "140",
                          maginContainerHoriZontal: 28,
                          valueGraph: double.parse(widget.diastolic),
                        ),
                      ),
                       widget.respiratory == ""?Container():Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: GraphHorizontalVitalsignWidget(
                          animate: animate,
                          titalGraph: "Respiratory (${widget.respiratory} mn)",
                          grapgNum1: "0",
                          grapgNum2: "4",
                          grapgNum3: "8",
                          grapgNum4: "12",
                          grapgNum5: "16",
                          grapgNum6: "20",
                          grapgNum7: "24",
                          grapgNum8: "28",
                          maginContainerHoriZontal: 28,
                          valueGraph: double.parse(widget.respiratory),
                        ),
                      ),
                       widget.spo2 == ""?Container(): Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: GrapSpo2Widget(
                          animate: animate,
                          titalGraph: "Spo2 (${widget.spo2} %Ra)",
                          grapgNum1: "65",
                          grapgNum2: "70",
                          grapgNum3: "75",
                          grapgNum4: "80",
                          grapgNum5: "85",
                          grapgNum6: "90",
                          grapgNum7: "95",
                          grapgNum8: "100",
                          maginContainerHoriZontal: 28,
                          valueGraph: double.parse(widget.spo2),
                        ),
                      ),
                       widget.pulse == ""?Container():Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: GraphHorizontalVitalsignWidget(
                          animate: animate,
                          titalGraph: "Pulse (${widget.pulse} mn)",
                          grapgNum1: "0",
                          grapgNum2: "20",
                          grapgNum3: "40",
                          grapgNum4: "60",
                          grapgNum5: "80",
                          grapgNum6: "100",
                          grapgNum7: "120",
                          grapgNum8: "140",
                          maginContainerHoriZontal: 28,
                          valueGraph: double.parse(widget.pulse),
                        ),
                      ),
                       widget.temperature == ""?Container():Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: GraphHorizontalTemperatureWidget(
                          animate: animate,
                          titalGraph: "Temperature (${widget.temperature} C)",
                          grapgNum1: "30",
                          grapgNum2: "33.5",
                          grapgNum3: "37",
                          grapgNum4: "40.5",
                          grapgNum5: "44",
                          maginContainerHoriZontal: 28,
                          valueGraph: double.parse(widget.temperature),
                        ),
                      ),
          //===============  Condition Glucose =======================
                      widget.glucose == ""?Container():
                      Container(
                        margin:const EdgeInsets.only(bottom: 18),
                        child: GraphHorizontalVitalsignWidget(
                          animate: animate,
                          titalGraph: "Glucose (${widget.glucose} mg/dL)",
                          grapgNum1:"0",
                          grapgNum2: widget.glucoseOption==1? "25":widget.glucoseOption==2?"45":"33",
                          grapgNum3: widget.glucoseOption==1? "50":widget.glucoseOption==2?"90":"66",
                          grapgNum4: widget.glucoseOption==1? "75":widget.glucoseOption==2?"135":"99",
                          grapgNum5: widget.glucoseOption==1? "100":widget.glucoseOption==2?"180":"132",
                          grapgNum6: widget.glucoseOption==1? "125":widget.glucoseOption==2?"225":"165",
                          grapgNum7: widget.glucoseOption==1? "150":widget.glucoseOption==2?"270":"198",
                          grapgNum8: widget.glucoseOption==1? "175":widget.glucoseOption==2?"315":"221",
                          maginContainerHoriZontal: 28,
                          valueGraph: double.parse(widget.glucose),
                        ),
                      ),
         //===============  End Condition Glucose =======================
                      widget.weight =="" && widget.height ==""?Container():Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(22),
                          color: Colorconstands.neutralWhite
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text("BMI Calculate",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.left,),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin:const EdgeInsets.symmetric(vertical: 6),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Height :",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightTrunks),),
                                            Text("${double.parse(widget.height)*100} cm",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin:const EdgeInsets.symmetric(vertical: 6),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Weight :",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightTrunks),),
                                            Text("${widget.weight} Kg",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin:const EdgeInsets.symmetric(vertical: 6),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Age :",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightTrunks),),
                                            Text(widget.age,style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin:const EdgeInsets.symmetric(vertical: 6),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Sex :",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightTrunks),),
                                            Text(widget.gender == 1? "Male":"Female",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 300,
                                  child: Stack(
                                    children: [
                                      Image.asset(
                                    //========= Male ===================
                                        widget.gender == 1?
                                        bmiCalculate <18.5?
                                        ImageAssets.menUnderweight
                                        :bmiCalculate >= 18.5 && bmiCalculate <= 24.9?
                                        ImageAssets.menNomal
                                        :bmiCalculate >= 25 && bmiCalculate <= 29.9?
                                        ImageAssets.menRiskTOOverweight
                                        :bmiCalculate >=30 && bmiCalculate <= 34.9?
                                        ImageAssets.menOvereweight
                                        : ImageAssets.menObese
                                    //========= Female ===================
                                      :bmiCalculate <18.5?
                                        ImageAssets.girlUnderweight
                                        :bmiCalculate >= 18.5 && bmiCalculate <= 24.9?
                                        ImageAssets.girlNomal
                                        :bmiCalculate >= 25 && bmiCalculate <= 29.9?
                                        ImageAssets.girlRiskTOOverweight
                                        :bmiCalculate >=30 && bmiCalculate <= 34.9?
                                        ImageAssets.girlOvereweight
                                        : ImageAssets.girlObese
                                        ,height: 300,fit: BoxFit.cover,),
                                      AnimatedPositioned(
                                        left: 0,right: 0,top: scanScrool,
                                        duration:const Duration(milliseconds:10),
                                        child: Container(
                                          height: 3,
                                          decoration: BoxDecoration(
                                            color: Colorconstands.primaryColor,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colorconstands.primaryColor.withOpacity(0.5),
                                                spreadRadius: 10,
                                                blurRadius: 25,
                                                offset:const Offset(0, 0), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                        ), 
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(child: Container()),
                              ],
                            ),
                            Container(
                              alignment: Alignment.center,
                              height: 45,
                              width: 250,
                              color: 
                              bmiCalculate <18.5?
                              const Color(0xFF59B7E7)
                              :bmiCalculate >= 18.5 && bmiCalculate <= 24.9?
                              const Color(0xCB33BE36)
                              :bmiCalculate >= 25 && bmiCalculate <= 29.9?
                              const Color(0xFFEDDE16)                         
                              :bmiCalculate >=30 && bmiCalculate <= 34.9?
                              const Color(0xFFE67E16)
                              : const Color(0xFFF24141),
                              child: Text(
                                bmiCalculate <18.5?
                                "Under weight"
                                :bmiCalculate >= 18.5 && bmiCalculate <= 24.9?
                                "Normal"
                                :bmiCalculate >= 25 && bmiCalculate <= 29.9?
                                "Over weight"                   
                                :bmiCalculate >=30 && bmiCalculate <= 34.9?
                                "Obese"
                                : "Extramly obese "
                              ,style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),),
                            ),
                            const SizedBox(height: 12,),
                            Text(
                              bmiCalculate <18.5?
                                 weightUnder
                                :bmiCalculate >= 18.5 && bmiCalculate <= 24.9?
                                weightNomal
                                :bmiCalculate >= 25 && bmiCalculate <= 29.9?
                                weightOver                 
                                :bmiCalculate >=30 && bmiCalculate <= 34.9?
                                weightObese
                                : weightExObese
                            ,style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey,),textAlign: TextAlign.center,)
                          ],
                        ),
                      )
                    ]),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}