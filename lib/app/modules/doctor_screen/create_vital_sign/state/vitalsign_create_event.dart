part of 'vitalsign_create_bloc.dart';

sealed class VitalsignCreateEvent extends Equatable {
  const VitalsignCreateEvent();

  @override
  List<Object> get props => [];
}

class GetVitalSignDataPaitient extends VitalsignCreateEvent{
  final String idpatientVisit;
  const GetVitalSignDataPaitient({required this.idpatientVisit});
}
