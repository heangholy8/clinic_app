part of 'vitalsign_create_bloc.dart';

sealed class VitalsignCreateState extends Equatable {
  const VitalsignCreateState();
  
  @override
  List<Object> get props => [];
}

final class VitalsignCreateInitial extends VitalsignCreateState {}
final class VitalsignCreateLoading extends VitalsignCreateState {}

final class VitalsignCreateLoaded extends VitalsignCreateState {
  final VitalsignDataModel vitalsignDataModel;
  const VitalsignCreateLoaded({required this.vitalsignDataModel});
}
final class VitalsignDataLoaded extends VitalsignCreateState {
  final VitalsignDataModel vitalsignDataModel;
  const VitalsignDataLoaded({required this.vitalsignDataModel});
}
final class VitalsignCreateError extends VitalsignCreateState {}
