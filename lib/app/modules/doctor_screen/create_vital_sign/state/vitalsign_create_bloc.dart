import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/doctor_model/vital_sign_data_model/vital_sign_data_model.dart';
import 'package:clinic_application/service/apis/doctor_api/vital_sign_data_api/vital_sign_data_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'vitalsign_create_event.dart';
part 'vitalsign_create_state.dart';

class VitalsignCreateBloc extends Bloc<VitalsignCreateEvent, VitalsignCreateState> {
  final GetVitalSignDataPatientApi getVitalSignDataPatientApi;
  VitalsignCreateBloc({required this.getVitalSignDataPatientApi}) : super(VitalsignCreateInitial()) {
    on<GetVitalSignDataPaitient>((event, emit) async {
        emit(VitalsignCreateLoading());
        try{
            var data = await getVitalSignDataPatientApi.getPatientVitalSignDataApi(idPatientVisit: event.idpatientVisit);
            emit(VitalsignDataLoaded(vitalsignDataModel: data));
        }catch(e){
          debugPrint("safasfsadf$e");
          emit(VitalsignCreateError());
        }
    });

    on<VitalsignCreateEvent>((event, emit) async {
        emit(VitalsignCreateLoading());
        try{
            // var data = await getVitalSignDataPatientApi.getPatientVitalSignDataApi(idPatientVisit: event.idpatientVisit);
            // emit(VitalsignDataLoaded(vitalsignDataModel: data));
        }catch(e){
          emit(VitalsignCreateError());
        }
    });
  }
}
