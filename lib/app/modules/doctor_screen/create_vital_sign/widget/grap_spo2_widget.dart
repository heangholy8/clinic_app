import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:flutter/material.dart';

class GrapSpo2Widget extends StatefulWidget {
  final String grapgNum1;
  final String grapgNum2;
  final String grapgNum3;
  final String grapgNum4;
  final String grapgNum5;
  final String grapgNum6;
  final String grapgNum7;
  final String grapgNum8;
  final String titalGraph;
  final bool  animate;
  final double maginContainerHoriZontal;
  final double valueGraph;
  const GrapSpo2Widget({super.key,
  required this.animate,
  required this.grapgNum1,
  required this.grapgNum2,
  required this.grapgNum3,
  required this.grapgNum4,
  required this.grapgNum5,
  required this.grapgNum6,
  required this.grapgNum7,
  required this.grapgNum8,
  required this.titalGraph,
  required this.maginContainerHoriZontal,
  required this.valueGraph,
  });

  @override
  State<GrapSpo2Widget> createState() => _GrapSpo2WidgetState();
}

class _GrapSpo2WidgetState extends State<GrapSpo2Widget> {
  double? valueGraphCutGraphNum1 ;
  @override
  Widget build(BuildContext context) {
    valueGraphCutGraphNum1 = widget.valueGraph-double.parse(widget.grapgNum1);
    double graphWeight = (MediaQuery.of(context).size.width - ((widget.maginContainerHoriZontal*2)+36)).toDouble();
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(22),
        color: Colorconstands.neutralWhite
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(widget.titalGraph,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.left,),
          Container(
            alignment: Alignment.center,
            margin:const EdgeInsets.only(top: 12),
            height: 30,
            width: MediaQuery.of(context).size.width - 92,
            child: Stack(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        decoration:const BoxDecoration(
                          color:  Color(0xFF3F667A)
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration:const BoxDecoration(
                          color:  Color(0xFF3F667A)
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration:const BoxDecoration(
                          color:  Color(0xFF3F667A)
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration:const BoxDecoration(
                          color: Color(0xFFED2A2A),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration:const BoxDecoration(
                          color: Color(0xFFED2A2A),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration:const BoxDecoration(
                          color: Color(0xFFED2A2A),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        decoration:const BoxDecoration(
                          color: Color(0xCB33BE36)
                        ),
                      ),
                    ),
                  ],
                ),
                AnimatedPositioned(
                  duration:const Duration(milliseconds: 1000),
                  right: 0,
                  left: (widget.animate == false?0:(valueGraphCutGraphNum1! / 
                  (valueGraphCutGraphNum1! > double.parse(widget.grapgNum8)?
                  valueGraphCutGraphNum1!
                  :double.parse(widget.grapgNum8)-double.parse(widget.grapgNum1))) * (graphWeight)),
                  child: Container(
                    width: 2,
                    alignment: Alignment.centerRight,
                    height:30,
                    decoration:const BoxDecoration(
                      color: Colorconstands.neutralWhite,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 5),
            child: Row(
              children: [
                Container(
                  height: 6,
                  width: 2,
                  color: Colorconstands.darkGray,
                ),
                Expanded(
                  child: Container(
                    height: 1.5,
                    color: Colorconstands.darkGray,
                  ),
                ),
                Container(
                  height: 6,
                  width: 2,
                  color: Colorconstands.darkGray,
                ),
                Expanded(
                  child: Container(
                    height: 1.5,
                    color: Colorconstands.darkGray,
                  ),
                ),
                Container(
                  height: 6,
                  width: 2,
                  color: Colorconstands.darkGray,
                ),
                Expanded(
                  child: Container(
                    height: 1.5,
                    color: Colorconstands.darkGray,
                  ),
                ),
                Container(
                  height: 6,
                  width: 2,
                  color: Colorconstands.darkGray,
                ),
                Expanded(
                  child: Container(
                    height: 1.5,
                    color: Colorconstands.darkGray,
                  ),
                ),
                Container(
                  height: 6,
                  width: 2,
                  color: Colorconstands.darkGray,
                ),
                Expanded(
                  child: Container(
                    height: 1.5,
                    color: Colorconstands.darkGray,
                  ),
                ),
                Container(
                  height: 6,
                  width: 2,
                  color: Colorconstands.darkGray,
                ),
                Expanded(
                  child: Container(
                    height: 1.5,
                    color: Colorconstands.darkGray,
                  ),
                ),
                Container(
                  height: 6,
                  width: 2,
                  color: Colorconstands.darkGray,
                ),
                Expanded(
                  child: Container(
                    height: 1.5,
                    color: Colorconstands.darkGray,
                  ),
                ),
                Container(
                  height: 6,
                  width: 2,
                  color: const Color(0xCB33BE36)
                ),
                
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 5),
            child: Row(
              children: [
                Text(widget.grapgNum1,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
                const Expanded(
                  child: SizedBox(),
                ),
                Text(widget.grapgNum2,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
                const Expanded(
                  child: SizedBox(),
                ),
                Text(widget.grapgNum3,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
                const Expanded(
                  child: SizedBox(),
                ),
                Text(widget.grapgNum4,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
                const Expanded(
                  child: SizedBox(),
                ),
                Text(widget.grapgNum5,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
                const Expanded(
                  child: SizedBox(),
                ),
                Text(widget.grapgNum6,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
                const Expanded(
                  child: SizedBox(),
                ),
                Text(widget.grapgNum7,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
                const Expanded(
                  child: SizedBox(),
                ),
                Text(widget.valueGraph > double.parse(widget.grapgNum8)?"${widget.valueGraph}":widget.grapgNum8,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.black),),
              ],
            ),
          )
        ],
      ),
    );
  }
}