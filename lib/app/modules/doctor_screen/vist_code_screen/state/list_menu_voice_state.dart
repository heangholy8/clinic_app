part of 'list_menu_voice_bloc.dart';

class ListMenuVoiceState extends Equatable {
  const ListMenuVoiceState();
  
  @override
  List<Object> get props => [];
}
class ListMenuVoiceInitial extends ListMenuVoiceState {}

class ListMenuVoiceLoading extends ListMenuVoiceState {}
class ListMenuVoiceLoaded extends ListMenuVoiceState {
  ListMenuVoiceModel listMenuVoiceModel;
  ListMenuVoiceLoaded({required this.listMenuVoiceModel});
}
class ListMenuVoiceError extends ListMenuVoiceState {}
