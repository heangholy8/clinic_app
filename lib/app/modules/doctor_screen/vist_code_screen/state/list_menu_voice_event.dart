part of 'list_menu_voice_bloc.dart';

class ListMenuVoiceEvent extends Equatable {
  const ListMenuVoiceEvent();

  @override
  List<Object> get props => [];
}

class PostCodeGetListMenuVoice extends ListMenuVoiceEvent{
  String? code;
  PostCodeGetListMenuVoice({required this.code});
}
