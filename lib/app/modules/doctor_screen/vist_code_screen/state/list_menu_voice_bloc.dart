import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/doctor_model/list_menu_voice_model/list_menu_voice_model.dart';
import 'package:clinic_application/service/apis/doctor_api/get_list_menu_voice_api/get_list_menu_voice_api.dart';
import 'package:equatable/equatable.dart';
part 'list_menu_voice_event.dart';
part 'list_menu_voice_state.dart';

class ListMenuVoiceBloc extends Bloc<ListMenuVoiceEvent, ListMenuVoiceState> {
  final PostCodeVisitGetListMenuVoiceApi postCodeVisitGetListMenuVoiceApi;
  ListMenuVoiceBloc({required this.postCodeVisitGetListMenuVoiceApi}) : super(ListMenuVoiceInitial()) {
    on<PostCodeGetListMenuVoice>((event, emit) async{
      emit(ListMenuVoiceLoading());
      try {
        var data = await postCodeVisitGetListMenuVoiceApi.postCodeVisitGetListMenuVoiceApi(
          code: event.code
        );
        emit(ListMenuVoiceLoaded(listMenuVoiceModel: data));
      } catch (e) {
        print(e);
        emit(ListMenuVoiceError());
      }
    });
  }
}
