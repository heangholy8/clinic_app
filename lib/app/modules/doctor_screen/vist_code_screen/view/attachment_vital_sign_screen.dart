import 'dart:io';

import 'package:audio_waveforms/audio_waveforms.dart';
import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/vist_code_screen/state/list_menu_voice_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/icon_back.dart';
import 'package:clinic_application/model/doctor_model/list_menu_voice_model/list_menu_voice_model.dart';
import 'package:clinic_application/service/apis/doctor_api/delect_voice/delect_voice_api.dart';
import 'package:clinic_application/service/apis/doctor_api/post_voice/post_voice_api.dart';
import 'package:clinic_application/widgets/button_widget/button_widget_custom.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:clinic_application/widgets/imageView/view_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image_picker/image_picker.dart';

class AttachmentVitalSingScreen extends StatefulWidget {
  Child dataAttach;
  String code;
  String objectId;
  String nameAttach;
  String nameAttachGroup;
  AttachmentVitalSingScreen({super.key,required this.nameAttachGroup,required this.nameAttach,required this.dataAttach,required this.objectId,required this.code});

  @override
  State<AttachmentVitalSingScreen> createState() => _AttachmentVitalSingScreenState();
}

class _AttachmentVitalSingScreenState extends State<AttachmentVitalSingScreen> {


  List<File> listMultiMedia = [];
  List<File> listMultiAttachement = [];
  final ImagePicker _picker = ImagePicker();
  bool loadingsumit = false;
  bool loadingDeleteVoice = false;
  bool loadingDeleteImage = false;
  PlayerController playerController = PlayerController();
  String playDuration = "0";
  String? path;
  String voicePath = "";
  List<double> waveformData = [];
  bool isplay = false;
  late Directory appDirectory;
  late final RecorderController recorderController;
  bool isRecording = false;
  bool isRecordingCompleted = false;
  bool isLoadingRecord = true;
  String durationRecord = "00:00";
  File? voice;

  // =================  attach from api ==========================
  List listImage = [];
  List listVoice = [];
  String playDurationNet = "0";
  bool isplayNet = false;
  PlayerController playerControllerNework = PlayerController();

  Future<void> _getVoiebyLink(String url) async {
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';
    try {
      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      var response = await request.close();
      if(response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = path!;
        file = File(filePath);
        voicePath = filePath;
        await file.writeAsBytes(bytes);
      }
      else
        filePath = 'Error code: '+response.statusCode.toString();
    }
    catch(ex){
      filePath = 'Can not fetch url';
    }
    Future.delayed(Duration.zero,() async {
      waveformData = await playerControllerNework.extractWaveformData(
      path: filePath,
      noOfSamples: 100,);
      await playerControllerNework.preparePlayer(
      path: filePath,
      shouldExtractWaveform: true,
      noOfSamples: 100,
      volume: 10.0);
      setState((){
        playDurationNet = (playerControllerNework.maxDuration~/1000).toInt().toMMSS();
      });
    },);
  }

  void _getDir() async {
    appDirectory = await getApplicationDocumentsDirectory();
    path = "${appDirectory.path}/recording.m4a";
    setState(() {});
  }

  Future<void> _getVoie(String path) async {
    waveformData = await playerController.extractWaveformData(
    path: path,
    noOfSamples: 100,);
    await playerController.preparePlayer(
    path: path,
    shouldExtractWaveform: true,
    noOfSamples: 100,
    volume: 10.0);
    setState((){
      playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
    });
  }

  void _startOrStopRecording(List<double> waveformData) async {
    try {
      if (isRecording) {
        recorderController.reset();
        final path = await recorderController.stop(false);
        if (path != null) {
          isRecordingCompleted = true;
          debugPrint(path);
          debugPrint("Recorded file size: ${File(path).lengthSync()}");
          Future.delayed(Duration.zero,() {
            _getVoie(path);
          },);
          voice = File(path);
        }
      } else {
        await recorderController.record(path: path!);
        recorderController.onCurrentDuration.listen((event) {
          setState(() {
            durationRecord = event.toHHMMSS().substring(3);
          });
        },);
      }
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      setState(() {
        isRecording = !isRecording;
      });
    }
  }

  @override
  void initState() {
    setState(() {
      if( widget.dataAttach.data!.isNotEmpty){
        setState(() {
          listImage = widget.dataAttach.data!.where((element) {return element.fileType != ".m4a";}).toList();
          listVoice = widget.dataAttach.data!.where((element) {return element.fileType!.contains(".m4a");}).toList();
          Future.delayed(const Duration(milliseconds: 100),(){
            _getVoiebyLink(listVoice[0].fileDisplay.toString());
          });
        });
      }
      _getDir();
      _initialiseControllers();
    });
    super.initState();
  }

  void _initialiseControllers() {
    recorderController = RecorderController()
      ..androidEncoder = AndroidEncoder.aac
      ..androidOutputFormat = AndroidOutputFormat.mpeg4
      ..iosEncoder = IosEncoder.kAudioFormatMPEG4AAC
      ..sampleRate = 44100;
  }

  @override
  void dispose() {
    playerController.dispose();
    playerControllerNework.dispose();
    recorderController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    // final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
             Container(
                margin:const EdgeInsets.symmetric(horizontal: 28),
                child: Row(
                  children: [
                    Container(
                      width: 30,
                      child: IconBack(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        hightButton: 30,
                        radiusButton: 6,
                        
                      ),
                    ),
                    const SizedBox(width: 16,),
                    Expanded(
                      child: Column(
                        children: [
                          Text(widget.nameAttach,style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,),
                          Text(widget.nameAttachGroup,style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,),
                        ],
                      ),
                    ),
                    Container(
                      width: 30,
                    )
                  ],
                ),
              ),
              Expanded(
                child: voice == null && listVoice.isEmpty && listImage.isEmpty && listMultiMedia.isEmpty && isRecording == false? Center(
                  child: EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Attachment", subtitle: "")
                ): SingleChildScrollView(
                  child: Container(
                    margin: const EdgeInsets.all(28),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center, 
                      children: [
                        listMultiMedia.isNotEmpty || listImage.isNotEmpty?Container(
                          margin:const EdgeInsets.only(bottom: 18,top: 18),
                          height: 90,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  physics:const NeverScrollableScrollPhysics(),
                                  padding: const EdgeInsets.all(0),
                                  itemCount: listMultiMedia.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                      width: 105,height: 105,
                                      margin: const EdgeInsets.only(right:0),
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: GestureDetector(
                                              onTap: (){
                                                Navigator.push(context,
                                                  PageRouteBuilder(
                                                    pageBuilder: (_, __, ___) =>ImageViewDownloads(listimagevide:listMultiMedia,activepage: index,listimageNet: [], ),
                                                    transitionDuration:const Duration(seconds: 0),
                                                  ),
                                                );
                                              },
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(20), // Image border
                                                child: SizedBox.fromSize(
                                                  size: const Size.fromRadius(40), // Image radius
                                                  child: Image.file(listMultiMedia[index], fit: BoxFit.cover),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            top: 3,
                                            right: 3,
                                            child: CircleAvatar(
                                              radius: 14,
                                              backgroundColor: Colorconstands.errorColor,
                                              child: IconButton(
                                                padding: const EdgeInsets.all(0),
                                                onPressed: (){
                                                  setState(() {
                                                    listMultiMedia.removeAt(index);
                                                  });
                                                }, 
                                                icon: const Icon(Icons.delete_outline_outlined,color: Colorconstands.neutralWhite,size: 16,)
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    );
                                  },
                                ),
                                listImage.isNotEmpty?ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  shrinkWrap: true,
                                  physics:const NeverScrollableScrollPhysics(),
                                  padding: const EdgeInsets.all(0),
                                  itemCount: listImage.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                      width: 105,height: 105,
                                      margin: const EdgeInsets.only(right:0),
                                      child: Stack(
                                        children: [
                                          Center(
                                            child: GestureDetector(
                                              onTap: (){
                                                Navigator.push(context,
                                                  PageRouteBuilder(
                                                    pageBuilder: (_, __, ___) =>ImageViewDownloads(listimagevide:listMultiMedia,activepage: index,listimageNet: listImage,),
                                                    transitionDuration:const Duration(seconds: 0),
                                                  ),
                                                );
                                              },
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(20), // Image border
                                                child: SizedBox.fromSize(
                                                  size: const Size.fromRadius(40), // Image radius
                                                  child: Image.network(listImage[index].fileDisplay.toString(), fit: BoxFit.cover),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            top: 3,
                                            right: 3,
                                            child: CircleAvatar(
                                              radius: 14,
                                              backgroundColor: Colorconstands.errorColor,
                                              child: IconButton(
                                                padding: const EdgeInsets.all(0),
                                                onPressed: (){
                                                  DeleteVoiceApi().deleteVoiceApi(id: listImage[index].id.toString()).then((onValue){
                                                    if(onValue == true){
                                                      BlocProvider.of<ListMenuVoiceBloc>(context).add(PostCodeGetListMenuVoice(code: widget.code.toUpperCase()));
                                                      setState(() {
                                                        isplayNet = false;
                                                        isplay = false;
                                                        playerController.stopPlayer();
                                                        playerControllerNework.stopPlayer();
                                                      },);
                                                    }
                                                  });
                                                  listImage.removeAt(index);
                                                }, 
                                                icon: const Icon(Icons.delete_outline_outlined,color: Colorconstands.neutralWhite,size: 16,)
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    );
                                  },
                                ):Container(),
                              ],
                            ),
                          ),
                        ):Container(),
                        listMultiMedia.isNotEmpty || listImage.isNotEmpty ?const Divider():Container(),
                        SizedBox(height: listMultiMedia.isNotEmpty || listImage.isNotEmpty? 12:0,),
                        isRecording == true?Container(
                            decoration: BoxDecoration(
                                color: Colorconstands.neutralWhite,
                                borderRadius: BorderRadius.circular(25)
                              ),
                              height: 50,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 18),
                              child: Row(
                                children: [
                                  Text(durationRecord,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.errorColor),),
                                  const SizedBox(width: 8,),
                                  Expanded(
                                    child: AudioWaveforms(
                                      enableGesture: true,
                                      size: Size( MediaQuery.of(context).size.width/1.5,50),
                                      recorderController: recorderController,
                                      waveStyle:  const WaveStyle(
                                        waveColor: Colorconstands.errorColor,
                                        extendWaveform: true,
                                        showMiddleLine: false,
                                      ),
                                      margin: const EdgeInsets.only(left: 10),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ):isRecordingCompleted == true?
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colorconstands.neutralWhite,
                                      borderRadius: BorderRadius.circular(25)
                                    ),
                                    height: 50,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            IconButton(
                                              padding:const EdgeInsets.all(0),
                                              onPressed:() {
                                                !isplay? playerController.startPlayer(finishMode: FinishMode.pause):playerController.pausePlayer();
                                                  playerController.onPlayerStateChanged.listen((event) {
                                                    if(event.isPlaying){
                                                        setState(() {
                                                          isplay =true;
                                                        },);
                                                    }else if(event.isPaused){
                                                        setState(() {
                                                          isplay =false;
                                                        },);
                                                    }
                                                  });
                                                  playerController.onCompletion.listen((event) {
                                                    setState(() {
                                                      playDuration = (playerController.maxDuration~/1000).toInt().toMMSS();
                                                      playerController.setRefresh(false);
                                                    });
                                                  });
                                                  playerController.onCurrentDurationChanged.listen((duration) {
                                                    setState(() {
                                                      playDuration = (playerController.maxDuration~/1000 - (duration~/1000).toInt()).toMMSS();
                                                    },);
                                                  });
                                                  
                                              },
                                              icon: Icon(isplay? Icons.pause_rounded:Icons.play_arrow_rounded,color: Colorconstands.primaryColor,size: 35,),
                                            ),
                                              Text(playDuration,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                                          ],
                                        ),
                                      Expanded(
                                          child: AudioFileWaveforms(
                                            margin: const EdgeInsets.symmetric(horizontal: 5),
                                            size: Size(MediaQuery.of(context).size.width/1.5,50),
                                            playerController: playerController,
                                            waveformType: WaveformType.long,
                                            waveformData: waveformData,
                                            animationCurve : Curves.bounceInOut,
                                            enableSeekGesture: false,
                                            playerWaveStyle: const PlayerWaveStyle(
                                                  fixedWaveColor: Colorconstands.mainColorSecondary,
                                                  liveWaveColor: Colorconstands.primaryColor,
                                                  showSeekLine: false
                                              ),
                                            ),
                                      ),
                                      IconButton(
                                        onPressed:loadingsumit == true?(){}:(){
                                        setState(() {
                                          isRecording=false;
                                          isRecordingCompleted = false;
                                          voice = null;
                                          playerController.dispose();
                                        },);
                                      } , icon: const Icon(Icons.delete,color: Colorconstands.errorColor,size: 25,)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ):Container(),
                          listVoice.isNotEmpty?Container(
                            margin:const EdgeInsets.only(top: 18),
                            decoration: BoxDecoration(
                              color: Colorconstands.neutralWhite,
                              borderRadius: BorderRadius.circular(25)
                            ),
                            height: 50,
                            child: Row(
                              children: [
                                Row(
                                  children: [
                                    IconButton(
                                      padding:const EdgeInsets.all(0),
                                      onPressed:() {
                                        !isplayNet? playerControllerNework.startPlayer(finishMode: FinishMode.pause):playerControllerNework.pausePlayer();
                                          playerControllerNework.onPlayerStateChanged.listen((event) {
                                            if(event.isPlaying){
                                                setState(() {
                                                  isplayNet =true;
                                                },);
                                            }else if(event.isPaused){
                                                setState(() {
                                                  isplayNet =false;
                                                },);
                                            }
                                          });
                                          playerControllerNework.onCompletion.listen((event) {
                                            setState(() {
                                              playDurationNet = (playerControllerNework.maxDuration~/1000).toInt().toMMSS();
                                              playerControllerNework.setRefresh(false);
                                            });
                                          });
                                          playerControllerNework.onCurrentDurationChanged.listen((duration) {
                                            setState(() {
                                              playDurationNet = (playerControllerNework.maxDuration~/1000 - (duration~/1000).toInt()).toMMSS();
                                            },);
                                          });
                                          
                                      },
                                      icon: Icon(isplayNet? Icons.pause_rounded:Icons.play_arrow_rounded,color: Colorconstands.primaryColor,size: 35,),
                                    ),
                                      Text(playDurationNet,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                                  ],
                                ),
                                Expanded(
                                  child: Container(
                                    margin:const EdgeInsets.only(left: 22),
                                    child: AudioFileWaveforms(
                                      margin: const EdgeInsets.symmetric(horizontal: 5),
                                      size: Size(MediaQuery.of(context).size.width/1,50),
                                      playerController: playerControllerNework,
                                      waveformType: WaveformType.long,
                                      waveformData: waveformData,
                                      animationCurve : Curves.bounceIn,
                                      enableSeekGesture: false,
                                      playerWaveStyle: const PlayerWaveStyle(
                                            fixedWaveColor: Colorconstands.mainColorSecondary,
                                            liveWaveColor: Colorconstands.primaryColor,
                                            showSeekLine: false
                                        ),
                                      ),
                                  ),
                                ),
                                IconButton(
                                  onPressed: (){
                                    setState(() {
                                      loadingDeleteVoice = true;
                                    },);
                                    DeleteVoiceApi().deleteVoiceApi(id: listVoice[0].id.toString()).then((onValue){
                                      if(onValue == true){
                                        BlocProvider.of<ListMenuVoiceBloc>(context).add(PostCodeGetListMenuVoice(code: widget.code.toUpperCase()));
                                        setState(() {
                                          isplayNet = false;
                                          isplay = false;
                                          playerController.stopPlayer();
                                          playerControllerNework.stopPlayer();
                                          loadingsumit = false;
                                          listVoice.removeAt(0);
                                          loadingDeleteVoice = false;
                                        },);
                                      }
                                      else{
                                        setState(() {
                                          loadingDeleteVoice = false;
                                        },);
                                      }
                                    });
                                  }, 
                                  icon: loadingDeleteVoice ==true?const Center(child: CircularProgressIndicator(),):const Icon(Icons.delete,size: 28,color: Colorconstands.errorColor,)
                                )
                              ],
                            ),
                          ):Container(),
                      ]
                    ),
                  ),
                ),
              ),
              loadingsumit == true? const Center(child: CircularProgressIndicator(),) : voice==null && listMultiMedia.isEmpty ? Container(): Container(
                margin: const EdgeInsets.symmetric(horizontal: 28),
                child: ButtonWidgetCustom(
                  radiusButton: 25, 
                  onTap: loadingsumit == true?(){}:() {
                    setState(() {
                      loadingsumit = true;
                    });
                    if(listMultiMedia.isNotEmpty){
                      listMultiAttachement = listMultiMedia;
                    }
                    if(voice!=null){
                      listMultiAttachement.add(voice!);
                    }
                    PostVoiceApi().postVoiceApi(objectId: widget.objectId, objectType: widget.dataAttach.objectType.toString(), listFile: listMultiAttachement).then((value) {
                      if(value == true){
                        BlocProvider.of<ListMenuVoiceBloc>(context).add(PostCodeGetListMenuVoice(code: widget.code.toUpperCase()));
                        Navigator.of(context).pop();
                        setState(() {
                          loadingsumit = false;
                          isRecording=false;
                          isRecordingCompleted = false;
                          voice = null;
                          playerController.dispose();
                        });
                      }else{
                        setState(() {
                          loadingsumit = false;
                        });
                      }
                    });
                  },
                  title: "SAVE".tr(), 
                  panddingVerButton: 12, 
                  panddinHorButton: 28, 
                  textStyleButton: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.primaryColor), 
                  buttonColor: Colorconstands.neutralWhite
                ),
              ),
              Divider(),
              Container(
                decoration:const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colorconstands.primaryColor,Color(0xFF0C7376), Color(0xFF053031),])),
                padding:const EdgeInsets.symmetric(horizontal: 22,vertical: 22),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: (MediaQuery.of(context).size.width/5),
                      child: MaterialButton(
                        disabledColor: Colorconstands.lightTrunks,
                        height: (MediaQuery.of(context).size.width/5),
                        color: Colorconstands.neutralWhite,
                        padding:const EdgeInsets.all(0),
                        onPressed: voice !=null || listVoice.isNotEmpty?null: (){
                          setState((){
                            _startOrStopRecording(waveformData);
                          });
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(28),
                        ),
                        child: Icon( isRecording == true? Icons.pause_rounded : Icons.mic,size: 55,color: Colorconstands.primaryColor,),
                      ),
                    ),
                    const SizedBox(width: 16,),
                    Container(
                      width: (MediaQuery.of(context).size.width/5),
                      child: MaterialButton(
                        height: (MediaQuery.of(context).size.width/5),
                        color: Colorconstands.neutralWhite,
                        padding:const EdgeInsets.all(0),
                        onPressed: (){
                          getImageFromCamera();
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(28),
                        ),
                        child: const Icon(Icons.camera_alt_rounded,size: 55,color: Colorconstands.primaryColor,),
                      ),
                    ),
                    const SizedBox(width: 16,),
                    Container(
                      width: (MediaQuery.of(context).size.width/5),
                      child: MaterialButton(
                        height: (MediaQuery.of(context).size.width/5),
                        color: Colorconstands.neutralWhite,
                        padding:const EdgeInsets.all(0),
                        onPressed: (){
                          getMediaFromDevice();
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(28),
                        ),
                        child:const Icon(Icons.photo_camera_back_rounded,size: 55,color: Colorconstands.primaryColor,),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getImageFromCamera() async {
    final pickedCamera = await _picker.pickImage(source: ImageSource.camera);
    if (pickedCamera != null) {
      setState(() {
        listMultiMedia.add(File(pickedCamera.path));
      }); 
    }
  }

  Future<void> getMediaFromDevice() async {
    List<XFile>? listMedia = await _picker.pickMultiImage();
    if (listMedia.isNotEmpty) {
      for (var item in listMedia) {
        setState(() {
          listMultiMedia.add(File(item.path));
        });
      }
    }
  }
}