import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/vist_code_screen/state/list_menu_voice_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/vist_code_screen/view/attachment_vital_sign_screen.dart';
import 'package:clinic_application/app/modules/doctor_screen/vist_code_screen/widget/expand_menu_visit_widget.dart';
import 'package:clinic_application/mixins/toast.dart';
import 'package:clinic_application/widgets/button_widget/button_back_widget.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VisitCodeScreen extends StatefulWidget {
  String visitCode;
  VisitCodeScreen({Key? key,required this.visitCode}) : super(key: key);

  @override
  State<VisitCodeScreen> createState() => _VisitCodeScreenState();
}

class _VisitCodeScreenState extends State<VisitCodeScreen> with Toast {

  String clinicId = "";
  String objectId = "";
  String objectType = "";
  bool checkfocususer = false;
  bool checkfocuspass = false;
  bool codeshow = true;
  bool isShowRecord = false;
  bool loadingsumit = false;
  bool displayBodyListMenu = false;  


  @override
  void initState() {
    setState(() {
    
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,top: true,
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Stack(
            children: [
              Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin:const EdgeInsets.only(left: 28),
                        child:const Iconbuttonback(),
                      ),
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.only(top: 18,bottom: 18),
                          child: Text(
                            'Visit Patient'.tr(),
                            style: ThemeConstands.headline2_SemiBold_24
                                .copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Container(width: 60,)
                    ],
                  ),
                  // Container(
                  //   margin:const EdgeInsets.only(top: 18),
                  //   height: 65,
                  //   child: Row(
                  //     crossAxisAlignment: CrossAxisAlignment.center,
                  //     children: [
                  //       Expanded(
                  //         child: Container(
                  //           margin: const EdgeInsets.only(left: 18,right: 18),
                  //           padding: EdgeInsets.zero,
                  //           decoration: BoxDecoration(
                  //             color: Colorconstands.lightGohan,
                  //             borderRadius: BorderRadius.circular(25),
                  //             border: Border.all(
                  //                 color: checkfocususer == false
                  //                     ? Colorconstands.neutralGrey
                  //                     : Colorconstands.primaryColor,
                  //                 width: checkfocususer == false
                  //                     ? 1
                  //                     : 2),
                  //           ),
                  //           child: Row(
                  //             children: [
                  //               Expanded(
                  //                 child: TextFormField(
                  //                   controller:codeController,
                  //                   onChanged: ((value) {
                  //                     setState(() {
                  //                      codeController.text;
                  //                     });
                  //                   }),
                  //                   autofocus:true,
                  //                   style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colors.black),
                  //                   decoration: InputDecoration(
                  //                     border: UnderlineInputBorder(borderRadius:BorderRadius.circular(16.0)),
                  //                     contentPadding:const EdgeInsets.only(top: 8,bottom: 5,left: 18,right: 18),
                  //                     labelText: 'Code Visit'.tr(),
                  //                     labelStyle: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.lightTrunks),
                  //                     enabledBorder:const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                  //                     focusedBorder:const UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                  //                   ),
                  //                 ),
                  //               ),
                  //               codeController.text == ""?Container():TextButton(
                  //                 onPressed: (){
                  //                   setState(() {
                  //                     FocusScope.of(context).unfocus();
                  //                     displayBodyListMenu = true;
                  //                   });
                  //                   BlocProvider.of<ListMenuVoiceBloc>(context).add(PostCodeGetListMenuVoice(code: codeController.text.toUpperCase()));
                  //                 }, 
                  //                 child:Text('SEND'.tr(),style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.primaryColor),),
          
                  //               )
                  //             ],
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Expanded(
                    child:BlocBuilder<ListMenuVoiceBloc, ListMenuVoiceState>(
                      builder: (context, state) {
                        if(state is ListMenuVoiceLoading){
                          return const Center(
                            child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                          );
                        }
                        else if(state is ListMenuVoiceLoaded){
                          var data = state.listMenuVoiceModel.data;
                          return Container(
                            margin:const EdgeInsets.symmetric(horizontal: 18),
                            child: Column(
                              children: [
                                // const Divider(height: 48,thickness: 1,),
                                Expanded(
                                  child: ListView.separated(
                                    padding:const EdgeInsets.all(0),
                                    shrinkWrap: true,
                                    physics:const ClampingScrollPhysics(),
                                    itemCount: data!.data!.length,
                                    itemBuilder: (context, index) {
                                      return ExpandMenuVisitWidget(
                                        isClick: true,
                                        colorButtonExpand: Colorconstands.exam,
                                        titleHead: data.data![index].name.toString(),
                                        isExpand: data.data![index].isExpand,
                                        numberRank:"${index+1}. ",
                                        widget: ListView.separated(
                                          padding:const EdgeInsets.symmetric(horizontal: 12),
                                          shrinkWrap: true,
                                          physics:const ClampingScrollPhysics(),
                                          itemCount: data.data![index].child!.length,
                                          itemBuilder: (context, indexChild) {
                                            return GestureDetector(
                                              onTap: () {
                                              },
                                              child: Container(
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        color: Colorconstands.exam,
                                                        borderRadius: BorderRadius.circular(25)
                                                      ),
                                                      padding:const EdgeInsets.symmetric(horizontal: 16,vertical: 2),
                                                      child: Row(
                                                        children: [
                                                          Expanded(
                                                            child: Text(data.data![index].child![indexChild].name.toString(),style: ThemeConstands.headline5_SemiBold_16,)
                                                          ),
                                                          Container(
                                                            width:data.data![index].child![indexChild].data!.where((element){return element.fileType == ".m4a";}).toList().isEmpty && data.data![index].child![indexChild].data!.where((element){return element.fileType != ".m4a";}).toList().isEmpty?40:80,
                                                            alignment: Alignment.center,
                                                            child: MaterialButton(
                                                              height: 35,
                                                              splashColor: Colors.transparent,
                                                              highlightColor: Colors.transparent,
                                                              padding:const EdgeInsets.all(0),
                                                              onPressed:() {
                                                                Navigator.push(context,PageRouteBuilder(
                                                                    pageBuilder: (context, animation1, animation2) =>  AttachmentVitalSingScreen(dataAttach: data.data![index].child![indexChild],code: widget.visitCode,objectId:data.objectId.toString(),nameAttach: "${data.data![index].child![indexChild].name}",nameAttachGroup: "( ${data.data![index].name.toString()} )",),
                                                                    transitionDuration: Duration.zero,
                                                                    reverseTransitionDuration: Duration.zero,
                                                                  ),
                                                                );
                                                              },
                                                              child: data.data![index].child![indexChild].data!.where((element){return element.fileType == ".m4a";}).toList().isEmpty && data.data![index].child![indexChild].data!.where((element){return element.fileType != ".m4a";}).toList().isEmpty ?Container(
                                                                alignment: Alignment.centerRight,
                                                                  height: 35,width: 35,
                                                                  child:const Icon(Icons.attachment_rounded,size: 28,color:Colorconstands.black,))
                                                                : Row(
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: [
                                                                  Container(
                                                                    height: 35,width: 35,
                                                                    child: Stack(
                                                                      children: [
                                                                        const Positioned(
                                                                          top: 0,left:0,right: 0,bottom:0,
                                                                          child:Icon(Icons.image_rounded,size: 26,color:Colorconstands.primaryColor,)),
                                                                        Positioned(
                                                                          top: 0,right: 0,
                                                                          child: Container(
                                                                            height: 18,width: 18,
                                                                            decoration:const BoxDecoration(
                                                                              shape: BoxShape.circle,
                                                                              color: Colorconstands.errorColor
                                                                            ),
                                                                            child: Text(data.data![index].child![indexChild].data!.where((element){return element.fileType != ".m4a";}).toList().length.toString(),textAlign: TextAlign.center,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.neutralWhite),)))
                                                                      ],
                                                                    )
                                                                  ),
                                                                  Container(
                                                                    height: 35,width: 35,
                                                                    child: Stack(
                                                                      children: [
                                                                        const Positioned(
                                                                          top: 0,left:0,right: 0,bottom:0,
                                                                          child:Icon(Icons.voice_chat,size: 26,color:Colorconstands.primaryColor,)),
                                                                        Positioned(
                                                                          top: 0,right: 0,
                                                                          child: Container(
                                                                            height: 18,width: 18,
                                                                            decoration:const BoxDecoration(
                                                                              shape: BoxShape.circle,
                                                                              color: Colorconstands.errorColor
                                                                            ),
                                                                            child: Text(data.data![index].child![indexChild].data!.where((element){return element.fileType == ".m4a";}).toList().length.toString(),textAlign: TextAlign.center,style: ThemeConstands.overline_Semibold_12.copyWith(color: Colorconstands.neutralWhite),)))
                                                                      ],
                                                                    )
                                                                  ),
                                                                ],
                                                              )),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          },
                                          separatorBuilder: (context, index) {
                                            return const SizedBox(height: 6,);
                                          },
                                        )
                                      );
                                    },
                                    separatorBuilder: (context, index) {
                                      return const SizedBox(height: 18,);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                        else{
                          return  const Center(child: EmptyWidget(icon: Icon(Icons.error_outline,size: 80,color: Colorconstands.errorColor,),title:"Code Invalide",subtitle: "",));
                        }
                      },
                    )
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }                                                         
}
