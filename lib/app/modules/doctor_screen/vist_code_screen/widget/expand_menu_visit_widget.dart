import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/widgets/expansion/expand_section.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ExpandMenuVisitWidget extends StatefulWidget {
  final String titleHead;
  bool isExpand;
  bool isClick;
  Widget widget;
  String numberRank;
  Color colorButtonExpand;
  ExpandMenuVisitWidget({
    Key? key,
    required this.numberRank,
    required this.isClick,
    required this.titleHead,
    required this.colorButtonExpand,
    required this.widget,
    required this.isExpand,
  }) : super(key: key);

  @override
  State<ExpandMenuVisitWidget> createState() => _ExpandMenuVisitWidgetState();
}

class _ExpandMenuVisitWidgetState extends State<ExpandMenuVisitWidget>
    with TickerProviderStateMixin {
  late Animation _arrowAnimation;
  late AnimationController _arrowAnimationController;
  final GlobalKey expansionTileKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    _arrowAnimation =
        Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController);
  }

  void _scrollToSelectedContent({required GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(const Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: const Duration(milliseconds: 200));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: expansionTileKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MaterialButton(
            height: 60,
            highlightColor: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            padding: const EdgeInsets.all(0),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
              ),
            ),
            onPressed: widget.isClick == true? () {
              setState(() {
                widget.isExpand = !widget.isExpand;
                _arrowAnimationController.isCompleted
                    ? _arrowAnimationController.reverse()
                    : _arrowAnimationController.forward();
                _scrollToSelectedContent(expansionTileKey: expansionTileKey);
              });
            }:(){},
            child: Container(
              padding: const EdgeInsets.only(
                  left: 12, top: 12, bottom: 12, right: 18),
              child: Row(
                children: [
                  Container(
                    margin:const EdgeInsets.only(right: 6),
                    child: Text(widget.numberRank,style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),),
                  ),
                  Expanded(
                    child: Text(
                      widget.titleHead,
                      style:ThemeConstands.headline3_SemiBold_20.copyWith(
                              color: Colorconstands.neutralWhite,),textAlign: TextAlign.left,
                    ),
                  ),
                   widget.isClick == true?Align(
                     alignment: Alignment.center,
                     child: AnimatedBuilder(
                       animation: _arrowAnimationController,
                       builder: (context, child) => Transform.rotate(
                         angle: _arrowAnimation.value,
                         child: const Icon(
                           Icons.expand_more_outlined,
                           size: 28.0,color: Colorconstands.neutralWhite,
                         ),
                       ),
                     ),
                   ):Container(width: 25,),
                ],
              ),
            ),
          ),
          AnimatedCrossFade(
            firstChild: Container(height: 0, color: Colorconstands.neutralGrey),
            secondChild: const SizedBox(),
            crossFadeState: widget.isExpand
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            duration: const Duration(milliseconds: 400),
          ),
          ExpandedSection(expand: widget.isExpand, child: widget.widget),
        ],
      ),
    );
  }
}
