import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/doctor_model/profile_doctor/profile_doctor_model.dart';
import 'package:clinic_application/service/apis/doctor_api/get_profile_api/get_doctor_profile_api.dart';
import 'package:equatable/equatable.dart';
part 'profile_doctor_event.dart';
part 'profile_doctor_state.dart';

class ProfileDoctorBloc extends Bloc<ProfileDoctorEvent, ProfileDoctorState> {
  GetDoctorProfilePatient doctorProfilePatient;
  ProfileDoctorBloc({required this.doctorProfilePatient}) : super(ProfileDoctorInitial()) {
    on<GetProfileDoctor>((event, emit) async{
      emit(ProfileDoctorLoading());
        try {
          var doctorData = await doctorProfilePatient.getDoctorProfileApi();
          emit(ProfileDoctorLoaded(profileDoctor: doctorData));
        } catch (e) {
          print("fhgfjh$e");
          emit(ProfileDoctorError());
        }
    });
  }
}
