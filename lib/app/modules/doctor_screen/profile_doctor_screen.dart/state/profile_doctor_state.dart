part of 'profile_doctor_bloc.dart';

class ProfileDoctorState extends Equatable {
  const ProfileDoctorState();
  
  @override
  List<Object> get props => [];
}

class ProfileDoctorInitial extends ProfileDoctorState {}

class ProfileDoctorLoading extends ProfileDoctorState {}
class ProfileDoctorLoaded extends ProfileDoctorState {
  final ProfileDoctorModel profileDoctor;
  const ProfileDoctorLoaded({required this.profileDoctor});
}
class ProfileDoctorError extends ProfileDoctorState {}
