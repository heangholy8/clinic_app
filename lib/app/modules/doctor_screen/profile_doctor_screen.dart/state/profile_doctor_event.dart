part of 'profile_doctor_bloc.dart';

class ProfileDoctorEvent extends Equatable {
  const ProfileDoctorEvent();

  @override
  List<Object> get props => [];
}

class GetProfileDoctor extends ProfileDoctorEvent{}
