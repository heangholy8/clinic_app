import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CardFeatureProfile extends StatelessWidget {
  String title;
  String subTitle;
  Color colorCard;
  String icon;
  VoidCallback onPressed;
  double borderRadius;
  CardFeatureProfile({Key? key,required this.borderRadius,required this.onPressed,required this.colorCard,required this.icon,required this.subTitle,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      color: colorCard,
      padding:const EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius)
      ),
      child: Container(
        margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 18),
        child: Row(
          children: [
            Container(
              margin:const EdgeInsets.only(right: 18),
              child: SvgPicture.asset(icon,width: 40,height: 40,),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(title,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                  subTitle == ""?Container():Text(subTitle,style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign: TextAlign.left,),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}