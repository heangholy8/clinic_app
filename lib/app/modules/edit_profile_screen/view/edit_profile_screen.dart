import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:flutter/material.dart';

class EditProfileUserScreen extends StatefulWidget {
  final String imageProfile;
  final String firstName;
  final String lastName;
  const EditProfileUserScreen({super.key,required this.imageProfile,required this.firstName,required this.lastName});

  @override
  State<EditProfileUserScreen> createState() => _EditProfileUserScreenState();
}

class _EditProfileUserScreenState extends State<EditProfileUserScreen> {
  TextEditingController firstnameController= TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  @override
  void initState() {
    setState(() {
      firstnameController.text = widget.firstName;
      lastNameController.text = widget.lastName;
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            HeaderFeature(titleHeader: "Profile user"),
            Expanded(
              child: Container(
                margin:const EdgeInsets.only(top: 28),
                decoration:BoxDecoration(
                  color: Colorconstands.neutralWhite,
                  borderRadius: BorderRadius.circular(18)
                ),
                child: Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.all(18),
                          child:Column(
                            children: [
                              Container(
                                margin:const EdgeInsets.only(bottom: 18),
                                child:CircleAvatar(
                                  radius: 50,
                                  backgroundImage: NetworkImage(widget.imageProfile),
                                ),
                              ),
                              ListTile(
                                  title: Text("First Name",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                                  subtitle: Container(
                                    margin:const EdgeInsets.only(top: 8),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: firstnameController,
                                      style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                      decoration:InputDecoration(
                                        border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                        contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                        hintText: 'First Name',
                                        hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                        focusedBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                        ),
                                        enabledBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                ListTile(
                                  title: Text("Last Name",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
                                  subtitle: Container(
                                    margin:const EdgeInsets.only(top: 8),
                                    child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      controller: lastNameController,
                                      style:  ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),
                                      decoration:InputDecoration(
                                        border:const OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(14)) ),
                                        contentPadding:const EdgeInsets.symmetric(horizontal: 15),
                                        hintText: 'Last Name',
                                        hintStyle: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),
                                        focusedBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.primaryColor,width: 2)
                                        ),
                                        enabledBorder:const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(14)),
                                          borderSide: BorderSide(color: Colorconstands.black,width: 1)
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                            ]
                          )
                        )
                      ),
                    ),
                  ],
                ),
              )
            )
          ]
        )
      ),
    );
  }
}