import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({super.key});

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  bool _switchValue = true;
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            HeaderFeature(titleHeader: "Setting App"),
            Expanded(
              child: Container(
                margin:const EdgeInsets.only(top: 28),
                decoration:BoxDecoration(
                  color: Colorconstands.neutralWhite,
                  borderRadius: BorderRadius.circular(18)
                ),
                child: Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.all(22),
                          child:Column(
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        "Notification",style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),
                                      ),
                                    ),
                                    Container(
                                      child: CupertinoSwitch(
                                        activeColor: Colorconstands.primaryColor,
                                        thumbColor: Colorconstands.neutralGrey,
                                        value: _switchValue,
                                        onChanged: (value) {
                                          setState(() {
                                            _switchValue = value;
                                          });
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ]
                          )
                        )
                      ),
                    ),
                  ],
                ),
              )
            )
          ]
        )
      ),
    );
  }
}