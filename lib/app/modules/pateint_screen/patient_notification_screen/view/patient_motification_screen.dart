import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/home_doctor_screen/view/home_doctor_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/home_page/widget/nav_home_page.dart';
import 'package:flutter/material.dart';

class PatientNotification extends StatefulWidget {
  final bool fromPatient ;
  const PatientNotification({super.key,required this.fromPatient});

  @override
  State<PatientNotification> createState() => _PatientNotificationState();
}

class _PatientNotificationState extends State<PatientNotification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Expanded(
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 28,vertical: 18),
                    alignment: Alignment.centerLeft,
                    child: Text("Notification",style: ThemeConstands.headline2_SemiBold_242.copyWith(color: Colorconstands.neutralWhite),),
                  ),
                  Expanded(
                    child: Container(
                      margin:const EdgeInsets.only(top: 12),
                      child: ListView.builder(
                        padding:const EdgeInsets.all(0),
                        itemCount:widget.fromPatient == true?1:2,
                        itemBuilder: (context, index) {
                          return Container(
                            padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 12),
                            margin:const EdgeInsets.only(top: 12),
                             color: Colorconstands.neutralWhite.withOpacity(1),
                             child: Row(
                              children: [
                                Container(
                                  margin:const EdgeInsets.only(right: 18),
                                  child:const Icon(Icons.campaign_outlined,color: Colorconstands.primaryColor,size: 55,)),
                                Expanded(
                                  child: Column(
                                     mainAxisAlignment: MainAxisAlignment.start,
                                     crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Container(
                                              alignment: Alignment.centerLeft,
                                              child: Container(
                                                padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 2),
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(16),
                                                  color: Colorconstands.primaryColor
                                                ),
                                                child: Text("Notice",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite),),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: Text(index == 0?"08/06/2024" : "01/06/2024",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),),
                                          )
                                        ],
                                      ),
                                      Text(index == 0?"It's time to take a break" : "To number for meeting",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.left,),
                                    ],
                                  ),
                                )
                              ],
                             ),
                          );
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
                padding:const EdgeInsets.only(bottom: 28,top: 16),
                decoration:const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colorconstands.primaryColor,Color(0xFF0C7376), Color(0xFF053031),])),
            child: NavHomePage(acitiveNav: 3,imageProfile: profilePicture,))
          ],
        ),
      ),
    );
  }
}