import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/feature_model/physical_model.dart';
import 'package:clinic_application/service/apis/pateint_api/all_menu_api/menu_api.dart';
import 'package:equatable/equatable.dart';
part 'physical_history_event.dart';
part 'physical_history_state.dart';

class PhysicalHistoryBloc extends Bloc<PhysicalHistoryEvent, PhysicalHistoryState> {
  GetMenuFeatureApi getMenuFeature;
  PhysicalHistoryBloc({required this.getMenuFeature}) : super(PhysicalHistoryInitial()) {
    on<GetPhysicalHistory>((event, emit) async {
      emit(PhysicalHistoryLoading());
      try{
        var data = await getMenuFeature.getPhysicalHistoryApi();
        emit(PhysicalHistoryLoaded(physicalHistory: data));
      }catch (e){
        emit(PhysicalHistoryError());
      }
    });
  }
}
