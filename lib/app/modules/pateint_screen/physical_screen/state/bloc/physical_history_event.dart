part of 'physical_history_bloc.dart';

sealed class PhysicalHistoryEvent extends Equatable {
  const PhysicalHistoryEvent();

  @override
  List<Object> get props => [];
}

class GetPhysicalHistory extends  PhysicalHistoryEvent{}
