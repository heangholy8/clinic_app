part of 'physical_history_bloc.dart';

sealed class PhysicalHistoryState extends Equatable {
  const PhysicalHistoryState();
  
  @override
  List<Object> get props => [];
}

final class PhysicalHistoryInitial extends PhysicalHistoryState {}
final class PhysicalHistoryLoading extends PhysicalHistoryState {}
final class PhysicalHistoryLoaded extends PhysicalHistoryState {
  final PhysicalHistory physicalHistory;
  const PhysicalHistoryLoaded({ required this.physicalHistory});
}
final class PhysicalHistoryError extends PhysicalHistoryState {}
