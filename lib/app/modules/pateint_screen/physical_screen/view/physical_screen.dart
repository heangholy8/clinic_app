import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/physical_screen/state/bloc/physical_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/expand_widget.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class PhysicalScreen extends StatefulWidget {
  const PhysicalScreen({Key? key}) : super(key: key);

  @override
  State<PhysicalScreen> createState() => _PhysicalScreenState();
}

class _PhysicalScreenState extends State<PhysicalScreen> {
  int indexFile = 0;
  bool disbleExpand = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HeaderFeature(
          titleHeader: "Physical History",
        ),
        Expanded(
          child: Container(
            margin:const EdgeInsets.only(top: 16),
            child: BlocBuilder<PhysicalHistoryBloc, PhysicalHistoryState>(
              builder: (context, state) {
                if(state is PhysicalHistoryLoading){
                  return const Center(
                    child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                  );
                }
                else if(state is PhysicalHistoryLoaded){
                  var Physical = state.physicalHistory.data;
                  return Physical!.isEmpty?EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "")
                  :ListView.builder(
                    shrinkWrap: true,
                    physics:const ClampingScrollPhysics(),
                    itemCount: Physical.length,
                    padding:const EdgeInsets.all(0),
                    itemBuilder: (context, index) {
                      if(Physical.isNotEmpty && disbleExpand == false){
                        Physical[indexFile].isExpand = true;
                      }
                      return Container(
                        margin:const EdgeInsets.only(left: 28,right: 28,top: 12),
                        child: expandCustom(
                          onPressed: (){
                            if(Physical[index].isExpand == true){
                              setState(() {
                                Physical[index].isExpand = false;
                                disbleExpand = true;
                              });
                            }
                            else{
                              for (var file in Physical) {
                                setState(() {
                                  file.isExpand = false;
                                  indexFile = index;
                                });
                              }
                              setState(() {
                                Physical[index].isExpand = true;
                              });
                            }
                          },
                          isClick: true,
                          colorButtonExpand: Colorconstands.exam,
                          titleHead: Physical[index].date,
                          isExpand: Physical[index].isExpand,
                          iconLeft:const Icon(Icons.access_time_rounded,size: 30,color: Colorconstands.neutralDarkGrey,),
                          widget: Container(
                            decoration:const BoxDecoration(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                              color: Colorconstands.neutralWhite,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                                    child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Physical[index].presentIllness == ""|| Physical[index].presentIllness == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text("Present Illness:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].presentIllness}'),
                                                  //Text(Medical[index].pastMedicalHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].other == ""|| Physical[index].other == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Other:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].other}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].clinicFeature == ""|| Physical[index].clinicFeature == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Clinic feature:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].clinicFeature.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].clinicalFeatureTypeValue == ""|| Physical[index].clinicalFeatureTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Clinical feature type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].clinicalFeatureTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].generalAppearance == ""|| Physical[index].generalAppearance == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("General appearance:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].generalAppearance.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].generalAppearanceTypeValue == ""|| Physical[index].generalAppearanceTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("General appearance type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].generalAppearanceTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].ent == ""|| Physical[index].ent == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("ent:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].ent.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].entTypeValue == ""|| Physical[index].entTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("ent type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].entTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].cardiovascularSystem == ""|| Physical[index].cardiovascularSystem == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Cardiovascular system:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].cardiovascularSystem.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].cardiovascularSystemTypeValue == ""|| Physical[index].cardiovascularSystemTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Cardiovascular system type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].cardiovascularSystemTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].respiratorySystem == ""|| Physical[index].respiratorySystem == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Respiratory system:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].respiratorySystem.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].respiratorySystemTypeValue == ""|| Physical[index].respiratorySystemTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Respiratory system type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].respiratorySystemTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].gastroIntestinalSystem == "" || Physical[index].gastroIntestinalSystem == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Gastro intestinal system:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].gastroIntestinalSystem.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].gastroIntestinalSystemTypeValue == ""|| Physical[index].gastroIntestinalSystemTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Gastro intestinal system type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].gastroIntestinalSystemTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].urogenitalSystem == "" || Physical[index].urogenitalSystem == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Urogenital system:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].urogenitalSystem.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].urogenitalSystemTypeValue == ""|| Physical[index].urogenitalSystemTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Urogenital system type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].urogenitalSystemTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].centralNervousSystem == "" || Physical[index].centralNervousSystem == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Central nervous system:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].centralNervousSystem.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].centralNervousSystemTypeValue == ""|| Physical[index].centralNervousSystemTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Central nervous system type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].centralNervousSystemTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].skinExtremity == "" || Physical[index].skinExtremity == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Skin extremity:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].skinExtremity.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].skinExtremityTypeValue == ""|| Physical[index].skinExtremityTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Skin extremity type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].skinExtremityTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].mentalStatus == "" || Physical[index].mentalStatus == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Mental status:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].mentalStatus.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].mentalStatusTypeValue == ""|| Physical[index].mentalStatusTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Mental status type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].mentalStatusTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].glasgowComaScale == "" || Physical[index].glasgowComaScale == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Glasgow coma scale:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].glasgowComaScale.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].glasgowComaScaleTypeValue == ""|| Physical[index].glasgowComaScaleTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("Glasgow coma scale type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].glasgowComaScaleTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].abdomen == "" || Physical[index].abdomen == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("abdomen:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  //HtmlWidget('${Physical[index].clinicFeature}'),
                                                  Text(Physical[index].abdomen.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                              Physical[index].abdomenTypeValue == ""|| Physical[index].abdomenTypeValue == null? Container():Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  const SizedBox(height: 10,),
                                                  Text("abdomen type value:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                                  HtmlWidget('${Physical[index].abdomenTypeValue}'),
                                                  //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                                ],
                                              ),
                                            ],
                                          ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                else{
                   return EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "");
                }
              },
            ),
          ),
        )
      ],
    );
  }
}