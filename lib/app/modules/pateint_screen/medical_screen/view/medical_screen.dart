import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/medical_screen/state/medical_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/expand_widget.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class MedicalScreen extends StatefulWidget {
  const MedicalScreen({Key? key}) : super(key: key);

  @override
  State<MedicalScreen> createState() => _MedicalScreenState();
}

class _MedicalScreenState extends State<MedicalScreen> {
  int indexFile = 0;
  bool disbleExpand = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HeaderFeature(
          titleHeader: "Medical History",
        ),
        Expanded(
          child: Container(
            margin:const EdgeInsets.only(top: 16),
            child: BlocBuilder<MedicalHistoryBloc, MedicalHistoryState>(
              builder: (context, state) {
                if(state is MedicalHistoryLoading){
                  return const Center(
                    child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                  );
                }
                else if(state is MedicalHistoryLoaded){
                  var Medical = state.medicalHistory.data;
                  return Medical!.isEmpty?EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "")
                  : ListView.builder(
                    shrinkWrap: true,
                    physics:const ClampingScrollPhysics(),
                    itemCount: Medical.length,
                    padding:const EdgeInsets.all(0),
                    itemBuilder: (context, index) {
                      if(Medical.isNotEmpty && disbleExpand == false){
                        Medical[indexFile].isExpand = true;
                      }
                      return  Container(
                        margin:const EdgeInsets.only(left: 28,right: 28,top: 12),
                        child: expandCustom(
                           onPressed: (){
                            if(Medical[index].isExpand == true){
                              setState(() {
                                Medical[index].isExpand = false;
                                disbleExpand = true;
                              });
                            }
                            else{
                              for (var file in Medical) {
                                setState(() {
                                  file.isExpand = false;
                                  indexFile = index;
                                });
                              }
                              setState(() {
                                Medical[index].isExpand = true;
                              });
                            }
                          },
                          isClick: true,
                          colorButtonExpand: Colorconstands.exam,
                          titleHead: Medical[index].data.toString(),
                          isExpand:  Medical[index].isExpand,
                          iconLeft:const Icon(Icons.access_time_rounded,size: 30,color: Colorconstands.neutralDarkGrey,),
                          widget: Container(
                            decoration:const BoxDecoration(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                              color: Colorconstands.neutralWhite,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Allergies:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            ListView.builder(
                                              padding:const EdgeInsets.all(0),
                                              shrinkWrap: true,
                                              itemCount: Medical[index].allergies!.length,
                                              itemBuilder: (context, subindex) {
                                                return Text("${subindex+1}. ${Medical[index].allergies![subindex].categoryName.toString()}",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,);
                                              },
                                            )
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Past Medical History:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            HtmlWidget('${Medical[index].pastMedicalHistory}'),
                                            //Text(Medical[index].pastMedicalHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Social History:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            HtmlWidget('${Medical[index].socialHistory}'),
                                            //Text(Medical[index].socialHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Family History:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            HtmlWidget('${Medical[index].familyHistory}'),
                                            //Text(Medical[index].familyHistory.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Medications:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            HtmlWidget('${Medical[index].medications}'),
                                            // Text(Medical[index].medications.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Review of Systems:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            HtmlWidget('${Medical[index].reviewOfSystems}'),
                                            // Text(Medical[index].reviewOfSystems.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                else{
                  return EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "");
                }
              },
            ),
          ),
        )
      ],
    );
  }
}