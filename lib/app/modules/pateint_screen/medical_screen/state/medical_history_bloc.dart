import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/feature_model/medical_model.dart';
import 'package:clinic_application/service/apis/pateint_api/all_menu_api/menu_api.dart';
import 'package:equatable/equatable.dart';
part 'medical_history_event.dart';
part 'medical_history_state.dart';

class MedicalHistoryBloc extends Bloc<MedicalHistoryEvent, MedicalHistoryState> {
  GetMenuFeatureApi getMenuFeature;
  MedicalHistoryBloc({required this.getMenuFeature}) : super(MedicalHistoryInitial()) {
    on<GetMedicalHistoryEvent>((event, emit) async{
      emit(MedicalHistoryLoading());
      try{
        var data = await getMenuFeature.getMedicalHistoryApi();
        emit(MedicalHistoryLoaded(medicalHistory: data));
      }catch (e){
        emit(MedicalHistoryError());
      }
    });
  }
}
