part of 'medical_history_bloc.dart';

sealed class MedicalHistoryState extends Equatable {
  const MedicalHistoryState();
  
  @override
  List<Object> get props => [];
}

final class MedicalHistoryInitial extends MedicalHistoryState {}

final class MedicalHistoryLoading extends MedicalHistoryState {}
final class MedicalHistoryLoaded extends MedicalHistoryState {
  final MedicalHistoryModel  medicalHistory;
  const MedicalHistoryLoaded({required this.medicalHistory});
}
final class MedicalHistoryError extends MedicalHistoryState {}
