part of 'medical_history_bloc.dart';

sealed class MedicalHistoryEvent extends Equatable {
  const MedicalHistoryEvent();

  @override
  List<Object> get props => [];
}

class GetMedicalHistoryEvent extends MedicalHistoryEvent{}
