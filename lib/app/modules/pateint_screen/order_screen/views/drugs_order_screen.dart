import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/order_screen/views/drug_order_detail.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/model/pateint_model/feature_model/prescription_model_test.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:flutter/material.dart';

class DrugsOrderScreen extends StatefulWidget {
  const DrugsOrderScreen({Key? key}) : super(key: key);

  @override
  State<DrugsOrderScreen> createState() => _DrugsOrderScreenState();
}

class _DrugsOrderScreenState extends State<DrugsOrderScreen> {
  bool isSelectAll = false;
  @override
  Widget build(BuildContext context) {
    TextEditingController searchCtrl = TextEditingController();
    List<PrescriptionHistoryModelModel> item = allPrescription;
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            HeaderFeature(
              titleHeader: "Medical Order",
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10,horizontal: 25),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                        textAlign: TextAlign.left,
                        controller: searchCtrl,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            hintText: 'Search a medical name',
                            hintStyle: const TextStyle(fontSize: 16),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: const BorderSide(
                                    width: 0, 
                                    style: BorderStyle.none,
                                ),
                            ),
                            filled: true,
                            contentPadding: const EdgeInsets.symmetric(vertical: 8,horizontal: 10),
                            fillColor: Colorconstands.white,
                        ),
                    ),
                  ),
                  const SizedBox(width: 15,),
                  SizedBox(
                    width: 100,
                    child: MaterialButton(
                        height: 50,
                        padding:const EdgeInsets.all(5),
                        onPressed: (){
                          // Navigator.of(context).pop();
                        },
                        color: Colorconstands.white,
                        shape:RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child:Row(
                          children: [
                            Expanded(child: Text("Search",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.center,)),
                          ],
                        ),
                      ),
                  ),
                  ],
                ),
              ),      
            Expanded(
              child: EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "")
              // child:  Container(
              //   margin: const EdgeInsets.only(top: 10),
              //  decoration: const BoxDecoration(color: Colorconstands.white,borderRadius: BorderRadius.vertical(top: Radius.circular(10)),),
              //   padding:const EdgeInsets.only(top: 16,left: 25,right: 25),
              //   child: SingleChildScrollView(
              //     child: Column(
              //       children: [
              //         Align(
              //           alignment: Alignment.centerLeft,
              //           child: Row(
              //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //             children: [
              //               Text("Last Order",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),),
              //               InkWell(
              //                 onTap: () {
              //                   setState(() {
              //                     isSelectAll=!isSelectAll;
              //                   });
              //                 },
              //                 child: Text(isSelectAll?"Unselect":"Select all",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.primaryColor,),)),
              //             ],
              //           )),
              //         ListView.builder(
              //           physics: const NeverScrollableScrollPhysics(),
              //           shrinkWrap: true,
              //           itemCount: item[0].prescription.length-2,
              //           itemBuilder: (context, index) {
              //             return Padding(
              //               padding: const EdgeInsets.symmetric(vertical: 5),
              //               child: Row(
              //                  crossAxisAlignment:CrossAxisAlignment.center,
              //                  mainAxisAlignment:MainAxisAlignment.spaceBetween,
              //                 children: [
              //                   ClipRRect(
              //                         borderRadius: BorderRadius.circular(12.0),
              //                         child: Image.network(
              //                             item[0].prescription[index].image,
              //                             height: 50.0,
              //                             width: 50.0,fit: BoxFit.cover,
              //                   ),),
              //                  const SizedBox(width: 10,),
              //                     Expanded(
              //                       child: Align(
              //                         alignment: Alignment.centerLeft,
              //                         child: Text(item[0].prescription[index].name,style: ThemeConstands.headline5_SemiBold_16,),
              //                       ),
              //                     ),
              //                     IconButton(onPressed: () {},
              //                     icon: Icon(isSelectAll? Icons.shopping_cart_sharp:Icons.shopping_cart_outlined,size: 26,color: Colorconstands.primaryColor,),),
              //               ],),
              //             );
              //           },),
              //         Align(
              //           alignment: Alignment.centerLeft,
              //           child: Text("Medical List",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),)),
              //         ListView.builder(
              //           physics: const NeverScrollableScrollPhysics(),
              //           shrinkWrap: true,
              //           itemCount: item[0].prescription.length,
              //           itemBuilder: (context, index) {
              //             return Padding(
              //               padding: const EdgeInsets.symmetric(vertical: 5),
              //               child: Row(
              //                  crossAxisAlignment:CrossAxisAlignment.center,
              //                  mainAxisAlignment:MainAxisAlignment.spaceBetween,
              //                 children: [
              //                   ClipRRect(
              //                         borderRadius: BorderRadius.circular(12.0),
              //                         child: Image.network(
              //                             item[0].prescription[index].image,
              //                             height: 50.0,
              //                             width: 50.0,fit: BoxFit.cover,
              //                   ),),
              //                  const SizedBox(width: 10,),
              //                     Expanded(
              //                       child: Align(
              //                         alignment: Alignment.centerLeft,
              //                         child: Text(item[0].prescription[index].name,style: ThemeConstands.headline5_SemiBold_16,),
              //                       ),
              //                     ),
              //                     IconButton(onPressed: () {},
              //                     icon: Icon(index%2==0? Icons.shopping_cart_sharp:Icons.shopping_cart_outlined,size: 26,color: Colorconstands.primaryColor,),),
              //               ],),
              //             );
              //           },),  
              //       ],
              //     ),
              //   ),
              // ),
            ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 18,horizontal: 25),
            //   child: MaterialButton(
            //     height: 50,
            //     padding:const EdgeInsets.all(5),
            //     onPressed: (){
            //       Navigator.push(context, MaterialPageRoute(builder: (context) => const DrugsOrderDetailScreen(),));
            //     },
            //     color: Colorconstands.white,
            //     shape:RoundedRectangleBorder(
            //       borderRadius: BorderRadius.circular(25)
            //     ),
            //     child:Row(
            //       children: [
            //         Expanded(child: Text("Add to Cart (3 items)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.center,)),
            //       ],
            //     ),
            //   ),
            // ),
            
          ],
        ),
      ),
    );
  }
}