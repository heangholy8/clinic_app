import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/model/pateint_model/feature_model/prescription_model_test.dart';
import 'package:flutter/material.dart';


class DrugsOrderDetailScreen extends StatefulWidget {
  const DrugsOrderDetailScreen({Key? key}) : super(key: key);

  @override
  State<DrugsOrderDetailScreen> createState() => _DrugsOrderDetailScreenState();
}

class _DrugsOrderDetailScreenState extends State<DrugsOrderDetailScreen> {
  @override
  Widget build(BuildContext context) {
    List<PrescriptionHistoryModelModel> item = allPrescription;
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            HeaderFeature(
              titleHeader: "Medical Order Detail",
            ),
             Expanded(
              child: Container(
                margin: const EdgeInsets.only(top: 18),
                decoration: const BoxDecoration(color: Colorconstands.white,borderRadius: BorderRadius.vertical(top: Radius.circular(18))),
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                        padding:const EdgeInsets.symmetric(horizontal: 18),
                        margin:const EdgeInsets.only(top: 10),
                        child: ListView.separated(
                       
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        shrinkWrap: true,
                        itemCount: item[0].prescription.length,
                        itemBuilder:(context, index) {
                          return Row(
                            crossAxisAlignment:CrossAxisAlignment.center,
                            mainAxisAlignment:MainAxisAlignment.spaceBetween,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(12.0),
                                child: Image.network(
                                    item[0].prescription[index].image,
                                    height: 50.0,
                                    width: 50.0,fit: BoxFit.cover,
                                ),
                                                    ),
                              const SizedBox(width: 10,),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(item[0].prescription[index].name,style: ThemeConstands.headline5_SemiBold_16,),
                                ),
                              ),
                              const SizedBox(width: 10,),
                              Container(
                                color: Colorconstands.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 26,
                                      child: MaterialButton(
                                        height: 20,
                                        onPressed: () {},
                                        padding:const EdgeInsets.all(0),
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        shape:const CircleBorder(side: BorderSide(color: Colorconstands.neutralDarkGrey)),
                                         child:const Padding(
                                          padding: EdgeInsets.only(bottom: 15),
                                          child: Icon(Icons.minimize_outlined)),
                                      ),
                                    ),
                                    Container(
                                      width: 30,
                                      child: Column(
                                        children: [
                                          Text(item[0].prescription[index].total.toString(),style: ThemeConstands.headline5_SemiBold_16,textAlign: TextAlign.center,),
                                          Container(
                                            width: 30,
                                            child:Text("tbl",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color:const Color(0xFF828492)),textAlign: TextAlign.center,)
                                          ),
                                        ],
                                      )
                                    ),
                                    Container(
                                      width: 30,
                                      child: MaterialButton(
                                        height: 20,
                                        onPressed: () {},
                                        padding:const EdgeInsets.all(0),
                                        splashColor: Colors.transparent,
                                        highlightColor: Colors.transparent,
                                        shape:const CircleBorder(side: BorderSide(color: Colorconstands.neutralDarkGrey)),
                                        child:const Icon(Icons.add,),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          );
                        },
                        separatorBuilder:(BuildContext context,int index) {
                          return const Divider(
                            indent: 10,
                            endIndent: 10,
                            thickness: 0.5,
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                          );
                        },
                                                                                                                                          ),
                      ),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 12),
                      height: 1,
                      color:const Color(0xFFEFEAEA),
                    ),
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 25),
                      child: Column(
                        children: [
                          const Text("Paymant summary",style: ThemeConstands.headline5_SemiBold_16,textAlign: TextAlign.center,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  "Price",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "\$18.50",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  "Delivery free",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "\$2.0 FREE",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                               Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                 children: [
                                   const Icon(Icons.verified,color: Colorconstands.primaryColor,),
                                   const SizedBox(width: 8,),
                                   Text(
                                      "Membership Discount 5%",
                                      style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                    ),
                                 ],
                               ),
                                
                                Text(
                                  "-\$0.93",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 12,horizontal: 25),
                      height: 1,
                      color:const Color(0xFFEFEAEA),
                    ),
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              "Total",
                              style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                            ),
                            Text(
                              "\$17.57",
                              style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                            ),
                        ],
                      ),
                    ),
                     const SizedBox(height: 10,)
                  ],
                ),
              ),
            ),
            Container(
              padding:const EdgeInsets.symmetric(horizontal: 30,vertical: 18),
              decoration: const BoxDecoration(
                color: Colorconstands.neutralWhite,
                  boxShadow: [
                    BoxShadow(
                      color: Colorconstands.neutralGrey,
                      blurRadius: 15,
                      offset: Offset(-5, 5),
                    ),
                  ],
              ),
              child: MaterialButton(
                height: 50,
                padding:const EdgeInsets.all(5),
                onPressed: (){
                  
                },
                color: Colorconstands.primaryColor,
                shape:RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)
                ),
                child:Row(
                  children: [
                    Expanded(child: Text("Order (\$17.53)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,)),
                  ],
                ),
              ),
            ),
          
          ],
        ),
      ),
    );
  }
}