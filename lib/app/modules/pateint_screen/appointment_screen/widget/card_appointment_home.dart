import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
class CardAppointments extends StatefulWidget {
  final String date;
  final bool isDisplay;
  final String profilePic;
  final String name;
  final String discription;
  final String type;
  final VoidCallback onPressed;
  final VoidCallback onPressedIcon;
  const CardAppointments({Key? key,required this.onPressedIcon,required this.onPressed,required this.date,required this.discription,required this.isDisplay,required this.name,required this.profilePic,required this.type}) : super(key: key);

  @override
  State<CardAppointments> createState() => _CardAppointmentsState();
}

class _CardAppointmentsState extends State<CardAppointments> {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      disabledColor: Colorconstands.gray,
      onPressed: widget.isDisplay == false?null:widget.onPressed,
      color: Colorconstands.neutralWhite,
      padding:const EdgeInsets.all(0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
      child: Container(
        padding:const EdgeInsets.symmetric(horizontal: 28,vertical: 16),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(widget.date,style: ThemeConstands.headline5_SemiBold_16.copyWith(color: widget.isDisplay == false?Colorconstands.neutralDarkGrey:Colorconstands.primaryColor),textAlign: TextAlign.left,),
                          widget.isDisplay == false?Text("Cancelled",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),textAlign: TextAlign.left,):Container(),
                        ],
                      )),
                    ],
                  ),
                ),
                widget.isDisplay == false?Container():IconButton(
                  padding:const EdgeInsets.all(0),
                  onPressed: widget.onPressedIcon, 
                  icon:const Icon(Icons.more_vert_sharp,size: 28,color: Colorconstands.black,),
                ),
              ],
            ),
            const Divider(height: 20,),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: 60,
                    decoration:const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colorconstands.primaryColor,
                    ),
                    padding:const EdgeInsets.all(0),
                    child:CircleAvatar(
                      radius: 28,
                      backgroundImage: NetworkImage(widget.profilePic),
                    ),
                  ),
                  const SizedBox(width: 8,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.name,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: widget.isDisplay == false?Colorconstands.neutralDarkGrey: Colorconstands.black),textAlign: TextAlign.left,),
                        Text(widget.type,style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.darkGray),textAlign: TextAlign.left,),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.discription,style: ThemeConstands.headline5_Medium_16.copyWith(color: widget.isDisplay == false?Colorconstands.neutralDarkGrey:Colorconstands.primaryColor),textAlign: TextAlign.left,),
                    ],
                  )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}