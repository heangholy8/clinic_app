import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/appointment_screen/state/book_appointment/book_appointment_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/appointment_screen/state/get_appointment/patient_appointment_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/appointment_screen/widget/card_appointment_home.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/model/appiontment_model/pateint_appiontment_model.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/appointment_model.dart';
import 'package:clinic_application/service/apis/pateint_api/appointment_api/book_appointment_api.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_calendar/table_calendar.dart';

class AppointmentScreen extends StatefulWidget {
  final String? patientId;
  final AppointmentModelData appointmentData;
  const AppointmentScreen({Key? key, required this.appointmentData,required this.patientId}) : super(key: key);

  @override
  State<AppointmentScreen> createState() => _AppointmentScreenState();
}

class _AppointmentScreenState extends State<AppointmentScreen> {
  
  List<AppointmentModel> appointment = allAppointment;
  bool upcoming = true;
  bool isExtended = true;
  DateTime? _selectedDate;
  ScrollController scrollController = ScrollController(); 
  TextEditingController _textFieldController = TextEditingController();

  bool appAppointmentLoading = false;

  String departmentId = "";
  String doctorId = "";
  String appointmentType = "";
  String tiemBookAppointment = "";

  var dataUpcoming;
  var datapast;

  @override
  void initState() {
    setState(() {
      dataUpcoming = widget.appointmentData.upcoming;
      datapast = widget.appointmentData.past;
      BlocProvider.of<BookAppointmentBloc>(context).add(GetAppointmentType());
      BlocProvider.of<DepartmentBloc>(context).add(GetDepartment());
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          showModalBottomSheet(
            enableDrag: false,
            isDismissible: true,
            backgroundColor: Colors.transparent,
            isScrollControlled: true,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16)),
            ),
            context: context,
            builder: (context) {
              return appointMentType(context,translate);
            }
          );
        },
        isExtended: isExtended,
        backgroundColor: Colorconstands.exam,
        icon:const Icon(Icons.date_range_outlined,size: 22,),
        label: Text('New appointment',style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black),),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30)
        ),
      ),
      body: SafeArea(
        bottom: false,
        child: BlocListener<PatientAppointmentBloc, PatientAppointmentState>(
          listener: (context, state) {
            if(state is PatientAppointmentLoading){
              setState(() {
                appAppointmentLoading = true;
              });
            }
            if(state is PatientAppointmentLoaded){
              setState(() {
                 dataUpcoming = state.patientAppointment.data!.upcoming;
                 datapast = state.patientAppointment.data!.past;
              });
            }
            else{
              setState(() {
                appAppointmentLoading = false;
              });
            }
          },
          child: Column(
                  children: [
                    HeaderFeature(
                      titleHeader: "Appontments",
                    ),
                    const SizedBox(height: 16,),
                    Container(
                      padding:const EdgeInsets.all(5),
                      margin:const EdgeInsets.symmetric(horizontal: 28),
                      height: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colorconstands.black.withOpacity(0.2)
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: MaterialButton(
                              elevation: 0,
                              onPressed: (){
                                setState(() {
                                  if(upcoming == false){
                                    upcoming = true;
                                  }
                                  
                                });
                              },
                              height: 50,
                              padding:const EdgeInsets.all(0),
                              color: upcoming == true?Colorconstands.neutralWhite:Colorconstands.black.withOpacity(0.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: Text("Upcoming",style: ThemeConstands.headline5_SemiBold_16.copyWith(color:upcoming == true?Colorconstands.primaryColor:Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                            ),
                          ),
                          Expanded(
                            child: MaterialButton(
                              elevation: 0,
                              onPressed: (){
                                setState(() {
                                 if(upcoming == true){
                                    upcoming = false;
                                  }
                                });
                              },
                              height: 50,
                              padding:const EdgeInsets.all(0),
                              color: upcoming == false?Colorconstands.neutralWhite:Colorconstands.black.withOpacity(0.0),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: Text("Past",style: ThemeConstands.headline5_SemiBold_16.copyWith(color:upcoming == false?Colorconstands.primaryColor:Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: appAppointmentLoading == true?
                      const Center(child: CircularProgressIndicator(),) 
                      :NotificationListener(
                        child: upcoming?Container(
                          margin: EdgeInsets.only(top:dataUpcoming!.isEmpty?0:16),
                          child: dataUpcoming.isEmpty? 
                          EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "")
                          :ListView.builder(
                            controller: scrollController,
                            shrinkWrap: true,
                            physics:const ClampingScrollPhysics(),
                            itemCount:dataUpcoming.length,
                            padding:const EdgeInsets.all(0),
                            itemBuilder: (context, index) {
                              return Container(
                                margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 9),
                                child: CardAppointments(
                                  onPressedIcon: (){
                                    print("fsadf${dataUpcoming[index].patientAppointmentId}");
                                    BookAppointmentApi().deleteAppointment(appointmentId:dataUpcoming[index].patientAppointmentId.toString()).then((value){
                                    if(value == true){
                                      BlocProvider.of<PatientAppointmentBloc>(context).add( GetPatientAppointment());
                                    }
                                    else{
                                    }
                                  });
                                  },
                                  onPressed: (){},
                                  type:dataUpcoming![index].appointmentTypeData!.nameLatin! ,
                                  name: "${dataUpcoming![index].doctorData!.title!} ${dataUpcoming![index].doctorData!.fullnameLatin!}",
                                  discription:dataUpcoming![index].note!,
                                  date: "${dataUpcoming![index].appointmentDate!} ${dataUpcoming![index].bookingTimeStr!}" ,
                                  isDisplay: dataUpcoming![index].status==0?true:false,
                                  profilePic: dataUpcoming![index].doctorData!.profile!.fileDisplay!,
                                ),
                              );
                            },
                          ),
                        ):Container(
                          margin: EdgeInsets.only(top:datapast!.isEmpty?0:16),
                          child: datapast.isEmpty? 
                          EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "")
                          :ListView.builder(
                            controller: scrollController,
                            shrinkWrap: true,
                            physics:const ClampingScrollPhysics(),
                            itemCount:datapast.length,
                            padding:const EdgeInsets.all(0),
                            itemBuilder: (context, index) {
                              return Container(
                                margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 9),
                                child: CardAppointments(
                                  onPressedIcon: (){},
                                  onPressed: (){},
                                  type:datapast![index].appointmentTypeData!.nameLatin! ,
                                  name: "${datapast![index].doctorData!.title!} ${datapast![index].doctorData!.fullnameLatin!}",
                                  discription:datapast![index].note!,
                                  date: "${datapast![index].appointmentDate!} ${datapast![index].bookingTimeStr!}" ,
                                  isDisplay: datapast![index].status==0?true:false,
                                  profilePic: datapast![index].doctorData!.profile!.fileDisplay!,
                                ),
                              );
                            },
                          ),
                        ),
                        onNotification: (t) {
                            if(isExtended == true){
                              scrollController.position.isScrollingNotifier.addListener(() { 
                                if(!scrollController.position.isScrollingNotifier.value) {
                                  setState(() {
                                    isExtended = true;
                                  });
                                  
                                } else {
                                  setState(() {
                                    isExtended = false;
                                  });
                                
                                }
                              });
                            }
                            if (t is ScrollEndNotification) {
                              if (scrollController.position.pixels > 0 &&
                                  scrollController.position.atEdge) {
                                setState(() {
                                  isExtended = false;
                                });
                              }
                              return true;
                            } else {
                              return false;
                            }
                          },
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }
  Widget appointMentType(BuildContext context, String translate,) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height/2,
            ),
            child: Column(
              children: [
                Container(
                  width: 100,
                  height: 5,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(1),
                    color:const Color.fromRGBO(0, 0, 0, 0.1),
                  ),
                ),
                const SizedBox(height: 10,),
                Row(
                  children: [
                    const SizedBox(width: 20,),
                    IconButton(
                      padding:const EdgeInsets.all(0),
                      onPressed: (){
                        Navigator.of(context).pop();
                      }, 
                      icon:const Icon(Icons.close,size: 25,color: Colorconstands.neutralDarkGrey,)
                    ),
                    Expanded(
                      child: Text(
                        "Choose appointment type",
                        style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                      ),
                    ),
                    Container(width: 65,),
                    
                  ],
                ),
                Container(
                  padding:const EdgeInsets.symmetric(horizontal: 18),
                  margin:const EdgeInsets.only(top: 10),
                  child: BlocBuilder<BookAppointmentBloc, BookAppointmentState>(
                    builder: (context, state) {
                      if(state is AppointmentTypeLoading){
                        return const Center( child: CircularProgressIndicator(),);
                      }
                      else if(state is AppointmentTypeLoaded){
                        var dataAppType = state.appointMentType.message;
                        return dataAppType!.isEmpty? 
                          EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,)
                          :ListView.builder(
                            physics:const NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            shrinkWrap: true,
                            itemCount: dataAppType.length,
                            itemBuilder:(context, index) {
                              return Container(
                                margin:const EdgeInsets.symmetric(vertical: 4),
                                child: MaterialButton(
                                  padding:const EdgeInsets.all(0),
                                  onPressed: () {
                                    setState(() {
                                      appointmentType = dataAppType[index].id.toString();
                                    },);
                                    showModalBottomSheet(
                                      enableDrag: false,
                                      isDismissible: true,
                                      backgroundColor: Colors.transparent,
                                      isScrollControlled: true,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(16),
                                            topRight: Radius.circular(16)),
                                      ),
                                      context: context,
                                      builder: (context) {
                                        return department(context,translate);
                                      }
                                    );
                                  },
                                  height: 45,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side:const BorderSide(color: Colorconstands.black)
                                  ),
                                  child: Text( translate == "km"?dataAppType[index].name.toString():dataAppType[index].nameLatin.toString(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,),
                                ),
                              );
                            }
                          );
                      }
                      else{
                        return EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,);
                      }
                    },
                  ),
                ),
                Container(height: 28,)
              ],
            ),
          ),
        ),
          
      );
    },);                                                                                
  }

  Widget department(BuildContext context, String translate,) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height/2,
            ),
            child: Column(
              children: [
                Container(
                  width: 100,
                  height: 5,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(1),
                    color:const Color.fromRGBO(0, 0, 0, 0.1),
                  ),
                ),
                const SizedBox(height: 10,),
                Row(
                  children: [
                    const SizedBox(width: 20,),
                    IconButton(
                      padding:const EdgeInsets.all(0),
                      onPressed: (){
                        Navigator.of(context).pop();
                      }, 
                      icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 25,color: Colorconstands.neutralDarkGrey,)
                    ),
                    Expanded(
                      child: Text(
                        "Choose department",
                        style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                      ),
                    ),
                    Container(width: 65,),
                    
                  ],
                ),
                Container(
                  padding:const EdgeInsets.symmetric(horizontal: 18),
                  margin:const EdgeInsets.only(top: 10),
                  child: BlocBuilder<DepartmentBloc, BookAppointmentState>(
                    builder: (context, state) {
                      if(state is DepartmentEventLoading){
                        return const Center( child: CircularProgressIndicator(),);
                      }
                      else if(state is DepartmentEventLoaded){
                        var dataDepartment = state.department.message;
                        return dataDepartment!.isEmpty? 
                          EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,)
                          :ListView.builder(
                            physics:const NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            shrinkWrap: true,
                            itemCount: dataDepartment.length,
                            itemBuilder:(context, index) {
                              return Container(
                                margin:const EdgeInsets.symmetric(vertical: 4),
                                child: MaterialButton(
                                  padding:const EdgeInsets.all(0),
                                  onPressed: () {
                                    setState((){
                                        departmentId = dataDepartment[index].id.toString();
                                    });
                                    showModalBottomSheet(
                                      enableDrag: false,
                                      isDismissible: true,
                                      backgroundColor: Colors.transparent,
                                      isScrollControlled: true,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(16),
                                            topRight: Radius.circular(16)),
                                      ),
                                      context: context,
                                      builder: (context) {
                                        return dateTime(context,translate);
                                      }
                                    );
                                  },
                                  height: 45,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side:const BorderSide(color: Colorconstands.black)
                                  ),
                                  child: Text(translate=="km"?dataDepartment[index].name.toString():dataDepartment[index].nameLatin.toString(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,),
                                ),
                              );
                            }
                          );
                      }
                      else{
                        return EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,);
                      }
                    },
                  ),
                ),
                Container(height: 28,)
              ],
            ),
          ),
        ),
      );
    },);                                                                                                                                                             
  }

  Widget dateTime(BuildContext context, String translate,) {
    _selectedDate = DateTime.now();
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height/2,
            ),
            child: Column(
              children: [
                Container(
                  width: 100,
                  height: 5,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(1),
                    color:const Color.fromRGBO(0, 0, 0, 0.1),
                  ),
                ),
                const SizedBox(height: 10,),
                Row(
                  children: [
                    const SizedBox(width: 20,),
                    IconButton(
                      padding:const EdgeInsets.all(0),
                      onPressed: (){
                        Navigator.of(context).pop();
                      }, 
                      icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 25,color: Colorconstands.neutralDarkGrey,)
                    ),
                    Expanded(
                      child: Text(
                        "Choose Date",
                        style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                      ),
                    ),
                    Container(width: 65,),
                    
                  ],
                ),
                Container(
                  padding:const EdgeInsets.symmetric(horizontal: 18),
                  margin:const EdgeInsets.only(top: 10),
                  child: TableCalendar(
                    locale: translate,
                    firstDay: DateTime.now(),
                    lastDay: DateTime(2500),
                    focusedDay: _selectedDate!,
                    calendarFormat: CalendarFormat.month,
                    headerStyle: const HeaderStyle(
                      formatButtonShowsNext: false,
                      formatButtonVisible: false
                    ),
                    calendarStyle: CalendarStyle(
                      selectedDecoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        shape: BoxShape.circle,
                      ),
                      todayDecoration: BoxDecoration(
                        color: Colors.grey[300],
                        shape: BoxShape.circle,
                      ),
                    ),
                    selectedDayPredicate: (date) {
                      return isSameDay(_selectedDate, date);
                    },
                    onDaySelected: (date, focusedDay) {
                      setState(() {
                        _selectedDate = date;
                      });
                      BlocProvider.of<DepartmentDoctorBloc>(context).add(GetDepartmentDoctor(departmentId: departmentId));
                      showModalBottomSheet(
                        enableDrag: false,
                        isDismissible: true,
                        backgroundColor: Colors.transparent,
                        isScrollControlled: true,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16),
                              topRight: Radius.circular(16)),
                        ),
                        context: context,
                        builder: (context) {
                          return doctorDepartment(context,translate);
                        }
                      );
                    },
                    calendarBuilders: CalendarBuilders(
                      selectedBuilder: (context, date, _) => Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          shape: BoxShape.circle,
                        ),
                        child: Center(
                          child: Text(
                            date.day.toString(),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(height: 28,)
              ],
            ),
          ),
        ),
      );
    },);                                                                                                                                                             
  }
  Widget doctorDepartment(BuildContext context, String translate,) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height/2,
            ),
            child: Column(
              children: [
                Container(
                  width: 100,
                  height: 5,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(1),
                    color:const Color.fromRGBO(0, 0, 0, 0.1),
                  ),
                ),
                const SizedBox(height: 10,),
                Row(
                  children: [
                    const SizedBox(width: 20,),
                    IconButton(
                      padding:const EdgeInsets.all(0),
                      onPressed: (){
                        Navigator.of(context).pop();
                      }, 
                      icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 25,color: Colorconstands.neutralDarkGrey,)
                    ),
                    Expanded(
                      child: Text(
                        "Choose Doctor",
                        style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                      ),
                    ),
                    Container(width: 65,),
                    
                  ],
                ),
                Container(
                  padding:const EdgeInsets.symmetric(horizontal: 18),
                  margin:const EdgeInsets.only(top: 10),
                  child: BlocBuilder<DepartmentDoctorBloc, BookAppointmentState>(
                    builder: (context, state) {
                      if(state is DepartmentDoctorLoading){
                        return const Center( child: CircularProgressIndicator(),);
                      }
                      else if(state is DepartmentDoctorLoaded){
                        var dataDoctor =  state.departmentDoctor.data!;
                        return dataDoctor.isEmpty? 
                          EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,)
                          :ListView.builder(
                            physics:const NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            shrinkWrap: true,
                            itemCount: dataDoctor.length,
                            itemBuilder:(context, indexDoctor) {
                              return Container(
                                margin:const EdgeInsets.symmetric(vertical: 4),
                                child: MaterialButton(
                                  padding:const EdgeInsets.all(0),
                                  onPressed: () {
                                    setState(() {
                                       doctorId = dataDoctor[indexDoctor].employeeId.toString();
                                    },);
                                    BlocProvider.of<DoctorTimeBloc>(context).add(GetTimeDoctor(date: _selectedDate.toString().substring(0,10),idDoctor: doctorId));
                                    showModalBottomSheet(
                                      enableDrag: false,
                                      isDismissible: true,
                                      backgroundColor: Colors.transparent,
                                      isScrollControlled: true,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(16),
                                            topRight: Radius.circular(16)),
                                      ),
                                      context: context,
                                      builder: (context) {
                                        return timeDoctor(context,translate);
                                      }
                                    );
                                  },
                                  height: 45,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side:const BorderSide(color: Colorconstands.black)
                                  ),
                                  child: Text(translate=="km"?"${dataDoctor[indexDoctor].title.toString()} ${dataDoctor[indexDoctor].lastname.toString()} ${dataDoctor[indexDoctor].firstname.toString()}":"${dataDoctor[indexDoctor].titleLatin.toString()} ${dataDoctor[indexDoctor].lastnameLatin.toString()} ${dataDoctor[indexDoctor].firstnameLatin.toString()}",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,),
                                ),
                              );
                            }
                          );
                      }
                      else{
                        return EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,);
                      }
                    },
                  ),
                ),
                Container(height: 28,)
              ],
            ),
          ),
        ),
      );
    },);                                                                                                                                                             
  }
  Widget timeDoctor(BuildContext context, String translate,) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height/2,
            ),
            child: Column(
              children: [
                Container(
                  width: 100,
                  height: 5,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(1),
                    color:const Color.fromRGBO(0, 0, 0, 0.1),
                  ),
                ),
                const SizedBox(height: 10,),
                Row(
                  children: [
                    const SizedBox(width: 20,),
                    IconButton(
                      padding:const EdgeInsets.all(0),
                      onPressed: (){
                        Navigator.of(context).pop();
                      }, 
                      icon:const Icon(Icons.arrow_back_ios_new_rounded,size: 25,color: Colorconstands.neutralDarkGrey,)
                    ),
                    Expanded(
                      child: Text(
                        "Choose Time",
                        style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                      ),
                    ),
                    Container(width: 65,),
                    
                  ],
                ),
                Container(
                  padding:const EdgeInsets.symmetric(horizontal: 18),
                  margin:const EdgeInsets.only(top: 10),
                  child: BlocBuilder<DoctorTimeBloc, BookAppointmentState>(
                    builder: (context, state) {
                      if(state is TimeDoctorEventLoading){
                        return const Center( child: CircularProgressIndicator(),);
                      }
                      else if(state is TimeDoctorEventLoaded){
                        var dataTime = state.timeDoctor.data!.availableTime;
                        return dataTime!.isEmpty? 
                          EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,)
                          :ListView.builder(
                            physics:const NeverScrollableScrollPhysics(),
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            shrinkWrap: true,
                            itemCount: dataTime.length,
                            itemBuilder:(context, index) {
                              return Container(
                                margin:const EdgeInsets.symmetric(vertical: 4),
                                child: MaterialButton(
                                  padding:const EdgeInsets.all(0),
                                  onPressed: () {
                                    setState(() {
                                      tiemBookAppointment = dataTime[index].timeStr.toString();
                                    },);
                                    _displayTextInputDialog(context);
                                  },
                                  height: 45,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side:const BorderSide(color: Colorconstands.black)
                                  ),
                                  child: Text(dataTime[index].timeStr.toString(),style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,),
                                ),
                              );
                            }
                          );
                      }
                      else{
                        return EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "",colorTitle: Colorconstands.black,);
                      }
                    },
                  ),
                ),
                Container(height: 28,)
              ],
            ),
          ),
        ),
      );
    },);                                                                                                                                                             
  }

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            titleTextStyle: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.primaryColor),
            title: const Text('Note book appointment'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                });
              },
              controller: _textFieldController,
              decoration:
                  const InputDecoration(hintText: "Note ..."),
            ),
            actions: <Widget>[
             
              MaterialButton(
                color: Colorconstands.primaryColor,
                textColor: Colors.white,
                child: const Text('Finish',style: ThemeConstands.headline6_SemiBold_14,),
                onPressed: () {
                  setState(() {
                    BookAppointmentApi().addAppointment(departmentId: departmentId,time: tiemBookAppointment,appointmentTypeId: appointmentType,status: "0",note: _textFieldController.text,appointDate: _selectedDate.toString().substring(0,10),employeeId: doctorId,patientVisitId:widget.patientId).then((value){
                      if(value == true){
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        BlocProvider.of<PatientAppointmentBloc>(context).add( GetPatientAppointment());
                      }
                      else{
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      }
                    });
                  });
                },
              ),
            ],
          );
        });
  }
}