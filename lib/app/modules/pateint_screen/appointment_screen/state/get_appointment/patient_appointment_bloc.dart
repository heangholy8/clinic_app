import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/appiontment_model/pateint_appiontment_model.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../service/apis/pateint_api/all_menu_api/menu_api.dart';

part 'patient_appointment_event.dart';
part 'patient_appointment_state.dart';

class PatientAppointmentBloc extends Bloc<PatientAppointmentEvent, PatientAppointmentState> {
  GetMenuFeatureApi doctorProfilePatient;
  PatientAppointmentBloc({required this.doctorProfilePatient}) : super(PatientAppointmentInitial()) {
    on<GetPatientAppointment>((event, emit) async{
      emit(PatientAppointmentLoading());
        try {
          var doctorData = await doctorProfilePatient.getAppointmentApi();
          emit(PatientAppointmentLoaded(patientAppointment: doctorData));
        } catch (e) {
          emit(PatientAppointmentError());
        }
    });
  }
}
