part of 'patient_appointment_bloc.dart';

class PatientAppointmentEvent extends Equatable {
  const PatientAppointmentEvent();

  @override
  List<Object> get props => [];
}

class GetPatientAppointment extends PatientAppointmentEvent{}