part of 'patient_appointment_bloc.dart';

class PatientAppointmentState extends Equatable {
  const PatientAppointmentState();
  
  @override
  List<Object> get props => [];
}

class PatientAppointmentInitial extends PatientAppointmentState {}

class PatientAppointmentLoading extends PatientAppointmentState {}
class PatientAppointmentLoaded extends PatientAppointmentState {
  final PatientAppointmentModel patientAppointment;
  const PatientAppointmentLoaded({required this.patientAppointment});
}
class PatientAppointmentError extends PatientAppointmentState {}

