part of 'book_appointment_bloc.dart';

sealed class BookAppointmentEvent extends Equatable {
  const BookAppointmentEvent();

  @override
  List<Object> get props => [];
}

class GetAppointmentType extends BookAppointmentEvent{}

class GetDepartment extends BookAppointmentEvent{}

class GetDepartmentDoctor extends BookAppointmentEvent{
  final String departmentId;
  const GetDepartmentDoctor({required this.departmentId});
}
class GetTimeDoctor extends BookAppointmentEvent{
  final String date;
  final String idDoctor;
  const GetTimeDoctor({required this.date,required this.idDoctor});
}