part of 'book_appointment_bloc.dart';

sealed class BookAppointmentState extends Equatable {
  const BookAppointmentState();
  
  @override
  List<Object> get props => [];
}

final class BookAppointmentInitial extends BookAppointmentState {}

final class TimeDoctorEventLoading extends BookAppointmentState {}
final class DepartmentEventLoading extends BookAppointmentState {}
final class AppointmentTypeLoading extends BookAppointmentState {}
final class DepartmentDoctorLoading extends BookAppointmentState {}


final class TimeDoctorEventLoaded extends BookAppointmentState {
  final TimeDoctorModel timeDoctor;
  const TimeDoctorEventLoaded({required this.timeDoctor});
}
final class DepartmentEventLoaded extends BookAppointmentState {
  final DepartmentModel department;
  const DepartmentEventLoaded({required this.department});
}
final class AppointmentTypeLoaded extends BookAppointmentState {
  final AppointMentTypeModel appointMentType;
  const AppointmentTypeLoaded({required this.appointMentType});
}
final class DepartmentDoctorLoaded extends BookAppointmentState {
  final DepartmentDoctorModel departmentDoctor;
  const DepartmentDoctorLoaded({required this.departmentDoctor});
}

final class TimeDoctorEventError extends BookAppointmentState {}
final class DepartmentEventError extends BookAppointmentState {}
final class AppointmentTypeError extends BookAppointmentState {}
final class DepartmentDoctorError extends BookAppointmentState {}
