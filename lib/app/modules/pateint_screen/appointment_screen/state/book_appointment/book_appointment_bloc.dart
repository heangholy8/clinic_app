import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/appointment_type_model.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/department_doctor.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/department_model.dart';
import 'package:clinic_application/model/pateint_model/appointment_model/time_doctor_model.dart';
import 'package:clinic_application/service/apis/pateint_api/appointment_api/book_appointment_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
part 'book_appointment_event.dart';
part 'book_appointment_state.dart';

class BookAppointmentBloc extends Bloc<BookAppointmentEvent, BookAppointmentState> {
  final BookAppointmentApi bookAppointmentApi;
  BookAppointmentBloc({required this.bookAppointmentApi}) : super(BookAppointmentInitial()) {
    on<GetAppointmentType>((event, emit) async {
       emit(AppointmentTypeLoading());
       try{
         var dataAppType = await bookAppointmentApi.getAppointmentTypeApi();
         emit(AppointmentTypeLoaded(appointMentType: dataAppType));
       }catch(e){
        debugPrint("$e");
        emit(AppointmentTypeError());
       }
    });
  }
}

class DepartmentBloc extends Bloc<BookAppointmentEvent, BookAppointmentState> {
  final BookAppointmentApi bookAppointmentApi;
  DepartmentBloc({required this.bookAppointmentApi}) : super(BookAppointmentInitial()) {
    on<GetDepartment>((event, emit) async {
       emit(DepartmentEventLoading());
       try{
         var dataDepartment = await bookAppointmentApi.getDepartmentApi();
         emit(DepartmentEventLoaded(department: dataDepartment));
       }catch(e){
        debugPrint("$e");
        emit(DepartmentEventError());
       }
    });
  }
}

class DepartmentDoctorBloc extends Bloc<BookAppointmentEvent, BookAppointmentState> {
  final BookAppointmentApi bookAppointmentApi;
  DepartmentDoctorBloc({required this.bookAppointmentApi}) : super(BookAppointmentInitial()) {
    on<GetDepartmentDoctor>((event, emit) async {
       emit(DepartmentDoctorLoading());
       try{
         var dataDepartment = await bookAppointmentApi.getDepartmentDoctorApi(
           departmentId: event.departmentId
         );
         emit(DepartmentDoctorLoaded(departmentDoctor: dataDepartment));
       }catch(e){
        debugPrint("$e");
        emit(DepartmentDoctorError());
       }
    });
  }
}

class DoctorTimeBloc extends Bloc<BookAppointmentEvent, BookAppointmentState> {
  final BookAppointmentApi bookAppointmentApi;
  DoctorTimeBloc({required this.bookAppointmentApi}) : super(BookAppointmentInitial()) {
    on<GetTimeDoctor>((event, emit) async {
       emit(TimeDoctorEventLoading());
       try{
         var dataTimeDoctor = await bookAppointmentApi.getTimeDoctorApi(
          doctorId: event.idDoctor, appointDate: event.date,
         );
         emit(TimeDoctorEventLoaded(timeDoctor: dataTimeDoctor));
       }catch(e){
        debugPrint("$e");
        emit(TimeDoctorEventError());
       }
    });
  }
}
