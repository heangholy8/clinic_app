import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:flutter/material.dart';

class IconBack extends StatelessWidget {
  double hightButton;
  double radiusButton;
  VoidCallback onPressed;
  IconBack({Key? key,required this.hightButton,required this.radiusButton,required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding:const EdgeInsets.all(0),
      onPressed: onPressed,
      height: hightButton,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radiusButton)
      ),
      color: Colorconstands.neutralWhite,
      child:const Icon(Icons.arrow_back_ios_new_rounded,size: 22,color: Colorconstands.primaryColor,),
    );
  }
}