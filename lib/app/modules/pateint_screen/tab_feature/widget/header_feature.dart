import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/icon_back.dart';
import 'package:flutter/material.dart';

class HeaderFeature extends StatefulWidget {
  String titleHeader;
  HeaderFeature({Key? key,required this.titleHeader}) : super(key: key);

  @override
  State<HeaderFeature> createState() => _HeaderFeatureState();
}

class _HeaderFeatureState extends State<HeaderFeature> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.symmetric(horizontal: 28),
      child: Row(
        children: [
          Container(
            width: 30,
            child: IconBack(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              hightButton: 30,
              radiusButton: 6,
              
            ),
          ),
          const SizedBox(width: 16,),
          Expanded(
            child: Text(widget.titleHeader,style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,),
          )
        ],
      ),
    );
  }
}