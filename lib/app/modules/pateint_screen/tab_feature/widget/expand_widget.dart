import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/widgets/expansion/expand_section.dart';
import 'package:flutter/material.dart';

class expandCustom extends StatefulWidget {
  final String titleHead;
  bool isExpand;
  bool isClick;
  Widget widget;
  Icon iconLeft;
  Color colorButtonExpand;
  VoidCallback onPressed;
  expandCustom({
    Key? key,
    required this.isClick,
    required this.titleHead,
    required this.colorButtonExpand,
    required this.widget,
    required this.isExpand,
    required this.iconLeft,
    required this.onPressed,
  }) : super(key: key);

  @override
  State<expandCustom> createState() => _expandCustomState();
}

class _expandCustomState extends State<expandCustom>
    with TickerProviderStateMixin {
  late Animation _arrowAnimation;
  late AnimationController _arrowAnimationController;
  final GlobalKey expansionTileKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    _arrowAnimation =
        Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController);
  }

  void _scrollToSelectedContent({required GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(const Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: const Duration(milliseconds: 200));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: expansionTileKey,
      decoration: BoxDecoration(
          color: widget.colorButtonExpand,
          borderRadius: BorderRadius.circular(30.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MaterialButton(
            height: 60,
            highlightColor: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            padding: const EdgeInsets.all(0),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
              ),
            ),
            onPressed: widget.isClick == true? widget.onPressed:(){},
            child: Container(
              padding: const EdgeInsets.only(
                  left: 18, top: 12, bottom: 12, right: 18),
              child: Row(
                children: [
                  Container(
                    margin:const EdgeInsets.only(right: 18),
                    child: widget.iconLeft,
                  ),
                  Expanded(
                    child: Text(
                      widget.titleHead,
                      style:ThemeConstands.headline5_SemiBold_16.copyWith(
                              color: Colorconstands.black,),textAlign: TextAlign.left,
                    ),
                  ),
                   widget.isClick == true?Container(
                    width: 25,
                    decoration:BoxDecoration(
                      border: Border.all(color: Colorconstands.neutralDarkGrey,width: 2.5),
                      shape: BoxShape.circle,
                    ),
                    child:Align(
                      alignment: Alignment.center,
                      child: Icon(
                            widget.isExpand == false?Icons.expand_more_outlined:Icons.expand_less_outlined,
                            size: 20.0,color: Colorconstands.black,
                          ),
                    ),
                  ):Container(width: 25,),
                ],
              ),
            ),
          ),
          AnimatedCrossFade(
            firstChild: Container(height: 0, color: Colorconstands.neutralGrey),
            secondChild: const SizedBox(),
            crossFadeState: widget.isExpand
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            duration: const Duration(milliseconds: 400),
          ),
          ExpandedSection(expand: widget.isExpand, child: widget.widget),
        ],
      ),
    );
  }
}
