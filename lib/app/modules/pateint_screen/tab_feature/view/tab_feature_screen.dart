import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/modules/pateint_screen/investigation_screen/view/linvestigation_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/medical_screen/state/medical_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/medical_screen/view/medical_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/physical_screen/state/bloc/physical_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/physical_screen/view/physical_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/prescription_screen/state/prescription_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/prescription_screen/view/prescription_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/vaccination_screen/state/vaccination_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/vaccination_screen/view/vaccination_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/vital_sing_screen/state/vitalsing_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/vital_sing_screen/view/vital_sing_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../investigation_screen/state/investigation_history_bloc.dart';

class TabFeature extends StatefulWidget {
  final String patientAge;
  final String patientGender;
  int isActive;
  
  TabFeature({Key? key,required this.isActive,required this.patientAge,required this.patientGender}) : super(key: key);

  @override
  State<TabFeature> createState() => _TabFeatureState();
}

class _TabFeatureState extends State<TabFeature> {

  @override
  void initState() {
    BlocProvider.of<VitalsingBloc>(context).add(GetVitalSingEvent());
    BlocProvider.of<PrescriptionHistoryBloc>(context).add(GetPriscriptionEvent());
    BlocProvider.of<VaccinationHistoryBloc>(context).add(GetVaccinationEvent());
    BlocProvider.of<InvestigationHistoryBloc>(context).add(GetInvestigationHistoryEvent());
    BlocProvider.of<MedicalHistoryBloc>(context).add(GetMedicalHistoryEvent());
    BlocProvider.of<PhysicalHistoryBloc>(context).add(GetPhysicalHistory());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
               Expanded(
                child:widget.isActive == 1? VitalSingScreen(patientAge: widget.patientAge,patientGender: widget.patientGender,):widget.isActive == 2?const MedicalScreen(): widget.isActive == 3? const VaccinationScreen():widget.isActive == 4?const PhysicalScreen():widget.isActive == 5? const PrescriptionScreen():const InvestigationScreen()
              ),
              Container(
                padding:const EdgeInsets.only(bottom: 18),
                decoration:const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colorconstands.primaryColor,Color(0xFF0C7376), Color(0xFF053031),])),
                child: Container(
                  margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 12),
                  child: Container(
                    padding:const EdgeInsets.symmetric(horizontal: 8,vertical: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(35),
                      color: Colorconstands.neutralWhite,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: MaterialButton(
                            height: 55,
                            elevation: 0,
                            onPressed: () {
                              if(widget.isActive !=1){
                                setState(() {
                                  widget.isActive =1;
                                });
                              }
                            },
                            padding:const EdgeInsets.all(5),
                            shape:const CircleBorder(),
                            color: widget.isActive == 1?Colorconstands.primaryColor:Colorconstands.neutralWhite,
                            child: SvgPicture.asset(ImageAssets.vital_sing,color: widget.isActive == 1? Colorconstands.neutralWhite:Colorconstands.primaryColor,width: 35,),
                          ),
                        ),
                        Expanded(
                          child: MaterialButton(
                            height: 55,
                            elevation: 0,
                            onPressed: () {
                              if(widget.isActive !=2){
                                setState(() {
                                  widget.isActive =2;
                                });
                              }
                            },
                            padding:const EdgeInsets.all(0),
                            shape:const CircleBorder(),
                            color: widget.isActive == 2?Colorconstands.primaryColor:Colorconstands.neutralWhite,
                            child: SvgPicture.asset(ImageAssets.medical,color: widget.isActive == 2? Colorconstands.neutralWhite:Colorconstands.primaryColor,width: 35,),
                          ),
                        ),
                        Expanded(
                          child: MaterialButton(
                            height: 55,
                            elevation: 0,
                            onPressed: () {
                              if(widget.isActive !=3){
                                setState(() {
                                  widget.isActive =3;
                                });
                              }
                            },
                            padding:const EdgeInsets.all(5),
                            shape:const CircleBorder(),
                            color: widget.isActive == 3?Colorconstands.primaryColor:Colorconstands.neutralWhite,
                            child: SvgPicture.asset(ImageAssets.vaccination,color: widget.isActive == 3? Colorconstands.neutralWhite:Colorconstands.primaryColor,width: 35,),
                          ),
                        ),
                        Expanded(
                          child: MaterialButton(
                            height: 55,
                            elevation: 0,
                            onPressed: () {
                              if(widget.isActive !=4){
                                setState(() {
                                  widget.isActive =4;
                                });
                              }
                            },
                            padding:const EdgeInsets.all(5),
                            shape:const CircleBorder(),
                            color: widget.isActive == 4?Colorconstands.primaryColor:Colorconstands.neutralWhite,
                            child: SvgPicture.asset(ImageAssets.pysical,color: widget.isActive == 4? Colorconstands.neutralWhite:Colorconstands.primaryColor,width: 35,),
                          ),
                        ),
                        Expanded(
                          child: MaterialButton(
                            height: 55,
                            elevation: 0,
                            onPressed: () {
                              if(widget.isActive !=5){
                                setState(() {
                                  widget.isActive =5;
                                });
                              }
                            },
                            padding:const EdgeInsets.all(5),
                            shape:const CircleBorder(),
                            color: widget.isActive == 5?Colorconstands.primaryColor:Colorconstands.neutralWhite,
                            child: SvgPicture.asset(ImageAssets.prescription,color: widget.isActive == 5? Colorconstands.neutralWhite:Colorconstands.primaryColor,width: 35,),
                          ),
                        ),
                        Expanded(
                          child: MaterialButton(
                            height: 55,
                            elevation: 0,
                            onPressed: () {
                              if(widget.isActive !=6){
                                setState(() {
                                  widget.isActive =6;
                                });
                              }
                            },
                            padding:const EdgeInsets.all(5),
                            shape:const CircleBorder(),
                            color: widget.isActive == 6?Colorconstands.primaryColor:Colorconstands.neutralWhite,
                            child: SvgPicture.asset(ImageAssets.laboratory,color: widget.isActive == 6? Colorconstands.neutralWhite:Colorconstands.primaryColor,width: 25,),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}