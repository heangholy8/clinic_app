import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/expand_widget.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/app/modules/pateint_screen/vaccination_screen/state/vaccination_history_bloc.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VaccinationScreen extends StatefulWidget {
  const VaccinationScreen({Key? key}) : super(key: key);

  @override
  State<VaccinationScreen> createState() => _VaccinationScreenState();
}

class _VaccinationScreenState extends State<VaccinationScreen> {
  int indexFile = 0;
  bool disbleExpand = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HeaderFeature(
          titleHeader: "Vaccine History",
        ),
        Expanded(
          child: Container(
            margin:const EdgeInsets.only(top: 16),
            child: BlocBuilder<VaccinationHistoryBloc, VaccinationHistoryState>(
              builder: (context, state) {
                if(state is VaccinationHistoryLoading ){
                  return const Center(
                    child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                  );
                }
                else if(state is VaccinationHistoryLoaded){
                  var Vaccination = state.vaccination.data;
                  return Vaccination!.isEmpty?EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "") 
                  : ListView.builder(
                    shrinkWrap: true,
                    physics:const ClampingScrollPhysics(),
                    itemCount: Vaccination!.length,
                    padding:const EdgeInsets.all(0),
                    itemBuilder: (context, index) {
                      if(Vaccination.isNotEmpty && disbleExpand == false){
                        Vaccination[indexFile].isExpand = true;
                      }
                      return Container(
                        margin:const EdgeInsets.only(left: 28,right: 28,top: 12),
                        child: expandCustom(
                           onPressed: (){
                            if(Vaccination[index].isExpand == true){
                              setState(() {
                                Vaccination[index].isExpand = false;
                                disbleExpand = true;
                              });
                            }
                            else{
                              for (var file in Vaccination) {
                                setState(() {
                                  file.isExpand = false;
                                  indexFile = index;
                                });
                              }
                              setState(() {
                                Vaccination[index].isExpand = true;
                              });
                            }
                          },
                          isClick: Vaccination[index].isCompleted == 0?false:true,
                          colorButtonExpand: Colorconstands.exam,
                          titleHead: Vaccination[index].vaccinationSheetData!.name.toString(),
                          isExpand: Vaccination[index].isCompleted == 0?false:Vaccination[index].isExpand,
                          iconLeft: Icon( Vaccination[index].isCompleted == 1? Icons.check_circle_rounded:Icons.cancel,size: 30,color: Vaccination[index].isCompleted == 1? Colorconstands.neutralDarkGrey:Colorconstands.errorColor,),
                          widget: Container(
                            decoration:const BoxDecoration(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                              color: Colorconstands.neutralWhite,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Date:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(Vaccination[index].vaccinationDate.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Product Name",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(Vaccination[index].vaccinationSheetData!.name.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("LOT Number:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(Vaccination[index].lotNumber.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Doctor:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(Vaccination[index].administratorData!.fullname.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        // const SizedBox(height: 10,),
                                        // Column(
                                        //   crossAxisAlignment: CrossAxisAlignment.start,
                                        //   children: [
                                        //     Text("Hospital/Clinic::",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                        //     Text(Vaccination[index].hospitalClinic,style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                        //   ],
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                else{
                  return const EmptyWidget(icon: Icon(Icons.error,size:28,color:Colorconstands.errorColor), title: "Empty", subtitle: "");
                }
              },
            ),
          ),
        )
      ],
    );
  }
}