part of 'vaccination_history_bloc.dart';

sealed class VaccinationHistoryState extends Equatable {
  const VaccinationHistoryState();
  
  @override
  List<Object> get props => [];
}

final class VaccinationHistoryInitial extends VaccinationHistoryState {}

final class VaccinationHistoryLoading extends VaccinationHistoryState {}

final class VaccinationHistoryLoaded extends VaccinationHistoryState {
  final VaccinationModel vaccination;
  const VaccinationHistoryLoaded({required this.vaccination});
}

final class VaccinationHistoryError extends VaccinationHistoryState {}
