import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/feature_model/vaccination_model.dart';
import 'package:clinic_application/service/apis/pateint_api/all_menu_api/menu_api.dart';
import 'package:equatable/equatable.dart';
part 'vaccination_history_event.dart';
part 'vaccination_history_state.dart';

class VaccinationHistoryBloc extends Bloc<VaccinationHistoryEvent, VaccinationHistoryState> {
  final GetMenuFeatureApi getMenuFeature;
  VaccinationHistoryBloc({required this.getMenuFeature}) : super(VaccinationHistoryInitial()) {
    on<GetVaccinationEvent>((event, emit) async {
      emit(VaccinationHistoryLoading());
      try{
        var data = await getMenuFeature.getVaccinationApi();
        emit(VaccinationHistoryLoaded(vaccination: data));
      }catch (e){
        emit(VaccinationHistoryError());
      }
    });
  }
}
