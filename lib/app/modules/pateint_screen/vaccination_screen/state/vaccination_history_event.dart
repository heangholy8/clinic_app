part of 'vaccination_history_bloc.dart';

sealed class VaccinationHistoryEvent extends Equatable {
  const VaccinationHistoryEvent();

  @override
  List<Object> get props => [];
}

class GetVaccinationEvent extends VaccinationHistoryEvent{}
