import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/edit_profile_screen/view/edit_profile_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/profile_screen.dart/state/bloc/info_patient_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/profile_screen.dart/widget/card_profile_feature.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/icon_back.dart';
import 'package:clinic_application/app/modules/setting_screen/view/setting_screen.dart';
import 'package:clinic_application/mixins/toast.dart';
import 'package:clinic_application/storages/remove_storage.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with Toast{
  final RemoveStoragePref _removeStorage = RemoveStoragePref();
  String nameUser = "-----";
  String blood = "--";
  String lastName = "-----";
  String firstName = "-----";
  String profilePicture = "https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-image-182145777.jpg";

  @override
  void initState() {
     BlocProvider.of<InfoPatientBloc>(context).add(GetInfoPatientEvent());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
     var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              BlocListener<InfoPatientBloc, InfoPatientState>(
                listener: (context, state) {
                  if(state is InfoPatientLoaded){
                    var dataProfile = state.infoPateint.data;
                    setState(() {
                      lastName = translate == "km"?dataProfile!.lastname.toString():dataProfile!.lastnameLatin.toString();
                      firstName = translate == "km"?dataProfile.firstname.toString():dataProfile.firstnameLatin.toString();
                      nameUser =  translate == "km"?"${dataProfile.lastname.toString()} ${dataProfile.firstname.toString()}":"${dataProfile.lastnameLatin.toString()} ${dataProfile.firstnameLatin.toString()}";
                      profilePicture = dataProfile.profile == null? profilePicture:profilePicture;
                      //nameClinic = dataProfile..toString();
                      blood = dataProfile.bloodGroupTypeData!.name.toString();
                    });
                  }
                },
                child: Container(),
              ),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 28),
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Container(width: 30,height: 30,margin:const EdgeInsets.only(right: 18),child: IconBack(hightButton: 30, radiusButton: 8, onPressed: (){Navigator.of(context).pop();})),
                    Expanded(child: Text("Profile",style: ThemeConstands.headline2_SemiBold_242.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,)),
                    IconButton(
                      onPressed:(){
                        showAlertLogout(
                          onSubmit: () {
                            _removeStorage.removeToken(context);
                          },
                          title: "DO_YOU_WANT_TO_LEAVE".tr(),
                          context: context
                        );
                      }, 
                      icon: const Icon(Icons.logout_rounded,color: Colorconstands.neutralWhite,),
                    )
                  ],
                )),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 18),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  color: Colorconstands.black.withOpacity(0.2),
                ),
                child: Row(
                  children: [
                    Container(
                      margin:const EdgeInsets.only(left: 28,right: 16,top: 22,bottom: 22),
                      child: CircleAvatar(
                        radius: 30,
                        backgroundImage: NetworkImage(profilePicture),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(nameUser,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.exam),textAlign: TextAlign.left,),
                          Text("Blood type: $blood",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 28),
                alignment: Alignment.centerLeft,
                child: Text("Genaral",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,)),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    padding:const EdgeInsets.only(bottom: 18),
                    margin:const EdgeInsets.symmetric(horizontal: 28),
                    child: Column(
                      children: [
                        const SizedBox(height: 18,),
                        CardFeatureProfile(
                          onPressed: (){
                            Navigator.push(context, PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) => EditProfileUserScreen(
                                  firstName: firstName,
                                  imageProfile: profilePicture,
                                  lastName: lastName,
                                ),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                            );
                          },
                          title: "Account information",
                          subTitle: "Change your account information",
                          colorCard: Colorconstands.neutralWhite,
                          icon: ImageAssets.profileIcon,
                          borderRadius: 18,
                        ),
                        // const SizedBox(height: 18,),
                        // CardFeatureProfile(
                        //   onPressed: (){},
                        //   title: "Clinic Information",
                        //   subTitle: "Visited clinics details",
                        //   colorCard: Colorconstands.neutralWhite,
                        //   icon: ImageAssets.hospital,
                        //   borderRadius: 18,
                        // ),
                        const SizedBox(height: 18,),
                        CardFeatureProfile(
                          onPressed: (){
                            Navigator.push(context, PageRouteBuilder(
                                pageBuilder: (context, animation1, animation2) => const SettingScreen(),
                                transitionDuration: Duration.zero,
                                reverseTransitionDuration: Duration.zero,
                              ),
                            );
                          },
                          title: "Setting",
                          subTitle: "Manage & Settings",
                          colorCard: Colorconstands.neutralWhite,
                          icon: ImageAssets.settingIcons,
                          borderRadius: 18,
                        ),
                        // const SizedBox(height: 18,),
                        // CardFeatureProfile(
                        //   onPressed: (){},
                        //   title: "About VHS App",
                        //   subTitle: "",
                        //   colorCard: Colorconstands.neutralWhite,
                        //   icon: ImageAssets.informationIcons,
                        //   borderRadius: 18,
                        // ),
                      ],
                    ),
                  ),
                ),
              ),
              // Container(
              //   padding:const EdgeInsets.only(bottom: 28,top: 16),
              //   decoration:const BoxDecoration(
              //     gradient: LinearGradient(
              //         begin: Alignment.topCenter,
              //         end: Alignment.bottomCenter,
              //         colors: [Colorconstands.primaryColor,Color(0xFF0C7376), Color(0xFF053031),])),
              //   child: NavHomePage(acitiveNav: 2,imageProfile: "https://www.befunky.com/images/wp/wp-2021-01-linkedin-profile-picture-after.jpg?auto=avif,webp&format=jpg&width=944",))
            ],
          ),
        ),
      ),
    );
  }
}