part of 'info_patient_bloc.dart';

sealed class InfoPatientEvent extends Equatable {
  const InfoPatientEvent();

  @override
  List<Object> get props => [];
}

class GetInfoPatientEvent extends InfoPatientEvent{}
