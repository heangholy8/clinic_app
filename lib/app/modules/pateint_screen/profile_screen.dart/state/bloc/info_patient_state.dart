part of 'info_patient_bloc.dart';

sealed class InfoPatientState extends Equatable {
  const InfoPatientState();
  
  @override
  List<Object> get props => [];
}

final class InfoPatientInitial extends InfoPatientState {}

final class InfoPatientLoading extends InfoPatientState {}
final class InfoPatientLoaded extends InfoPatientState {
  final InfoPateintModel  infoPateint;
  const InfoPatientLoaded({required this.infoPateint});
}
final class InfoPatientError extends InfoPatientState {}
