import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/get_profile_model/info_patient_model.dart';
import 'package:clinic_application/service/apis/pateint_api/get_profile_api/get_profile_patient_api.dart';
import 'package:equatable/equatable.dart';
part 'info_patient_event.dart';
part 'info_patient_state.dart';

class InfoPatientBloc extends Bloc<InfoPatientEvent, InfoPatientState> {
  final GetPatientProfile getPatientProfile;
  InfoPatientBloc({required this.getPatientProfile}) : super(InfoPatientInitial()) {
    on<GetInfoPatientEvent>((event, emit) async {
      emit(InfoPatientLoading());
      try{
        var data = await getPatientProfile.getProfilePatientApi();
        emit(InfoPatientLoaded(infoPateint: data));
      }catch (e){
        print(e);
       emit(InfoPatientError());
      }
    });
  }
}
