import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/view/result_calculate_vital_sign.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/view/vital_sign_form_create_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/expand_widget.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/app/modules/pateint_screen/vital_sing_screen/state/vitalsing_bloc.dart';
import 'package:clinic_application/widgets/button_widget/button_widget_custom.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VitalSingScreen extends StatefulWidget {
  final String patientAge;
  final String patientGender;
  const VitalSingScreen({Key? key,required this.patientAge,required this.patientGender}) : super(key: key);

  @override
  State<VitalSingScreen> createState() => _VitalSingScreenState();
}

class _VitalSingScreenState extends State<VitalSingScreen> {
  int indexFile = 0;
  bool disbleExpand = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: HeaderFeature(
                titleHeader: "Vital sign",
              ),
            ),
            Container(
              margin:const EdgeInsets.only(right: 28),
              child: ButtonWidgetCustom(
                panddingVerButton: 3, 
                buttonColor: Colorconstands.neutralWhite,
                onTap:() {
                  Navigator.push(context,PageRouteBuilder(
                      pageBuilder: (context, animation1, animation2) => FormCreateVitalSingScreem(patient: true,patientIdVisit: "",patientGender: widget.patientGender,patientIdAge: widget.patientAge,),
                      transitionDuration: Duration.zero,
                      reverseTransitionDuration: Duration.zero,
                    ),
                  );
                },
                panddinHorButton: 12,
                radiusButton: 8,
                heightButton: 30,
                textStyleButton: ThemeConstands.headline6_SemiBold_14.copyWith(color:Colorconstands.primaryColor),
                title: 'Test Vital sign'.tr(),
              ),
            ),
          ],
        ),
        Expanded(
          child: Container(
            margin:const EdgeInsets.only(top: 16),
            child: BlocBuilder<VitalsingBloc, VitalsingState>(
              builder: (context, state) {
                if(state is VitalsingLoading ){
                  return const Center(
                    child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                  );
                }
                else if(state is VitalsingLoaded){
                  var data  =  state.vitalsing.data;
                  return ListView.builder(
                    shrinkWrap: true,
                    physics:const ClampingScrollPhysics(),
                    itemCount: data!.length,
                    padding:const EdgeInsets.all(0),
                    itemBuilder: (context, index) {
                      if(data.isNotEmpty && disbleExpand == false){
                        data[indexFile].isExpand = true;
                      }
                      return Container(
                        margin:const EdgeInsets.only(left: 28,right: 28,top: 12),
                        child: expandCustom(
                           onPressed: (){
                            if(data[index].isExpand == true){
                              setState(() {
                                data[index].isExpand = false;
                                disbleExpand = true;
                              });
                            }
                            else{
                              for (var file in data) {
                                setState(() {
                                  file.isExpand = false;
                                  indexFile = index;
                                });
                              }
                              setState(() {
                                data[index].isExpand = true;
                              });
                            }
                          },
                          isClick: true,
                          colorButtonExpand: Colorconstands.exam,
                          titleHead: data[index].data.toString(),
                          isExpand:data[index].isExpand,
                          iconLeft:const Icon(Icons.access_time_rounded,size: 30,color: Colorconstands.neutralDarkGrey,),
                          widget: Container(
                            decoration:const BoxDecoration(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                              color: Colorconstands.neutralWhite,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Chief Complaint:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].chiefComplaint.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Current Medication:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].currentMedication.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Systolic:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].systolic.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Diastolic:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].diastolic.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Respiratory Rate:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].respiratoryRate.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Spo2:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].spo2.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Pulse:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].pulse.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Temperature:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].temperature.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Glucose:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].glucose.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Height:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].height.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Weight:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].weight.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 10,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("History of",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            Text(data[index].historyOfIllness.toString(),style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,)
                                          ],
                                        ),
                                        const SizedBox(height: 16,),
                                        MaterialButton(
                                          height: 50,
                                          padding:const EdgeInsets.all(5),
                                          onPressed: (){
                                            Navigator.push(context,PageRouteBuilder(
                                              pageBuilder: (context, animation1, animation2) => ResultCalculateVitalSingScreem(
                                                weight: data[index].weight.toString(),
                                                height: data[index].height == ""?"":"${double.parse(data[index].height.toString(),)/100}",
                                                age: "23",
                                                gender: 1,
                                                systolic: data[index].systolic.toString(),
                                                diastolic: data[index].diastolic.toString(),
                                                glucose: data[index].glucose.toString(),
                                                glucoseOption: 1,
                                                temperature: data[index].temperature.toString(),
                                                spo2: data[index].spo2.toString(),
                                                respiratory: data[index].respiratoryRate.toString(),
                                                pulse: data[index].pulse.toString(),
                                                chiefComplaint: data[index].chiefComplaint.toString(),
                                                currentMedcal: data[index].currentMedication.toString(),
                                                patient: true,
                                              ),
                                              transitionDuration: Duration.zero,
                                              reverseTransitionDuration: Duration.zero,
                                            ),
                                          );
                                          },
                                          color: Colorconstands.primaryColor,
                                          shape:RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(25)
                                          ),
                                          child:Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              const Icon(Icons.remove_red_eye_outlined,color: Colorconstands.neutralWhite,size: 28,),
                                              const SizedBox(width: 8,),
                                              Text("View Detail",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                else{
                  return const EmptyWidget(icon: Icon(Icons.error,size:28,color:Colorconstands.errorColor), title: "Empty", subtitle: "");
                }
              },
            ),
          ),
        )
      ],
    );
  }
}