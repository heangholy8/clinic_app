part of 'vitalsing_bloc.dart';

sealed class VitalsingState extends Equatable {
  const VitalsingState();
  
  @override
  List<Object> get props => [];
}

final class VitalsingInitial extends VitalsingState {}

final class VitalsingLoading extends VitalsingState {}
final class VitalsingLoaded extends VitalsingState {
  final VitalSingModel vitalsing;
  const VitalsingLoaded({required this.vitalsing});
}
final class VitalsingError extends VitalsingState {}
