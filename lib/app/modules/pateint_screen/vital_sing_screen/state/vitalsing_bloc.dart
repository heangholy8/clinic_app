import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/feature_model/vital_sing_model.dart';
import 'package:clinic_application/service/apis/pateint_api/all_menu_api/menu_api.dart';
import 'package:equatable/equatable.dart';

part 'vitalsing_event.dart';
part 'vitalsing_state.dart';

class VitalsingBloc extends Bloc<VitalsingEvent, VitalsingState> {
  GetMenuFeatureApi menuFeatureApi;
  VitalsingBloc({required this.menuFeatureApi}) : super(VitalsingInitial()) {
    on<GetVitalSingEvent>((event, emit)  async{
      emit(VitalsingLoading());
      try{
        var data = await menuFeatureApi.getVitalSingApi();
        emit(VitalsingLoaded(vitalsing: data));
      }catch (e){
        emit(VitalsingError());
      }
    });
  }
}
