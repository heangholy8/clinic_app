part of 'vitalsing_bloc.dart';

sealed class VitalsingEvent extends Equatable {
  const VitalsingEvent();

  @override
  List<Object> get props => [];
}

class GetVitalSingEvent extends VitalsingEvent{}
