import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/prescription_screen/state/prescription_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/expand_widget.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/model/pateint_model/feature_model/prescription_model.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PrescriptionScreen extends StatefulWidget {
  const PrescriptionScreen({Key? key}) : super(key: key);

  @override
  State<PrescriptionScreen> createState() => _PrescriptionScreenState();
}

class _PrescriptionScreenState extends State<PrescriptionScreen> {
  int indexFile = 0;
  bool disbleExpand = false;
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Column(
      children: [
        HeaderFeature(
          titleHeader: "Prescription History",
        ),
        Expanded(
          child: Container(
            margin:const EdgeInsets.only(top: 16),
            child: BlocBuilder<PrescriptionHistoryBloc, PrescriptionHistoryState>(
              builder: (context, state) {
                if(state is PrescriptionHistoryLoading){
                  return const Center(
                    child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                  );
                }
                else if(state is PrescriptionHistoryLoaded){
                  var Prescription = state.prescriptionModel.data;
                  return Prescription!.isEmpty?EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "") 
                  :ListView.builder(
                    shrinkWrap: true,
                    physics:const ClampingScrollPhysics(),
                    itemCount: Prescription.length,
                    padding:const EdgeInsets.all(0),
                    itemBuilder: (context, index) {
                      if(Prescription.isNotEmpty && disbleExpand == false){
                        Prescription[indexFile].isExpand = true;
                      }
                      return Container(
                        margin:const EdgeInsets.only(left: 28,right: 28,top: 12),
                        child: expandCustom(
                           onPressed: (){
                            if(Prescription[index].isExpand == true){
                              setState(() {
                                Prescription[index].isExpand = false;
                                disbleExpand = true;
                              });
                            }
                            else{
                              for (var file in Prescription) {
                                setState(() {
                                  file.isExpand = false;
                                  indexFile = index;
                                });
                              }
                              setState(() {
                                Prescription[index].isExpand = true;
                              });
                            }
                          },
                          isClick: true,
                          colorButtonExpand: Colorconstands.exam,
                          titleHead: Prescription[index].date.toString(),
                          isExpand: Prescription[index].isExpand,
                          iconLeft:const Icon(Icons.access_time_rounded,size: 30,color: Colorconstands.neutralDarkGrey,),
                          widget: Container(
                            decoration:const BoxDecoration(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                              color: Colorconstands.neutralWhite,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Prescribed Medcines:",style: ThemeConstands.headline6_SemiBold_14.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                            ListView.builder(
                                              physics:const NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              padding:const EdgeInsets.all(0),
                                              itemCount: Prescription[index].prescriptionItemData!.length,
                                              itemBuilder: (context, indexSub) {
                                                return Text("${Prescription[index].prescriptionItemData![indexSub].medication!.name.toString()} x ${Prescription[index].prescriptionItemData![indexSub].quantity.toString()}",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,);
                                              },
                                            )
                                          ],
                                        ),
                                        Container(
                                          margin:const EdgeInsets.symmetric(vertical: 12),
                                          height: 50,
                                          child: Row(
                                            children: [
                                              // Container(
                                              //   child: MaterialButton(
                                              //     height: 50,
                                              //     padding:const EdgeInsets.all(5),
                                              //     onPressed: (){
                                              //       showModalBottomSheet(
                                              //         enableDrag: false,
                                              //         isDismissible: true,
                                              //         backgroundColor: Colors.transparent,
                                              //         isScrollControlled: true,
                                              //         shape: const RoundedRectangleBorder(
                                              //           borderRadius: BorderRadius.only(
                                              //               topLeft: Radius.circular(16),
                                              //               topRight: Radius.circular(16)),
                                              //         ),
                                              //         context: context,
                                              //         builder: (context) {
                                              //           return receipt(context,translate,Prescription[index]);
                                              //         }
                                              //       );
                                              //     },
                                              //     color: Colorconstands.primaryColor,
                                              //     shape:const CircleBorder(),
                                              //     child:const Icon(Icons.receipt_long_sharp,color: Colorconstands.neutralWhite,size: 28,),
                                              //   ),
                                              // ),
                                              Expanded(
                                                child: MaterialButton(
                                                  height: 50,
                                                  padding:const EdgeInsets.all(5),
                                                  onPressed: (){
                                                    // showModalBottomSheet(
                                                    //   enableDrag: false,
                                                    //   isDismissible: true,
                                                    //   backgroundColor: Colors.transparent,
                                                    //   isScrollControlled: true,
                                                    //   shape: const RoundedRectangleBorder(
                                                    //     borderRadius: BorderRadius.only(
                                                    //         topLeft: Radius.circular(16),
                                                    //         topRight: Radius.circular(16)),
                                                    //   ),
                                                    //   context: context,
                                                    //   builder: (context) {
                                                    //     return reorder(context,translate,Prescription[index]);
                                                    //   }
                                                    // );
                                                    showModalBottomSheet(
                                                      enableDrag: false,
                                                      isDismissible: true,
                                                      backgroundColor: Colors.transparent,
                                                      isScrollControlled: true,
                                                      shape: const RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.only(
                                                            topLeft: Radius.circular(16),
                                                            topRight: Radius.circular(16)),
                                                      ),
                                                      context: context,
                                                      builder: (context) {
                                                        return receipt(context,translate,Prescription[index]);
                                                      }
                                                    );
                                                  },
                                                  color: Colorconstands.primaryColor,
                                                  shape:RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(25)
                                                  ),
                                                  child:Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      const Icon(Icons.receipt_long_sharp,color: Colorconstands.neutralWhite,size: 28,),
                                                      const SizedBox(width: 8,),
                                                      Text("Receipt",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,),
                                                    ],
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                else{
                  return const EmptyWidget(icon: Icon(Icons.error,size:28,color:Colorconstands.errorColor), title: "Empty", subtitle: "");
                }
              },
            ),
          ),
        )
      ],
    );
  }
  Widget reorder(BuildContext context, String translate, Datum Prescription ) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: Column(
          children: [
            Container(
              width: 100,
              height: 5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(1),
                color:const Color.fromRGBO(0, 0, 0, 0.1),
              ),
            ),
            const SizedBox(height: 10,),
            Row(
              children: [
                Container(width: 50,),
                Expanded(
                  child: Text(
                    "New Order",
                    style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.primaryColor),textAlign:  TextAlign.center,
                  ),
                ),
                IconButton(
                  padding:const EdgeInsets.all(0),
                  onPressed: (){
                    Navigator.of(context).pop();
                  }, 
                  icon:const Icon(Icons.close,size: 25,color: Colorconstands.neutralDarkGrey,)
                )
              ],
            ),
            Container(
              padding:const EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Name:",
                            style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                          ),
                          const SizedBox(width: 6,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Sophea Som",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "012 234 567",
                                  style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "Date:       ",
                          style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.left,
                        ),
                        Text(
                          Prescription.date.toString(),
                          style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            const Divider(
              thickness: 0.5,
              color: Color.fromRGBO(0, 0, 0, 0.1),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 18),
                      margin:const EdgeInsets.only(top: 10),
                      child: ListView.separated(
                      physics:const NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      shrinkWrap: true,
                      itemCount: Prescription.prescriptionItemData!.length,
                      itemBuilder:(context, index) {
                        return Row(
                          crossAxisAlignment:CrossAxisAlignment.center,
                          mainAxisAlignment:MainAxisAlignment.spaceBetween,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(12.0),
                              child: Image.network(
                                  Prescription.prescriptionItemData![index].medication!.imageData ==null?"https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg": Prescription.prescriptionItemData![index].medication!.imageData!.fileDisplay.toString(),
                                  height: 50.0,
                                  width: 50.0,fit: BoxFit.cover,
                              ),
                                                  ),
                            const SizedBox(width: 10,),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(Prescription.prescriptionItemData![index].medication!.name.toString(),style: ThemeConstands.headline5_SemiBold_16,),
                              ),
                            ),
                            const SizedBox(width: 10,),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 26,
                                    child: MaterialButton(
                                      height: 20,
                                      onPressed: () {},
                                      padding:const EdgeInsets.all(0),
                                      splashColor: Colors.transparent,
                                      highlightColor: Colors.transparent,
                                      shape:const CircleBorder(side: BorderSide(color: Colorconstands.neutralDarkGrey)),
                                       child:const Padding(
                                        padding: EdgeInsets.only(bottom: 15),
                                        child: Icon(Icons.minimize_outlined)),
                                    ),
                                  ),
                                  Container(
                                    width: 30,
                                    child: Column(
                                      children: [
                                        Text(Prescription.prescriptionItemData![index].quantity.toString(),style: ThemeConstands.headline5_SemiBold_16,textAlign: TextAlign.center,),
                                        Container(
                                          width: 30,
                                          child:Text("tbl",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color:const Color(0xFF828492)),textAlign: TextAlign.center,)
                                        ),
                                      ],
                                    )
                                  ),
                                  Container(
                                    width: 30,
                                    child: MaterialButton(
                                      height: 20,
                                      onPressed: () {},
                                      padding:const EdgeInsets.all(0),
                                      splashColor: Colors.transparent,
                                      highlightColor: Colors.transparent,
                                      shape:const CircleBorder(side: BorderSide(color: Colorconstands.neutralDarkGrey)),
                                      child:const Icon(Icons.add,),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        );
                      },
                      separatorBuilder:(BuildContext context,int index) {
                        return const Divider(
                          indent: 10,
                          endIndent: 10,
                          thickness: 0.5,
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                        );
                      },
                                                                                                                                        ),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 12),
                      height: 1,
                      color:const Color(0xFFEFEAEA),
                    ),
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 25),
                      child: Column(
                        children: [
                          Text("Paymant summary",style: ThemeConstands.headline5_SemiBold_16,textAlign: TextAlign.center,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  "Price",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "\$18.50",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  "Delivery free",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "\$2.0 FREE",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                               Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                 children: [
                                   Icon(Icons.verified,color: Colorconstands.primaryColor,),
                                   const SizedBox(width: 8,),
                                   Text(
                                      "Membership Discount 5%",
                                      style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                    ),
                                 ],
                               ),
                                
                                Text(
                                  "-\$0.93",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 12,horizontal: 25),
                      height: 1,
                      color:const Color(0xFFEFEAEA),
                    ),
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              "Total",
                              style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                            ),
                            Text(
                              "\$17.57",
                              style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding:const EdgeInsets.symmetric(horizontal: 30,vertical: 28),
              decoration: BoxDecoration(
                color: Colorconstands.neutralWhite,
                borderRadius: BorderRadius.circular(28),
                  boxShadow: const [
                    BoxShadow(
                      color: Colorconstands.neutralGrey,
                      blurRadius: 15,
                      offset: Offset(-5, 5),
                    ),
                  ],
              ),
              child: MaterialButton(
                height: 50,
                padding:const EdgeInsets.all(5),
                onPressed: (){
                  Navigator.of(context).pop();
                },
                color: Colorconstands.primaryColor,
                shape:RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)
                ),
                child:Row(
                  children: [
                    Expanded(child: Text("Order (\$17.53)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,)),
                  ],
                ),
              ),
            ),
          
          ],
        ),
      );
    },);                                                                                
  }

  Widget receipt(BuildContext context, String translate,Datum Prescription) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: Column(
          children: [
            Container(
              width: 100,
              height: 5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(1),
                color:const Color.fromRGBO(0, 0, 0, 0.1),
              ),
            ),
            const SizedBox(height: 10,),
            Row(
              children: [
                Container(width: 50,),
                Expanded(
                  child: Text(
                    "Receipt",
                    style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                  ),
                ),
                IconButton(
                  padding:const EdgeInsets.all(0),
                  onPressed: (){
                    Navigator.of(context).pop();
                  }, 
                  icon:const Icon(Icons.close,size: 25,color: Colorconstands.neutralDarkGrey,)
                )
              ],
            ),
            Container(
              padding:const EdgeInsets.symmetric(horizontal: 35),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Name:",
                            style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                          ),
                          const SizedBox(width: 6,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Sophea Som",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "012 234 567",
                                  style: ThemeConstands.headline6_Medium_14.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "Date:       ",
                          style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black),textAlign:  TextAlign.left,
                        ),
                        Text(
                          Prescription.date.toString(),
                          style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            const Divider(
              thickness: 0.5,
              color: Color.fromRGBO(0, 0, 0, 0.1),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 18),
                      margin:const EdgeInsets.only(top: 10),
                      child: ListView.separated(
                      physics:const NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      shrinkWrap: true,
                      itemCount: Prescription.prescriptionItemData!.length,
                      itemBuilder:(context, index) {
                        return Row(
                          crossAxisAlignment:CrossAxisAlignment.center,
                          mainAxisAlignment:MainAxisAlignment.spaceBetween,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(12.0),
                              child: Image.network(
                                  Prescription.prescriptionItemData![index].medication!.imageData ==null?"https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg": Prescription.prescriptionItemData![index].medication!.imageData!.fileDisplay.toString(),
                                  height: 50.0,
                                  width: 50.0,fit: BoxFit.cover,
                              ),
                                                  ),
                            const SizedBox(width: 10,),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(Prescription.prescriptionItemData![index].medication!.name.toString(),style: ThemeConstands.headline5_SemiBold_16,),
                              ),
                            ),
                            const SizedBox(width: 10,),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 50,
                                    child: Text(Prescription.prescriptionItemData![index].quantity.toString(),style: ThemeConstands.headline5_SemiBold_16,textAlign: TextAlign.center,)
                                  ),
                                  Container(
                                    width: 50,
                                    child:Text("tbl",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color:const Color(0xFF828492)),textAlign: TextAlign.center,)
                                  ),
                                  
                                ],
                              ),
                            )
                          ],
                        );
                      },
                      separatorBuilder:(BuildContext context,int index) {
                        return const Divider(
                          indent: 10,
                          endIndent: 10,
                          thickness: 0.5,
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                        );
                      },
                                                                                                                                        ),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 12),
                      height: 1,
                      color:const Color(0xFFEFEAEA),
                    ),
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 25),
                      child: Column(
                        children: [
                          Text("Paymant summary",style: ThemeConstands.headline5_SemiBold_16,textAlign: TextAlign.center,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  "Price",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "\$18.50",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                  "Delivery free",
                                  style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                                Text(
                                  "\$2.0 FREE",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                               Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                 children: [
                                   Icon(Icons.verified,color: Colorconstands.primaryColor,),
                                   const SizedBox(width: 8,),
                                   Text(
                                      "Membership Discount 5%",
                                      style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                    ),
                                 ],
                               ),
                                
                                Text(
                                  "-\$0.93",
                                  style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                                ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(vertical: 12,horizontal: 25),
                      height: 1,
                      color:const Color(0xFFEFEAEA),
                    ),
                    Container(
                      padding:const EdgeInsets.symmetric(horizontal: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              "Total",
                              style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                            ),
                            Text(
                              "\$17.57",
                              style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralDarkGrey),textAlign:  TextAlign.center,
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding:const EdgeInsets.symmetric(horizontal: 30,vertical: 28),
              decoration: BoxDecoration(
                color: Colorconstands.neutralWhite,
                borderRadius: BorderRadius.circular(28),
                  boxShadow: const [
                    BoxShadow(
                      color: Colorconstands.neutralGrey,
                      blurRadius: 15,
                      offset: Offset(-5, 5),
                    ),
                  ],
              ),
              child: MaterialButton(
                height: 50,
                padding:const EdgeInsets.all(5),
                onPressed: (){
                  Navigator.of(context).pop();
                },
                color: Colorconstands.neutralGrey,
                shape:RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)
                ),
                child:Row(
                  children: [
                    Expanded(child: Text("Order (\$17.53)",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.black),textAlign: TextAlign.center,)),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    },);                                                                                
  }
}