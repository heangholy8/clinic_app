import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/feature_model/prescription_model.dart';
import 'package:clinic_application/service/apis/pateint_api/all_menu_api/menu_api.dart';
import 'package:equatable/equatable.dart';

part 'prescription_history_event.dart';
part 'prescription_history_state.dart';

class PrescriptionHistoryBloc extends Bloc<PrescriptionHistoryEvent, PrescriptionHistoryState> {
  final GetMenuFeatureApi getMenuFeature;
  PrescriptionHistoryBloc({required this.getMenuFeature}) : super(PrescriptionHistoryInitial()) {
    on<GetPriscriptionEvent>((event, emit) async{
      emit(PrescriptionHistoryLoading());
      try{
        var data = await getMenuFeature.getPrescriptionApi();
        emit(PrescriptionHistoryLoaded(prescriptionModel: data));
      }catch (e){
        emit(PrescriptionHistoryError());
      }
    });
  }
}
