part of 'prescription_history_bloc.dart';

sealed class PrescriptionHistoryState extends Equatable {
  const PrescriptionHistoryState();
  
  @override
  List<Object> get props => [];
}

final class PrescriptionHistoryInitial extends PrescriptionHistoryState {}

final class PrescriptionHistoryLoading extends PrescriptionHistoryState {}
final class PrescriptionHistoryLoaded extends PrescriptionHistoryState {
  final PrescriptionModel prescriptionModel;
  const PrescriptionHistoryLoaded({required this.prescriptionModel});
}
final class PrescriptionHistoryError extends PrescriptionHistoryState {}
