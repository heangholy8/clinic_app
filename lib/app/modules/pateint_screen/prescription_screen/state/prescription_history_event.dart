part of 'prescription_history_bloc.dart';

sealed class PrescriptionHistoryEvent extends Equatable {
  const PrescriptionHistoryEvent();

  @override
  List<Object> get props => [];
}

class GetPriscriptionEvent extends PrescriptionHistoryEvent{}
