import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/appointment_screen/view/appintment_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/home_page/widget/card_appointment_home.dart';
import 'package:clinic_application/app/modules/pateint_screen/home_page/widget/card_menu_home.dart';
import 'package:clinic_application/app/modules/pateint_screen/home_page/widget/custom_expansion.dart';
import 'package:clinic_application/app/modules/pateint_screen/home_page/widget/nav_home_page.dart';
import 'package:clinic_application/app/modules/pateint_screen/profile_screen.dart/state/bloc/info_patient_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/view/tab_feature_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../appointment_screen/state/get_appointment/patient_appointment_bloc.dart';
import '../../profile_screen.dart/view/profile_screen.dart';

class HomePagePateint extends StatefulWidget {
  const HomePagePateint({Key? key}) : super(key: key);

  @override
  State<HomePagePateint> createState() => _HomePagePateintState();
}

bool expainWidgetEmergency = false;

class _HomePagePateintState extends State<HomePagePateint> {
   
  String nameUser = "------";
  String nameClinic = "Viborith's Clinic";
  String blood = "--";
  String gender = "";
  String age = "";
  String patientId = "";
  String profilePicture = "https://thumbs.dreamstime.com/b/default-avatar-profile-icon-vector-social-media-user-image-182145777.jpg";

  void initState() {
    BlocProvider.of<InfoPatientBloc>(context).add(GetInfoPatientEvent());
    BlocProvider.of<PatientAppointmentBloc>(context).add( GetPatientAppointment());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var translate = context.locale.toString();
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        bottom: false,
        child: Container(
          child: Column(
            children: [
              BlocListener<InfoPatientBloc, InfoPatientState>(
                listener: (context, state) {
                  if(state is InfoPatientLoaded){
                    var dataProfile = state.infoPateint.data;
                    setState(() {
                      nameUser = dataProfile==null?nameUser: translate == "km"?"${dataProfile!.lastname.toString()} ${dataProfile.firstname.toString()}":"${dataProfile!.lastnameLatin.toString()} ${dataProfile.firstnameLatin.toString()}";
                      profilePicture = dataProfile==null?profilePicture: dataProfile.profile == null? profilePicture:profilePicture;
                      //nameClinic = dataProfile..toString();
                      blood = dataProfile!.bloodGroupTypeData ==null?blood:dataProfile.bloodGroupTypeData!.name.toString();
                      gender = dataProfile.gender.toString();
                      age = dataProfile.age.toString();
                      patientId = dataProfile.patientId.toString();
                    });
                  }
                },
                child: Container(),
              ),
              Container(
                margin:const EdgeInsets.only(left: 28,top: 18,bottom: 12),
                child: Row(
                  children: [
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(ImageAssets.app_logo,width: MediaQuery.of(context).size.width/4,),
                          Container(
                            margin:const EdgeInsets.only(top: 5),
                            padding:const EdgeInsets.symmetric(horizontal: 5,vertical: 2),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colorconstands.neutralWhite),
                              borderRadius: BorderRadius.circular(16)
                            ),
                            child:const Text("Vibolrith’s Health System",style: TextStyle(fontSize: 9,color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          // Container(alignment: Alignment.center,child: Text(nameClinic,style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,)),
                          // const SizedBox(height: 8,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(nameUser,style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                                  Text("Blood type: $blood",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.left,),
                                ],
                              ),
                              const SizedBox(width: 12,),
                              GestureDetector(
                                onTap: () {
                                   Navigator.push(context,PageRouteBuilder(
                                      pageBuilder: (context, animation1, animation2) => const ProfileScreen(),
                                      transitionDuration: Duration.zero,
                                      reverseTransitionDuration: Duration.zero,
                                    ),
                                  );
                                },
                                child: Container(
                                  padding:const EdgeInsets.all(3),
                                  decoration:const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colorconstands.neutralWhite,
                                  ),
                                  child: CircleAvatar(
                                    radius: 25,
                                    backgroundImage: NetworkImage(profilePicture),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    
                  ],
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                   // padding:const EdgeInsets.only(bottom: 18),
                    margin:const EdgeInsets.symmetric(horizontal: 28),
                    child: Column(
                      children: [
                        CustomHelpExpandEmergency(
                          titleHead:
                            "Need urgent assitant?".tr(),
                          isExpand: expainWidgetEmergency,
                          widget: Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.symmetric(horizontal: 18.0,vertical: 12),
                            child: MaterialButton(
                              onPressed: () {
                              //  Navigator.push(context,
                              //     MaterialPageRoute(builder: (context) => const DrugsOrderScreen()),
                              //   );
                              },
                              color: Colorconstands.errorColor,
                              padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 8),
                              shape:const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(22))),
                              child: Text("Need urgent assitant?",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            
                            BlocBuilder<PatientAppointmentBloc, PatientAppointmentState>(
                              builder: (context, state) {
                                if(state is PatientAppointmentLoading){
                                  return Container(
                                    height: 150,
                                    child: const Center(
                                      child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                                    ),
                                  );
                                }
                                else if(state is PatientAppointmentLoaded){
                                    var data = state.patientAppointment.data;
                                    return Column(
                                      children: [
                                        Container(
                                          margin:const EdgeInsets.symmetric(vertical: 18),
                                          child: Row(
                                            children: [
                                              Text("My Appointments",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                                            ],
                                          ),
                                        ),
                                        data!.upcoming!.isEmpty? 
                                        Container(
                                          child: Column(
                                            children: [
                                              Text("No appointment !",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.exam),textAlign: TextAlign.center,),
                                              MaterialButton(
                                                onPressed: () {
                                                  Navigator.push(context,PageRouteBuilder(
                                                      pageBuilder: (context, animation1, animation2) => AppointmentScreen(appointmentData:  data,patientId: patientId,),
                                                      transitionDuration: Duration.zero,
                                                      reverseTransitionDuration: Duration.zero,
                                                    ),
                                                  );
                                                },
                                                color: Colorconstands.neutralWhite,
                                                padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 6),
                                                shape:const RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.all(Radius.circular(22))),
                                                child: Text("Book appointment now",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.center,),
                                              ),
                                            ],
                                          ),
                                        )
                                        :CardAppointmentsHome(appointmentData: data,patientId: patientId,),
                                      ],
                                    );
                                }
                               else{
                                return Container();
                               }
                              },
                            ),
                            Container(
                              margin:const EdgeInsets.symmetric(vertical: 18),
                              child: Row(
                                children: [
                                  Text("My Medical Information",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: CardMenuHome(
                                    width: 90,
                                    onPressed: () {
                                      Navigator.push(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) => TabFeature(isActive: 1,patientAge: age,patientGender: gender,),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                      );
                                    },
                                    title: "Vital\nSign",
                                    icons: ImageAssets.vital_sing,
                                  ),
                                ),
                                const SizedBox(width: 18,),
                                Expanded(
                                  child: CardMenuHome(
                                    width: 90,
                                    onPressed: () {
                                      Navigator.push(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) => TabFeature(isActive: 2,patientAge: age,patientGender: gender,),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                      );
                                    },
                                    title: "Medical\nHistory",
                                   icons: ImageAssets.medical,
                                  ),
                                ),
                                const SizedBox(width: 18,),
                                Expanded(
                                  child: CardMenuHome(
                                    width: 90,
                                    onPressed: () {
                                      Navigator.push(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) => TabFeature(isActive: 3,patientAge: age,patientGender: gender,),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                      );
                                    },
                                    title: "Vaccination\nHistory",
                                    icons: ImageAssets.vaccination,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 18,),
                            Row(
                              children: [
                                Expanded(
                                  child: CardMenuHome(
                                    width: 90,
                                    onPressed: () {
                                      Navigator.push(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) => TabFeature(isActive: 4,patientAge: age,patientGender: gender,),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                      );
                                    },
                                    title: "Physical\nHistory",
                                    icons: ImageAssets.pysical,
                                  ),
                                ),
                                const SizedBox(width: 18,),
                                Expanded(
                                  child: CardMenuHome(
                                    width: 90,
                                    onPressed: () {
                                      Navigator.push(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) => TabFeature(isActive: 5,patientAge: age,patientGender: gender,),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                      );
                                    },
                                    title: "Prescription\nHistory",
                                   icons: ImageAssets.prescription,
                                  ),
                                ),
                                const SizedBox(width: 18,),
                                Expanded(
                                  child: CardMenuHome(
                                    width: 90,
                                    onPressed: () {
                                      Navigator.push(context,PageRouteBuilder(
                                          pageBuilder: (context, animation1, animation2) => TabFeature(isActive: 6,patientAge: age,patientGender: gender,),
                                          transitionDuration: Duration.zero,
                                          reverseTransitionDuration: Duration.zero,
                                        ),
                                      );
                                    },
                                    title: "Laboratory\nHistory",
                                    icons: ImageAssets.laboratory,
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                padding:const EdgeInsets.only(bottom: 28,top: 16),
                decoration:const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colorconstands.primaryColor,Color(0xFF0C7376), Color(0xFF053031),])),
                child: NavHomePage(acitiveNav: 1,imageProfile: profilePicture,))
            ],
          ),
        ),
      ),
    );
  }
}