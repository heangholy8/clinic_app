import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/pateint_screen/appointment_screen/view/appintment_screen.dart';
import 'package:clinic_application/model/appiontment_model/pateint_appiontment_model.dart';
import 'package:flutter/material.dart';
class CardAppointmentsHome extends StatefulWidget {
  final String? patientId;
  final AppointmentModelData appointmentData;
  const CardAppointmentsHome({Key? key, required this.appointmentData,required this.patientId}) : super(key: key);

  @override
  State<CardAppointmentsHome> createState() => _CardAppointmentsHomeState();
}


class _CardAppointmentsHomeState extends State<CardAppointmentsHome> {
  @override
  Widget build(BuildContext context) {
    var dataUpcoming = widget.appointmentData.upcoming;
    return MaterialButton(
      onPressed: () {
        Navigator.push(context,PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => AppointmentScreen(appointmentData:  widget.appointmentData,patientId: widget.patientId,),
            transitionDuration: Duration.zero,
            reverseTransitionDuration: Duration.zero,
          ),
        );
      },
      color: Colorconstands.neutralWhite,
      padding:const EdgeInsets.all(0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
      child: Container(
        padding:const EdgeInsets.all(28),
        child: Row(
          children: [
            Expanded(
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(child: Text("${dataUpcoming![0].appointmentDate!} ${dataUpcoming[0].bookingTimeStr!}",style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.primaryColor),textAlign: TextAlign.left,)),
                        ],
                      ),
                    ),
                    const SizedBox(height: 8,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            height: 60,
                            decoration:const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colorconstands.primaryColor,
                            ),
                            padding:const EdgeInsets.all(0),
                            child: CircleAvatar(
                              backgroundImage: NetworkImage(dataUpcoming[0].doctorData!.profile==null?"https://www.befunky.com/images/wp/wp-2021-01-linkedin-profile-picture-after.jpg?auto=avif,webp&format=jpg&width=944":dataUpcoming[0].doctorData!.profile!.fileDisplay!),
                            ),
                          ),
                          const SizedBox(width: 8,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${dataUpcoming[0].doctorData!.title!} ${dataUpcoming[0].doctorData!.fullnameLatin!}",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.black),textAlign: TextAlign.left,),
                                Text(dataUpcoming[0].doctorData!.titleLatin!,style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.darkGray),textAlign: TextAlign.left,),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            const Icon(Icons.arrow_forward_ios_rounded,color: Colorconstands.neutralGrey,size: 26,)
          ],
        ),
      ),
    );
  }
}