import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/widgets/expansion/expand_section.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomHelpExpandEmergency extends StatefulWidget {
  final String titleHead;
  bool isExpand;
  Widget widget;
  CustomHelpExpandEmergency({
    Key? key,
    required this.titleHead,
    required this.widget,
    required this.isExpand,
  }) : super(key: key);

  @override
  State<CustomHelpExpandEmergency> createState() => _CustomHelpExpandEmergencyState();
}

class _CustomHelpExpandEmergencyState extends State<CustomHelpExpandEmergency>
    with TickerProviderStateMixin {
  late Animation _arrowAnimation;
  late AnimationController _arrowAnimationController;
  final GlobalKey expansionTileKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _arrowAnimationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
    _arrowAnimation =
        Tween(begin: 0.0, end: 3.14).animate(_arrowAnimationController);
  }

  void _scrollToSelectedContent({required GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(const Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: const Duration(milliseconds: 200));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: expansionTileKey,
      decoration: BoxDecoration(
          color: Colorconstands.black.withOpacity(0.2),
          borderRadius: BorderRadius.circular(18.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          MaterialButton(
            highlightColor: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            padding: const EdgeInsets.all(0),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
              ),
            ),
            onPressed: () {
              setState(() {
                widget.isExpand = !widget.isExpand;
                _arrowAnimationController.isCompleted
                    ? _arrowAnimationController.reverse()
                    : _arrowAnimationController.forward();
                _scrollToSelectedContent(expansionTileKey: expansionTileKey);
              });
            },
            child: Container(
              padding: const EdgeInsets.only(
                  left: 18, top: 12, bottom: 12, right: 18),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      widget.titleHead,
                      style:ThemeConstands.headline5_SemiBold_16.copyWith(
                              color: Colorconstands.neutralWhite,),textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    decoration:const BoxDecoration(shape: BoxShape.circle,color: Colorconstands.neutralWhite),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: AnimatedBuilder(
                        animation: _arrowAnimationController,
                        builder: (context, child) => Transform.rotate(
                          angle: _arrowAnimation.value,
                          child: const Icon(
                            Icons.expand_more_outlined,
                            size: 24.0,color: Colorconstands.primaryColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          AnimatedCrossFade(
            firstChild: Container(height: 0, color: Colorconstands.neutralGrey),
            secondChild: const SizedBox(),
            crossFadeState: widget.isExpand
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            duration: const Duration(milliseconds: 400),
          ),
          ExpandedSection(expand: widget.isExpand, child: widget.widget),
        ],
      ),
    );
  }
}
