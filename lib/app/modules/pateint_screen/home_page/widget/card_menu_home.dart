import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';

class CardMenuHome extends StatelessWidget {
  String icons;
  VoidCallback onPressed;
  String title;
  double width;
  double? widthIcon;
  double? heughtIcon;
  TextStyle? style;
  CardMenuHome({Key? key,this.heughtIcon,this.widthIcon,this.style,required this.width,required this.icons,required this.onPressed,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Colorconstands.neutralWhite,
      onPressed: onPressed,
      padding:const EdgeInsets.all(10),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16)
      ),
      child: Container(
        width:width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(icons,width: widthIcon??70,height:heughtIcon?? 70,color: Colorconstands.primaryColor,),
            const SizedBox(height: 12,),
            Text(title,style: style??ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.black),textAlign: TextAlign.center,),
          ],
        ),
      ),
    );
  }
}