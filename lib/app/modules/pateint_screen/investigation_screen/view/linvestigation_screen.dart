import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/expand_widget.dart';
import 'package:clinic_application/app/modules/pateint_screen/tab_feature/widget/header_feature.dart';
import 'package:clinic_application/widgets/empydata_widget/empty_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import '../../../../core/themes/themes.dart';
import '../state/investigation_history_bloc.dart';

class InvestigationScreen extends StatefulWidget {
  const InvestigationScreen({Key? key}) : super(key: key);

  @override
  State<InvestigationScreen> createState() => _InvestigationScreenState();
}

class _InvestigationScreenState extends State<InvestigationScreen> {
  bool labo = true;
  bool byCashOrBank = true;
  int indexFile = 0;
  bool disbleExpand = false;
  // GlobalKey<SfPdfViewerState> _pdf = GlobalKey();
  PdfViewerController controller = PdfViewerController();
  @override
  void initState() {
    setState(() {
      controller = PdfViewerController();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HeaderFeature(
          titleHeader: "Investigation",
        ),
        const SizedBox(height: 12,),
        Container(
              padding:const EdgeInsets.all(5),
              margin:const EdgeInsets.symmetric(horizontal: 28),
              height: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colorconstands.black.withOpacity(0.2)
              ),
              child: Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      elevation: 0,
                      onPressed: (){
                        setState(() {
                         if(labo == false){
                            labo = true;
                          }
                        });
                      },
                      height: 50,
                      padding:const EdgeInsets.all(0),
                      color: labo == true?Colorconstands.neutralWhite:Colorconstands.black.withOpacity(0.0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Text("Laboratory",style: ThemeConstands.headline5_SemiBold_16.copyWith(color:labo == true?Colorconstands.primaryColor:Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                    ),
                  ),
                  Expanded(
                    child: MaterialButton(
                      elevation: 0,
                      onPressed: (){
                        setState(() {
                          if(labo == true){
                            labo = false;
                          }
                        });
                      },
                      height: 50,
                      padding:const EdgeInsets.all(0),
                      color: labo == false?Colorconstands.neutralWhite:Colorconstands.black.withOpacity(0.0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Text("Imaging",style: ThemeConstands.headline5_SemiBold_16.copyWith(color:labo == false?Colorconstands.primaryColor:Colorconstands.neutralWhite),textAlign: TextAlign.center,),
                    ),
                  ),
                ],
              ),
            ),   
        Expanded(
          child: Container(
            margin:const EdgeInsets.only(top: 16),
            child: BlocBuilder<InvestigationHistoryBloc, InvestigationHistoryState>(
              builder: (context, state) {
                if(state is InvestigationHistoryLoading){
                  return const Center(
                    child: CircularProgressIndicator(color: Colorconstands.neutralWhite,),
                  );
                }
                else if(state is InvestigationHistoryLoaded){
                  var dataInvers = state.investigationModel.data;
                  return labo == true? 
                  dataInvers!.labor!.isEmpty?EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "")
                  : ListView.builder(
                    shrinkWrap: true,
                    physics:const ClampingScrollPhysics(),
                    itemCount: dataInvers.labor!.length,
                    padding:const EdgeInsets.all(0),
                    itemBuilder: (context, index) {
                      if(dataInvers.labor!.isNotEmpty && disbleExpand == false){
                        dataInvers.labor![indexFile].isShowPdf = true;
                      }
                      return Container(
                        margin:const EdgeInsets.only(left: 28,right: 28,top: 12),
                        child: expandCustom(
                          onPressed: (){
                            if(dataInvers.labor![index].isShowPdf == true){
                              setState(() {
                                dataInvers.labor![index].isShowPdf = false;
                                disbleExpand = true;
                              });
                            }
                            else{
                              for (var file in dataInvers.labor!) {
                                setState(() {
                                  file.isShowPdf = false;
                                  indexFile = index;
                                });
                              }
                              setState(() {
                                dataInvers.labor![index].isShowPdf = true;
                                controller = PdfViewerController();
                              });
                            }
                              
                          },
                          isClick:true,
                          colorButtonExpand: Colorconstands.exam,
                          titleHead: dataInvers.labor![index].createdAt.toString(),
                          isExpand: dataInvers.labor![index].isShowPdf,
                          iconLeft:const Icon(Icons.access_time_rounded,size: 30,color: Colorconstands.neutralDarkGrey,),
                          widget: Container(
                            height: 595,
                            width: MediaQuery.of(context).size.width,
                            decoration:const BoxDecoration(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                              color: Colorconstands.neutralWhite,
                            ),
                            child: Column(
                              children: [
                                Expanded(
                                  child: Container(
                                      margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                                      child: dataInvers.labor![index].isShowPdf == false?Container():SfPdfViewer.network(
                                        dataInvers.labor![index].fileDisplay.toString(),
                                        controller: controller,
                                        canShowScrollHead: true,
                                        scrollDirection: PdfScrollDirection.vertical,
                                        // key: _pdf,
                                        enableDocumentLinkAnnotation: true,
                                        enableTextSelection: true,
                                        onDocumentLoaded: (PdfDocumentLoadedDetails loading) {
                                          setState(() {});
                                        },
                                      ),
                                    ),
                                ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 16),
                                    child: MaterialButton(
                                        height: 50,
                                        padding:const EdgeInsets.all(10),
                                        onPressed: (){
                                          showModalBottomSheet(
                                            enableDrag: false,
                                            isDismissible: true,
                                            backgroundColor: Colors.transparent,
                                            isScrollControlled: true,
                                            shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(16),
                                                  topRight: Radius.circular(16)),
                                            ),
                                            context: context,
                                            builder: (context) {
                                              return paymentShareService(context);
                                            }
                                          );
                                        },
                                        color: Colorconstands.primaryColor,
                                        shape:RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(25)
                                        ),
                                        child:Row(
                                          children: [
                                            Expanded(child: Text("Share",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.white),textAlign: TextAlign.center,)),
                                          ],
                                        ),
                                      ),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ):
                  dataInvers!.imaging!.isEmpty?EmptyWidget(icon: Image.asset(ImageAssets.emptyImage,width: 150,), title: "Empty Data", subtitle: "")
                  : ListView.builder(
                    shrinkWrap: true,
                    physics:const ClampingScrollPhysics(),
                    itemCount: dataInvers.imaging!.length,
                    padding:const EdgeInsets.all(0),
                    itemBuilder: (context, index) {
                      if(dataInvers.imaging!.isNotEmpty && disbleExpand == false){
                        dataInvers.imaging![indexFile].isShowPdf = true;
                      }
                      return Container(
                        margin:const EdgeInsets.only(left: 28,right: 28,top: 12),
                        child: expandCustom(
                          onPressed: (){
                            if(dataInvers.imaging![index].isShowPdf == true){
                              setState(() {
                                dataInvers.imaging![index].isShowPdf = false;
                                disbleExpand = true;
                              });
                            }
                            else{
                              for (var file in dataInvers.imaging!) {
                                setState(() {
                                  file.isShowPdf = false;
                                  indexFile = index;
                                });
                              }
                              setState(() {
                                dataInvers.imaging![index].isShowPdf = true;
                                controller = PdfViewerController();
                              });
                            }
                              
                          },
                          isClick:true,
                          colorButtonExpand: Colorconstands.exam,
                          titleHead: dataInvers.imaging![index].createdAt.toString(),
                          isExpand: dataInvers.imaging![index].isShowPdf,
                          iconLeft:const Icon(Icons.access_time_rounded,size: 30,color: Colorconstands.neutralDarkGrey,),
                          widget: Container(
                            height: 595,
                            width: MediaQuery.of(context).size.width,
                            decoration:const BoxDecoration(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                              color: Colorconstands.neutralWhite,
                            ),
                            child: Column(
                              children: [
                                Expanded(
                                  child: Container(
                                      margin:const EdgeInsets.symmetric(horizontal: 18,vertical: 18),
                                      child: dataInvers.imaging![index].isShowPdf == false?Container():SfPdfViewer.network(
                                        dataInvers.imaging![index].fileDisplay.toString(),
                                        controller: controller,
                                        canShowScrollHead: true,
                                        scrollDirection: PdfScrollDirection.vertical,
                                        // key: _pdf,
                                        enableDocumentLinkAnnotation: true,
                                        enableTextSelection: true,
                                        onDocumentLoaded: (PdfDocumentLoadedDetails loading) {
                                          setState(() {});
                                        },
                                      ),
                                    ),
                                ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 16),
                                    child: MaterialButton(
                                        height: 50,
                                        padding:const EdgeInsets.all(10),
                                        onPressed: (){
                                          showModalBottomSheet(
                                            enableDrag: false,
                                            isDismissible: true,
                                            backgroundColor: Colors.transparent,
                                            isScrollControlled: true,
                                            shape: const RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(16),
                                                  topRight: Radius.circular(16)),
                                            ),
                                            context: context,
                                            builder: (context) {
                                              return paymentShareService(context);
                                            }
                                          );
                                        },
                                        color: Colorconstands.primaryColor,
                                        shape:RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(25)
                                        ),
                                        child:Row(
                                          children: [
                                            Expanded(child: Text("Share",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.white),textAlign: TextAlign.center,)),
                                          ],
                                        ),
                                      ),
                                  ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
                else{
                  return const EmptyWidget(icon: Icon(Icons.error,size:28,color:Colorconstands.errorColor), title: "Empty Data", subtitle: "");
                }
              },
            ),
          ),
        )
      ],
    );
  }
   Widget paymentShareService(BuildContext context,) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        width: MediaQuery.of(context).size.width,
        margin:const EdgeInsets.only(top: 65),
        padding:const EdgeInsets.only(top: 18),
        decoration:const BoxDecoration(
          color: Colorconstands.neutralWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(18),
            topRight: Radius.circular(18),),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Center(child: Text("Share Service Fee",style: ThemeConstands.headline2_SemiBold_242,)),
            const SizedBox(height: 10,),
            const BenifiteWidget(title: "Share to Telegram",),
            const BenifiteWidget(title: "Save as PDF",),
            const BenifiteWidget(title: "Print with near by printer",),
            const SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.only(left: 28),
              child: Row(
                children: [
                  Text("You need to pay",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.primaryColor,),),
                  Text(" 1000 real ",style: ThemeConstands.headline3_Medium_20.copyWith(color: Colors.red,fontWeight: FontWeight.w700),),
                  Text("per share service fee.",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.primaryColor,overflow: TextOverflow.clip),),
                ],
            )),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Row(children: [
                IconButton(onPressed: (){
                  setState(() {
                    byCashOrBank=!byCashOrBank;
                  },);
                }, icon:Icon(byCashOrBank? Icons.radio_button_checked_sharp:Icons.radio_button_off,color: Colorconstands.primaryColor,)),
                Text("Pay by Bank",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),),
              ],),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Row(children: [
                IconButton(onPressed: (){
                  setState(() {
                    byCashOrBank=!byCashOrBank;
                  },);
                }, icon:Icon(!byCashOrBank? Icons.radio_button_checked_sharp:Icons.radio_button_off,color: Colorconstands.primaryColor)),
                Text("Pay by Cash",style: ThemeConstands.headline5_SemiBold_16.copyWith(color: Colorconstands.black),),
              ],),
            ),
            const SizedBox(height: 35,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: MaterialButton(
                height: 50,
                padding:const EdgeInsets.all(10),
                onPressed: (){
                  Navigator.pop(context);
                },
                color: Colorconstands.primaryColor,
                shape:RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)
                ),
                child:Row(
                  children: [
                    Expanded(child: Text("Confrim",style: ThemeConstands.headline4_SemiBold_18.copyWith(color: Colorconstands.white),textAlign: TextAlign.center,)),
                  ],
                ),
              ),
            ),
             const SizedBox(height: 55,),
          ],
        ),
      );
    },);                                                                                
  }
}

class BenifiteWidget extends StatelessWidget {
  final String title;
  const BenifiteWidget({
    Key? key, required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25,top: 8),
      child: Row(children: [
        const Icon(Icons.check_circle_outline,color: Colorconstands.primaryColor,),
        const SizedBox(width: 20,),
        Text(title,style: ThemeConstands.headline4_Regular_18,)
      ],),
    );
  }
}