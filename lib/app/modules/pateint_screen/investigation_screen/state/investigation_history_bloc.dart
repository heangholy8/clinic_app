import 'package:bloc/bloc.dart';
import 'package:clinic_application/model/pateint_model/feature_model/investigation_model.dart';
import 'package:clinic_application/service/apis/pateint_api/all_menu_api/menu_api.dart';
import 'package:equatable/equatable.dart';
part 'investigation_history_event.dart';
part 'investigation_history_state.dart';

class InvestigationHistoryBloc extends Bloc<InvestigationHistoryEvent, InvestigationHistoryState> {
  final GetMenuFeatureApi getMenuFeature;
  InvestigationHistoryBloc({required this.getMenuFeature}) : super(InvestigationHistoryInitial()) {
    on<GetInvestigationHistoryEvent>((event, emit) async{
      emit(InvestigationHistoryLoading());
      try{
        var data = await getMenuFeature.getInvestigationApi();
        emit(InvestigationHistoryLoaded(investigationModel: data));
      }catch (e){
        emit(InvestigationHistoryError());
      }
    });
  }
}
