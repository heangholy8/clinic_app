part of 'investigation_history_bloc.dart';

sealed class InvestigationHistoryEvent extends Equatable {
  const InvestigationHistoryEvent();

  @override
  List<Object> get props => [];
}

class GetInvestigationHistoryEvent extends InvestigationHistoryEvent{}
