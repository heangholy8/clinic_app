part of 'investigation_history_bloc.dart';

sealed class InvestigationHistoryState extends Equatable {
  const InvestigationHistoryState();
  
  @override
  List<Object> get props => [];
}

final class InvestigationHistoryInitial extends InvestigationHistoryState {}

final class InvestigationHistoryLoading extends InvestigationHistoryState {}
final class InvestigationHistoryLoaded extends InvestigationHistoryState {
  final InvestigationModel investigationModel;
  const InvestigationHistoryLoaded({required this.investigationModel});
}
final class InvestigationHistoryError extends InvestigationHistoryState {}
