import 'dart:async';
import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/view/result_calculate_vital_sign.dart';
import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/view/vital_sign_form_create_screen.dart';
import 'package:clinic_application/app/modules/doctor_screen/home_doctor_screen/view/home_doctor_screen.dart';
import 'package:clinic_application/app/modules/login_screen/state/list_clinic/bloc/list_clinic_bloc.dart';
import 'package:clinic_application/app/modules/option_screen/option_screen.dart';
import 'package:clinic_application/app/modules/pateint_screen/home_page/view/home_page.dart';
import 'package:clinic_application/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../core/resources/asset_resource.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final keyStoragePref keyPref = keyStoragePref();
  
  Future checkLogin() async {
    final SharedPreferences _pref = await SharedPreferences.getInstance(); 
    var token = _pref.getString("jsonToken") ?? "";
    var tokenPatient = _pref.getString("jsonTokenPatient") ?? "";
    var whoLogin = _pref.getString(keyPref.whoLogin) ?? "";
    debugPrint("object $token");
    debugPrint("object $whoLogin");
    // Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
    //         pageBuilder: (context, animation1, animation2) => const FormCreateVitalSingScreem (),
    //         transitionDuration: Duration.zero,
    //         reverseTransitionDuration: Duration.zero,
    //       ),(route) => false,
    //       );
    if(whoLogin=="Doctor"){
      if(token!=""){
        Future.delayed(const Duration(milliseconds: 1500), () async{
        setState(() {
          Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => const HomePageDoctor(),
            transitionDuration: Duration.zero,
            reverseTransitionDuration: Duration.zero,
          ),(route) => false,
          );
        });
      });
      }else{
        Future.delayed(const Duration(milliseconds: 1500), () async{
        setState(() {
            BlocProvider.of<ListClinicBloc>(context).add(GetListClinic());
            Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => const OptionScreen(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ),(route) => false,
            );
          });
        });
      }
    }
    else{
      if(tokenPatient!=""){
        Future.delayed(const Duration(milliseconds: 1500), () async{
        setState(() {
          Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => const HomePagePateint(),
            transitionDuration: Duration.zero,
            reverseTransitionDuration: Duration.zero,
          ),(route) => false,
          );
        });
      });
      }else{
        Future.delayed(const Duration(milliseconds: 1500), () async{
        setState(() {
            BlocProvider.of<ListClinicBloc>(context).add(GetListClinic());
            Navigator.pushAndRemoveUntil(context,PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) => const OptionScreen(),
              transitionDuration: Duration.zero,
              reverseTransitionDuration: Duration.zero,
            ),(route) => false,
            );
          });
        });
      }
    }
  }
  @override
  void initState() {
    setState(() {
     checkLogin();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        child: Container(
          margin:const EdgeInsets.symmetric(vertical: 75),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin:const EdgeInsets.only(top: 25,bottom: 18),
                child:Text("Welcome to",style: ThemeConstands.headline3_Medium_20_26height.copyWith(color: Colorconstands.neutralWhite,),),
              ),
              Container(
                child: Image.asset(ImageAssets.app_logo,),
              ),
              Container(
                margin:const EdgeInsets.only(top: 25),
                padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 3),
                decoration: BoxDecoration(
                  border: Border.all(color: Colorconstands.neutralWhite),
                  borderRadius: BorderRadius.circular(16)
                ),
                child: Text("Vibolrith’s Health System",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite,),),
              ),
              Container(
                margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 28),
                child:Text("Track your medical history and book clinic appointments effortlessly.",style: ThemeConstands.headline3_Medium_20_26height.copyWith(color: Colorconstands.neutralWhite,),textAlign: TextAlign.center,),
              ),
            ],
          ),
        ),
      ),
    );
  }
}