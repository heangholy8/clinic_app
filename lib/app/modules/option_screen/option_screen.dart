// ignore_for_file: avoid_print

import 'package:clinic_application/app/core/resources/asset_resource.dart';
import 'package:clinic_application/app/core/themes/themes.dart';
import 'package:clinic_application/app/modules/login_screen/state/list_clinic/bloc/list_clinic_bloc.dart';
import 'package:clinic_application/app/modules/login_screen/views/login_patient_screen.dart';
import 'package:clinic_application/app/modules/login_screen/views/login_screen.dart';
import 'package:clinic_application/widgets/button_widget/button_icon_left_with_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../core/themes/color_app.dart';

class OptionScreen extends StatefulWidget {
  const OptionScreen({
    Key? key,}) : super(key: key);
  @override
  State<OptionScreen> createState() => _OptionScreenState();
}

class _OptionScreenState extends State<OptionScreen> {
  bool isTrail = false;
  bool isoffline = true;
  
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colorconstands.primaryColor,
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin:const EdgeInsets.only(top: 38),
                      child: Image.asset(ImageAssets.app_logo,),
                    ),
                    Container(
                      margin:const EdgeInsets.only(top: 25),
                      padding:const EdgeInsets.symmetric(horizontal: 18,vertical: 3),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colorconstands.neutralWhite),
                        borderRadius: BorderRadius.circular(16)
                      ),
                      child: Text("Vibolrith’s Health System",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite,),),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 28),
                      child:Text("Track your medical history and book clinic appointments effortlessly.",style: ThemeConstands.headline3_Medium_20_26height.copyWith(color: Colorconstands.neutralWhite,),textAlign: TextAlign.center,),
                    ),
                    Container(
                      margin:const EdgeInsets.symmetric(horizontal: 28),
                      child: ButtonIconLeftWithTitle(
                        onTap: () {
                          setState(() {
                              BlocProvider.of<ListClinicBloc>(context).add(GetListClinic());
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> const LogingScreen()));
                            });
                        },
                        title: "Doctor Login Account".tr(),
                        textStyleButton: ThemeConstands
                            .headline5_SemiBold_16
                            .copyWith(
                          color: Colorconstands.primaryColor,
                        ),
                        speaceTitleAndImage: 12,
                        buttonColor: Colorconstands.neutralWhite,
                        radiusButton: 25,
                        heightButton: 50,
                        panddinHorButton: 18,
                        panddingVerButton: 6, 
                        iconImage: ImageAssets.icon_login, 
                        iconColor: Colorconstands.primaryColor,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin:const EdgeInsets.symmetric(horizontal: 28,vertical: 28),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Patient login?",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.neutralWhite,),textAlign: TextAlign.center,),
                    TextButton(
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> const LoginPatientScreen()));
                      }, 
                      child: Text("Tap here",style: ThemeConstands.headline3_SemiBold_20.copyWith(color: Colorconstands.exam,),textAlign: TextAlign.center,),)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
