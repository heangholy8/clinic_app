// ignore_for_file: constant_identifier_names
const String ICONS_PATH = "assets/icons";
const String IMAGE_PATH = "assets/images";
const String LOGO_PATH = "assets/logo";
const String CLIP_PATH = "assets/clip_images";
const String SVG_PATH = "assets/images/svg";
const String PNG_PATH = "assets/images/png";
const String ICONS_SVG_PATH = "assets/icons/svg";
const String ICONS_PNG_PATH = "assets/icons/png";

class ImageAssets {
  // <<<<<<<<<<<<<<< LOGO >>>>>>>>>>>>>>>>> //
  static const String app_logo = "$LOGO_PATH/VHS Health System Logo4 1.png";


  //<<<<<<<<<<<<<<<<< icons >>>>>>>>>>>>>>>>>>>>>>>>>>
   static const String icon_login = "$ICONS_SVG_PATH/login.svg";
  // ------- icons nenu -------------------
   static const String vital_sing = "$ICONS_SVG_PATH/vital-sing.svg";
   static const String trolley_icon = "$ICONS_SVG_PATH/trolley.svg";
   static const String vaccination = "$ICONS_SVG_PATH/vaccination.svg";
   static const String pysical = "$ICONS_SVG_PATH/physical.svg";
   static const String laboratory = "$ICONS_SVG_PATH/laboratory.svg";
   static const String prescription = "$ICONS_SVG_PATH/prescription.svg";
   static const String medical = "$ICONS_SVG_PATH/medical.svg";
   static const String scan = "$ICONS_SVG_PATH/scan.svg";
   static const String voiceMenu = "$ICONS_SVG_PATH/streamline_voice-mail.svg";
   static const String settingIcons = "$ICONS_SVG_PATH/setting-3.svg";
   static const String profileIcon = "$ICONS_SVG_PATH/profile-circle.svg";
   static const String informationIcons = "$ICONS_SVG_PATH/info-circle.svg";
   static const String notification = "$ICONS_SVG_PATH/notification.svg";
   static const String homeOutline = "$ICONS_SVG_PATH/home-2.svg";
   static const String hospital = "$ICONS_SVG_PATH/hospital.svg";
   static const String notification_fill = "$ICONS_SVG_PATH/notification-fill.svg";
   static const String home_fill = "$ICONS_SVG_PATH/home-2-fill.svg";


/// ============== image BMI ===================
   static const String menUnderweight = "$PNG_PATH/Screenshot_2024-05-29_at_3.51.32_in_the_afternoon-removebg-preview.png";
   static const String menNomal = "$PNG_PATH/Screenshot_2024-05-29_at_3.51.54_in_the_afternoon-removebg-preview.png";
   static const String menRiskTOOverweight = "$PNG_PATH/Screenshot_2024-05-29_at_3.52.09_in_the_afternoon-removebg-preview.png";
   static const String menOvereweight = "$PNG_PATH/Screenshot_2024-05-29_at_3.52.23_in_the_afternoon-removebg-preview.png";
   static const String menObese = "$PNG_PATH/Screenshot_2024-05-29_at_3.52.50_in_the_afternoon-removebg-preview.png";

   static const String girlUnderweight = "$PNG_PATH/Screenshot_2024-05-30_at_1.50.02_in_the_afternoon-removebg-preview.png";
   static const String girlNomal = "$PNG_PATH/Screenshot_2024-05-30_at_1.50.40_in_the_afternoon-removebg-preview.png";
   static const String girlRiskTOOverweight = "$PNG_PATH/Screenshot_2024-05-30_at_1.50.53_in_the_afternoon-removebg-preview.png";
   static const String girlOvereweight = "$PNG_PATH/Screenshot_2024-05-30_at_1.51.17_in_the_afternoon-removebg-preview.png";
   static const String girlObese = "$PNG_PATH/Screenshot_2024-05-30_at_1.51.51_in_the_afternoon-removebg-preview.png";


// ==================== Image Empty Data ======================
static const String emptyImage = "$PNG_PATH/empty_data.png";
}
