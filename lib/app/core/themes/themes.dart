import 'package:flutter/material.dart';

class ThemeConstands {
  static const List<String> fontFamilyFallback = ['KantumruyPro'];      
  static const  headline1_SemiBold_32 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 32.0
);
  static const  headline1_SemiBold_322 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 32.0
);
  static const  headline2_SemiBold_242 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 24.0
);
  static const  headline2_SemiBold_24 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 24.0
);
  static const  headline3_SemiBold_20 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
);
  static const  headline3_Medium_20_26height =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
);
  static const  headline3_Medium_20 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 20.0
);
  static const  headline4_SemiBold_18 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
);
  static const  headline4_Medium_18 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
);
  static const  headline4_Regular_18 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 18.0
);
  static const  headline5_SemiBold_16 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  button_SemiBold_16 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  headline5_Medium_16 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  subtitle1_Regular_16 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 16.0
);
  static const  headline6_SemiBold_14 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  headline6_Medium_14 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  headline6_Regular_14_24height =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  headline6_Regular_14_20height =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 14.0
);
  static const  overline_Semibold_12 =  TextStyle(
    fontFamily: "KantumruyPro-SemiBold",
    fontStyle:  FontStyle.normal,
    fontSize: 12.0
);
  static const  caption_Regular_12 =  TextStyle(
    fontFamily: "KantumruyPro-Regular",
    fontStyle:  FontStyle.normal,
    fontSize: 12.0
);
}
