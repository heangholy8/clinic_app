mixin class keyStoragePref {
  static const String _jsonToken = 'jsonToken';
  static const String _jsonTokenPatient = 'jsonTokenPatient';
  static const String _langSuccess = 'LangSuccess';
  static const String _whoLogin = 'WhoLogin';
  static const String _keySecrypt = 'keySecrypt';
 

  String get jsonToken => _jsonToken;
  String get jsonTokenPatient => _jsonTokenPatient;
  String get langSuccess => _langSuccess;
  String get whoLogin => _whoLogin;
  String get keySecrypt => _keySecrypt;

}
