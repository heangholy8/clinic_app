import 'package:clinic_application/app/modules/option_screen/option_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RemoveStoragePref {
  removeToken(context) async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();

      await pref.remove("jsonToken");
      await pref.remove("jsonTokenPatient");
      Navigator.pushAndRemoveUntil(
        context,
        PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) => const OptionScreen(),
        transitionDuration: Duration.zero,
        reverseTransitionDuration: Duration.zero,
      ),
            (route) => false);
    } catch (e) {
      debugPrint("Fail to remove pref $e");
    }
  }
}
