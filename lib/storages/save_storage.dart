import 'package:clinic_application/storages/key_storage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SaveStoragePref with keyStoragePref {

  void saveJsonToken({required String authModel}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref
          .setString(jsonToken, authModel)
          .then((value) => debugPrint("Success to store json in cache"));
    } catch (e) {
      // debugPrint("Unsuccess to store json auth in cache");
    }
  }
  void saveJsonTokenPatient({required String authPatientModel}) async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      _pref
          .setString(jsonTokenPatient, authPatientModel)
          .then((value) => debugPrint("Success to store json in cache"));
    } catch (e) {
      // debugPrint("Unsuccess to store json auth in cache");
    }
  }
  void saveConfirmInformationLogin({required String login}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(whoLogin, login)
          .then((value) => debugPrint("Who Login: $value $login"));
    } catch (e) {
      debugPrint("Who login: $e");
    }
  }

  void saveKeySecryt({required String key}) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs
          .setString(keySecrypt, key)
          .then((value) => debugPrint("ket secrypt: $value $key"));
    } catch (e) {
      debugPrint("ket secrypt: $e");
    }
  }

  //=================End Save defauld child ==============
}
