// ignore_for_file: avoid_print, unnecessary_null_comparison
import 'dart:convert';
import 'package:clinic_application/model/auth_model/auth_model.dart';
import 'package:clinic_application/model/auth_model/auth_pateint_model.dart';
import 'package:clinic_application/storages/key_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GetStoragePref with keyStoragePref {
  Future<AuthModel> get getJsonToken async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      String? data = _pref.getString(jsonToken) ?? "";
      AuthModel? authModel;
      if (data != null) {
        var result = json.decode(data.toString());
        authModel = AuthModel.fromJson(result);
        return authModel;
      }
      return authModel ?? AuthModel();
    } catch (e) {
      throw Exception("Get JsonToke Unsuccess");
    }
  }

  Future<AuthPateintModel> get getJsonTokenPatient async {
    try {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      String? data = _pref.getString(jsonTokenPatient) ?? "";
      AuthPateintModel? authPatientModel;
      if (data != null) {
        var result = json.decode(data.toString());
        authPatientModel = AuthPateintModel.fromJson(result);
        return authPatientModel;
      }
      return authPatientModel ?? AuthPateintModel();
    } catch (e) {
      throw Exception("Get JsonToke Unsuccess");
    }
  }

  Future<String?> getWhoLogin() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var login = prefs.getString(whoLogin.toString()) ?? "";
      if (login != "") {
        return login;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  Future<String?> getKeySecrypt() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      var secryptKey = prefs.getString(keySecrypt.toString()) ?? "";
      if (secryptKey != "") {
        return secryptKey;
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
  //=======================================
}
