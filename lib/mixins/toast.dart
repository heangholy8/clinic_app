// ignore_for_file: sort_child_properties_last
import 'package:clinic_application/widgets/button_widget/button_customwidget.dart';
import 'package:clinic_application/widgets/custom_alertdialog.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import '../app/core/themes/color_app.dart';
import '../app/core/themes/themes.dart';

mixin Toast {
  Future<T> showSuccessDialogUpdateProfile<T>(
    Function() callback,
    String? title,
    String? discription,
    context,
  ) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          blurColor: Colorconstands.primaryColor.withOpacity(0.1),
          child: Icon(
            Icons.check_circle,
            color: Colorconstands.primaryColor,
            size: MediaQuery.of(context).size.width / 6,
          ),
          title: title ?? "SUCCESSFULLY".tr(),
          subtitle:discription??"SUCCESS_SUBTITLE".tr(),
          onPressed: callback,
          // () {
          //   Navigator.push(
          //     context,
          //     PageTransition(
          //       child: const HomeScreen(),
          //       type: PageTransitionType.rightToLeft,
          //     ),
          //   );
          //},
          titleButton: 'OK'.tr(),
          buttonColor: Colorconstands.primaryColor,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }
  Future<T> showSuccessDialog<T>( 
    {required Function() callback,
    String? title,
    String? discription,
    String? titleButton,
    Color? colorButton,
    Color? colorIcon,
    context,
   }
  ) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          blurColor: Colorconstands.primaryColor.withOpacity(0.1),
          child: Icon(
            Icons.check_circle,
            color: colorIcon??Colorconstands.primaryColor,
            size: MediaQuery.of(context).size.width / 6,
          ),
          title: title ?? "SUCCESSFULLY".tr(),
          subtitle:discription??"SUCCESS_SUBTITLE".tr(),
          onPressed: callback,
          // () {
          //   Navigator.push(
          //     context,
          //     PageTransition(
          //       child: const HomeScreen(),
          //       type: PageTransitionType.rightToLeft,
          //     ),
          //   );
          //},
          titleButton:titleButton??'OK'.tr(),
          buttonColor:colorButton?? Colorconstands.primaryColor,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  Future<T> showErrorDialog<T>(
    Function() callback,
    context,{String? title, String? description}
  ) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          child: Icon(
            Icons.cancel,
            color: Colors.red,
            size: MediaQuery.of(context).size.width / 6,
          ),
          title: title?? "INVALID CODE".tr(),
          subtitle:description?? "INVALID SUBTITLE".tr(),
          onPressed: callback,
          titleButton: "TRYAGAIN".tr(),
          buttonColor: Colors.red,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  Future<T> showInformationDialog<T>(
    Function() callback,
    context,{String? name,String? nameEn, String? profile,String? gender,String? phone, String? dob}
  ) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return Dialog(
          elevation: 5,
          backgroundColor: Colorconstands.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 25),
                  width: MediaQuery.of(context).size.width / 6,
                  height: MediaQuery.of(context).size.width / 6,
                  decoration: BoxDecoration(
                      color:Colorconstands.primaryColor.withOpacity(0.1),
                      shape: BoxShape.circle),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 6,
                    child: CircleAvatar(
                      backgroundColor: Colorconstands.primaryColor,
                      backgroundImage: NetworkImage(profile.toString(),),
                    ),
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 35, left: 22.0, right: 22.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              "ឈ្មោះ : ",
                              style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.lightTrunks,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),
                            Expanded(child: Text(
                              name.toString(),style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),)
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "Name : ",
                              style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.lightTrunks,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),
                            Expanded(child: Text(
                              nameEn.toString(),style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.lightTrunks,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),)
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "Gender : ",
                              style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.lightTrunks,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),
                            Expanded(child: Text(
                               gender == null?"---":gender.toString() == "1"?"Male":"Female",style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),)
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "Date of Birth : ",
                              style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.lightTrunks,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),
                            Expanded(child: Text(
                              dob.toString(),style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),)
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "Phone : ",
                              style: ThemeConstands.headline6_Regular_14_24height.copyWith(color: Colorconstands.lightTrunks,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),
                            Expanded(child: Text(
                              phone.toString(),style: ThemeConstands.headline5_Medium_16.copyWith(color: Colorconstands.black,fontWeight: FontWeight.w600),textAlign: TextAlign.left,
                            ),)
                          ],
                        ),
                        
                      ],
                    )),
                Container(
                  margin: const EdgeInsets.only(top: 8, left: 17.0, right: 17.0),
                  child: Align(
                    child: Text(
                      "",
                      style: ThemeConstands.subtitle1_Regular_16.copyWith(
                          color: Colorconstands.darkGray,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 25, bottom: 25),
                  child: Button_Custom(
                    onPressed: callback,
                    buttonColor: Colorconstands.primaryColor,
                    hightButton: 45,
                    radiusButton: 12,
                    maginRight: 17.0,
                    maginleft: 17.0,
                    titleButton: "OK".tr(),
                    titlebuttonColor: Colorconstands.white,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<T> showAlertLogout<T>(
      {required String title,
      void Function()? onSubmit, 
      void Function()? onCancel,
      String? onSubmitTitle,
      Color? bgColorSubmitTitle,
      Color? bgColoricon,
      String? onCancelTitle,
      Widget? icon,
      required BuildContext context}) async {
    return await showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return Dialog(
          elevation: 5,
          backgroundColor: Colorconstands.neutralWhite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 25),
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width / 6,
                  height: MediaQuery.of(context).size.width / 6,
                  decoration: BoxDecoration(
                      color:bgColoricon??Colorconstands.errorColor.withOpacity(0.1),
                      shape: BoxShape.circle),
                  child: Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: icon ?? Icon(
                      Icons.info,
                      color: Colors.green,
                      size: MediaQuery.of(context).size.width / 6,
                    ),
                  ),
                ),
                Container(
                    margin:
                        const EdgeInsets.only(top: 20, left: 17.0, right: 17.0),
                    child: Align(
                      child: Text(
                        title,
                        style: ThemeConstands.button_SemiBold_16.copyWith(
                            color: Colorconstands.lightBulma,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.center,
                      ),
                    )),
                Container(
                  margin: const EdgeInsets.only(top: 25, bottom: 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Button_Custom(
                          onPressed: onCancel ?? (){
                            Navigator.pop(context);
                          },
                          buttonColor: Colorconstands.neutralGrey,
                          hightButton: 45,
                          radiusButton: 12,
                          widthButton: double.maxFinite,
                          maginleft: 17.0,
                          titleButton:onCancelTitle?? "NO".tr(),
                          titlebuttonColor: Colorconstands.lightBlack,
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Button_Custom(
                          onPressed: onSubmit,
                          buttonColor: bgColorSubmitTitle ?? Colorconstands.mainColorSecondary,
                          hightButton: 45,
                          radiusButton: 12,
                          maginRight: 17.0,
                          titleButton:onSubmitTitle?? "LEAVE".tr(),
                          titlebuttonColor: Colorconstands.neutralWhite,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future success(
      context, String? title, String? subtitle, Function onTap) async {
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          blurColor: const Color(0xFF2699FB),
          child: Center(
            child: Icon(
              Icons.check_circle,
              color: Colorconstands.white,
              size: MediaQuery.of(context).size.width / 6,
            ),
          ),
          title: title!,
          subtitle: subtitle!,
          onPressed: () {
            onTap();
          },
          titleButton: 'OK',
          buttonColor: Colorconstands.primaryColor,
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }

  Future unsuccess(
      context, String? title, String? subtitle, Function onTap) async {
    showDialog(
      barrierDismissible: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: SvgPicture.asset(
              "assets/icons/cross_circle.svg",
              width: MediaQuery.of(context).size.width / 6,
            ),
          ),
          title: title!,
          subtitle: subtitle!,
          onPressed: () {
            onTap();
          },
          titleButton: 'Try Again!',
          buttonColor: const Color(0xFFEB4D4B),
          titlebuttonColor: Colorconstands.white,
        );
      },
    );
  }
}
