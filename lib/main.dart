import 'package:clinic_application/app/core/themes/color_app.dart';
import 'package:clinic_application/app/routes/route_generator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'app/core/themes/build_material_color.dart';
import 'app/routes/app_routes.dart';
import 'helper/bloc_provider_helper.dart';

void main() async {
  EasyLocalization.logger.enableBuildModes = [];
  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('km'), Locale('en')],
      path: 'assets/translations',
      startLocale: const Locale('km'),
      child: const MyApp(),
    ),
  );
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final fontName = context.locale.toString();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  
    return MultiBlocProvider(
      providers: listBlocProvider,
      child: MaterialApp(
        builder: (context, child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaler:const TextScaler.linear(1.0),),
            child: child!,
          );
        },
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        initialRoute: Routes.SPLAHSSCREEN,
        onGenerateRoute: RouteGenerator.generateRoute,
        theme: ThemeData(
          primarySwatch: buildMaterialColor(Colorconstands.primaryColor),
          fontFamily: fontName == "en" ? "KantumruyPro" : "KantumruyPro",
        ),
      ),
   
    );
  }
}
