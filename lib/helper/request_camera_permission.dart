
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

void requestCameraPermission() async {
    try {
      if (!await Permission.camera.isGranted) {
        final status = await Permission.camera.request();
        if (status == PermissionStatus.granted) {
        } else if (status == PermissionStatus.permanentlyDenied) {
          //  await openAppSettings();
        } else {}
      } else {}
    } catch (e) {
      debugPrint('$e');
    }
  }