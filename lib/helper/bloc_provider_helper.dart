import 'package:clinic_application/app/modules/doctor_screen/create_vital_sign/state/vitalsign_create_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/get_profile_screen/state/get_profile_patient_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/profile_doctor_screen.dart/state/profile_doctor_bloc.dart';
import 'package:clinic_application/app/modules/doctor_screen/vist_code_screen/state/list_menu_voice_bloc.dart';
import 'package:clinic_application/app/modules/login_screen/state/auth/auth_bloc.dart';
import 'package:clinic_application/app/modules/login_screen/state/list_clinic/bloc/list_clinic_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/appointment_screen/state/book_appointment/book_appointment_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/appointment_screen/state/get_appointment/patient_appointment_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/investigation_screen/state/investigation_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/physical_screen/state/bloc/physical_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/prescription_screen/state/prescription_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/profile_screen.dart/state/bloc/info_patient_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/vaccination_screen/state/vaccination_history_bloc.dart';
import 'package:clinic_application/app/modules/pateint_screen/vital_sing_screen/state/vitalsing_bloc.dart';
import 'package:clinic_application/service/apis/doctor_api/get_list_menu_voice_api/get_list_menu_voice_api.dart';
import 'package:clinic_application/service/apis/doctor_api/get_profile_api/get_doctor_profile_api.dart';
import 'package:clinic_application/service/apis/doctor_api/get_profile_api/get_profile_api.dart';
import 'package:clinic_application/service/apis/doctor_api/list_patient_visit_api/list_patient_vist_api.dart';
import 'package:clinic_application/service/apis/doctor_api/vital_sign_data_api/vital_sign_data_api.dart';
import 'package:clinic_application/service/apis/pateint_api/all_menu_api/menu_api.dart';
import 'package:clinic_application/service/apis/pateint_api/appointment_api/book_appointment_api.dart';
import 'package:clinic_application/service/apis/pateint_api/get_profile_api/get_profile_patient_api.dart';
import 'package:clinic_application/state_management/doctor/list_patoent_visit/list_patient_visit_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../app/modules/pateint_screen/medical_screen/state/medical_history_bloc.dart';
import '../service/apis/auth_api/authentication_api.dart';

var api = GetMenuFeatureApi();

List<BlocProvider> _listBlocProvider = [
  BlocProvider<AuthBloc>(create: (context) => AuthBloc(authApi: AuthApi()),),
  BlocProvider<ListClinicBloc>(create: (context) => ListClinicBloc(listClinic: AuthApi()),),
  BlocProvider<GetProfilePatientBloc>(create: (context) => GetProfilePatientBloc(getProfilePatient: GetProfilePatient()),),
  BlocProvider<ListMenuVoiceBloc>(create: (context) => ListMenuVoiceBloc(postCodeVisitGetListMenuVoiceApi: PostCodeVisitGetListMenuVoiceApi()),),
  BlocProvider<ProfileDoctorBloc>(create: (context) => ProfileDoctorBloc(doctorProfilePatient: GetDoctorProfilePatient()),),
  BlocProvider<InfoPatientBloc>(create: (context) => InfoPatientBloc(getPatientProfile: GetPatientProfile()),),
  BlocProvider<VitalsingBloc>(create: (context) => VitalsingBloc(menuFeatureApi: GetMenuFeatureApi()),),
  BlocProvider<MedicalHistoryBloc>(create: (context) => MedicalHistoryBloc(getMenuFeature: GetMenuFeatureApi()),),
  BlocProvider<InvestigationHistoryBloc>(create: (context) => InvestigationHistoryBloc(getMenuFeature: GetMenuFeatureApi()),),
  BlocProvider<VaccinationHistoryBloc>(create: (context) => VaccinationHistoryBloc(getMenuFeature: GetMenuFeatureApi()),),
  BlocProvider<PrescriptionHistoryBloc>(create: (context) => PrescriptionHistoryBloc(getMenuFeature: GetMenuFeatureApi()),),
  BlocProvider<PhysicalHistoryBloc>(create: (context) => PhysicalHistoryBloc(getMenuFeature: GetMenuFeatureApi()),),
  BlocProvider<PatientAppointmentBloc>(create: (context) => PatientAppointmentBloc(doctorProfilePatient:GetMenuFeatureApi()),),
  BlocProvider<ListPatientVisitBloc>(create: (context) => ListPatientVisitBloc(listVisitPatientApi:ListVisitPatientApi()),),
  BlocProvider<VitalsignCreateBloc>(create: (context) => VitalsignCreateBloc(getVitalSignDataPatientApi:GetVitalSignDataPatientApi()),),
  BlocProvider<BookAppointmentBloc>(create: (context) => BookAppointmentBloc(bookAppointmentApi:BookAppointmentApi()),),
  BlocProvider<DepartmentBloc>(create: (context) => DepartmentBloc(bookAppointmentApi:BookAppointmentApi()),),
  BlocProvider<DepartmentDoctorBloc>(create: (context) => DepartmentDoctorBloc(bookAppointmentApi:BookAppointmentApi()),),
  BlocProvider<DoctorTimeBloc>(create: (context) => DoctorTimeBloc(bookAppointmentApi:BookAppointmentApi()),),
];
List<BlocProvider> get listBlocProvider => _listBlocProvider;


